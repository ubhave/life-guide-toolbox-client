package org.ubhave.ubhavedynamicapp.function;

import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditional;

public class SetVarToTimeUbhaveFunction extends UbhaveFunction{

	String varName;
	
	public SetVarToTimeUbhaveFunction(String fid,String var){
		varName=var;
		setCondition(null);
		setId(fid);
	}
	
	public SetVarToTimeUbhaveFunction(String fid,String var, UbhaveConditional cond){
		varName=var;
		setCondition(cond);
		setId(fid);
	}
	
	@Override
	public void run(DynamicApplicationController dac) {
		if(!this.evalCondition())
			return;
		else{
			InterventionDataAccess.storeCustomVariable(varName, Long.toString(System.currentTimeMillis()));
			//Log.i("UDA", "SR TEST1 "+varName+" "+varVal);
			//Log.i("UDA", "SR TEST2 "+varName+" "+InterventionDataAccess.getVariable(varName));
		}
				
	}
	
	
}