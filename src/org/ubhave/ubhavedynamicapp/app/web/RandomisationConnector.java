package org.ubhave.ubhavedynamicapp.app.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.InterventionPreferences;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

public class RandomisationConnector extends AsyncTask<Void, Void, String>{
	
	public static String getRandomisation(){
		
		String result = null;
		String url = InterventionPreferences.getInterventionDetails(DynamicApplicationConstants.RANDOMISATIONENDPOINT);		
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		final String token = settings.getString(DynamicApplicationConstants.AUTH_TOKEN, "default");
    	
    	HttpClient httpclient = new DefaultHttpClient();
    	
    	Log.i("RAND URL", ""+url);
    	
    	HttpPost request = new HttpPost(url);
    	
    	request.addHeader("Accept", DynamicApplicationConstants.REST_API_CONTENT_TYPE_JSON);
    	request.setHeader("Content-type", DynamicApplicationConstants.REST_API_CONTENT_TYPE_JSON);
        request.addHeader("Authentication", "IDToken "+token);
        
        
        try {
			request.setEntity(new StringEntity(getRandomisationsVarsString()));
        } catch (JSONException e1) {
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e2) {
			e2.printStackTrace();
		}
       
        
        ResponseHandler<String> handler = new BasicResponseHandler(); 
        HttpResponse response = null;
             
        try {  
        		response = httpclient.execute(request);
        		result = handler.handleResponse(response);        		
       
        } catch (ClientProtocolException e) {  
        	result = e.toString();
        	e.printStackTrace();        	
        } catch (IOException e) {
        	result = e.toString();
            e.printStackTrace();
        }  
        httpclient.getConnectionManager().shutdown();  
        		
		Log.i("RAND GET", result); //Look for "status":"success" and "group"
		
		JSONObject resJO=null;
		String resStatus = null;
		String res = null;
		try {
			 resJO = new JSONObject(result);
			 resStatus = resJO.getString("status");
			 res=resJO.getString("group");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(resJO!=null&&resStatus.equals("success")){
			return res;
		}
		else{
			return null;
		}
	}
	
	private static String getRandomisationsVarsString() throws JSONException{
		
		JSONObject randJO = new JSONObject();
		
		for(String var:InterventionPreferences.getInterventionDetails(DynamicApplicationConstants.RANDOMISATIONVARS).split(",")){
			randJO.put(var, InterventionDataAccess.getVariable(var));			
		}		
		
		return randJO.toString(0);	
		
	}
	
	@Override
	protected void onPostExecute(String result){
		super.onPostExecute(result);
	}

	@Override
	protected String doInBackground(Void... params) {
		return getRandomisation();		
	}
	
}