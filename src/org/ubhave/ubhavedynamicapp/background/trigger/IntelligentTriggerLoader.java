/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.background.trigger;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ubhave.intelligenttrigger.triggers.config.BasicTriggerConfig;
import org.ubhave.intelligenttrigger.triggers.definitions.SensorTriggerDefinition;
import org.ubhave.intelligenttrigger.triggers.definitions.TimeTriggerDefinition;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.JSONLoader;
import org.ubhave.ubhavedynamicapp.UbhaveTextAdaptor;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditional;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditionalLoader;

import android.util.Log;
import android.util.SparseArray;

import com.ubhave.triggermanager.config.TriggerConfig;
import com.ubhave.triggermanager.triggers.TriggerUtils;

public class IntelligentTriggerLoader extends JSONLoader {

	private final static String TRIGGERS = "triggers";	
	private final static String TRIGGER_ID = "ID";	
	private final static String TRIGGER_TYPE = "trigger_type";	
	// NOTE: "condition" determines whether a trigger is going to
	// be initialised at all.
	// Not to be confused with action_condition
	private final static String TRIGGER_CONDITION = "condition";	
	private final static String TRIGGER_ACTION_CONTENT = "action_content";
	
	private final static String TRIGGER_ACTION_TYPE = "action_type";
	private final static String TRIGGER_ACTION_ID = "action_id";
	private static final String TRIGGER_ACTION_CONDITION = "action_condition";
	private static final String TRIGGER_ACTION_POST = "action_post";

	private final static String TRIGGER_NOTIF_TITLE = "notification_title";
	private final static String TRIGGER_NOTIF_TEXT = "notification_text";		
	private final static String TRIGGER_SENSOR_ID_ARRAY = "sensor_id_array";
	private final static String TRIGGER_CONFIG = "trigger_config";	
	private final static String TRIGGER_SUBTYPE = "subtype";
	private final static String SENSING_CONDITIONS = "sensing_conditions";
	private final static String TRIGGER_SUBCONFIG = "subconfig";
	private final static String TRIGGER_DELAY_MINS = "delay_mins";
	private final static String TRIGGER_INTERVAL_DAYS = "interval_days";
	private final static String TRIGGER_INTERVAL_START_DAYS = "interval_start_after_days";
	private final static String TRIGGER_DAILY_HOUR = "dailyHour";
	private final static String TRIGGER_DAILY_MINS = "dailyMinute";
	private static final String MODALITY_TYPE = "mod";
	private static final String RELATION = "relation";
	private static final String MODALITY_TARGET_VALUE = "value";
	private static final String MODALITY_DEFAULT_TARGET_VALUE = "default_value";

	private static final String TAG = "IntelligentTriggerLoader";

	public static ArrayList<String> listAllTriggers() throws Exception {
		
		ArrayList<String> triggers = new ArrayList<String>();
		

		JSONObject configJSON = JSONLoader.getFileContents(DynamicApplicationConstants.CURRENT_INTERVENTION_CONFIG).getJSONObject("content");
		
		///Debug		
		//configJSON = new JSONObject(JSONLoader.loadFileContents(R.raw.stressproto)).getJSONObject("content");
		////
		
		if (configJSON.has(TRIGGERS)) { 
			JSONArray triggerArray = (JSONArray) configJSON.get(TRIGGERS);
			
			for (int i = 0; i < triggerArray.length(); i++) {
				JSONObject triggerJO = triggerArray.getJSONObject(i);
				String triggerID = triggerJO.getString(TRIGGER_ID);
				if (triggerID != null && !triggerID.equals("")) {
					triggers.add(triggerID);
				}
			}
		}
		
		return triggers;
	}
	
	public static TriggerDefinitionBundle loadTrigger(final String a_triggerID) throws Exception {
		
		JSONObject configJSON = JSONLoader.getFileContents(DynamicApplicationConstants.CURRENT_INTERVENTION_CONFIG).getJSONObject("content");	
		
		///Debug		
		//configJSON = new JSONObject(JSONLoader.loadFileContents(R.raw.stressproto)).getJSONObject("content");
		////
		
		JSONArray triggersJA = (JSONArray) configJSON.get(TRIGGERS);
		
		if (triggersJA != null) {

			for (int i = 0; i < triggersJA.length(); i++) {

				JSONObject triggerJO = triggersJA.getJSONObject(i);

				String triggerID = triggerJO.getString(TRIGGER_ID);

				if (triggerID.equals(a_triggerID)) {

					Log.d(TAG, "Inspecting trigger "+triggerID);
					
					TriggerDefinitionBundle triggerDef;

					int triggerType = triggerJO.getInt(TRIGGER_TYPE);

					UbhaveConditional triggerCondition = null;
					if (triggerJO.has(TRIGGER_CONDITION)) {
						JSONObject triggerConditionJSON = triggerJO.getJSONObject(TRIGGER_CONDITION);
						triggerCondition = UbhaveConditionalLoader.loadConditional(triggerConditionJSON);
					}
					
					//JSONObject triggerActionContentJO = triggerJO.getJSONObject(TRIGGER_ACTION_CONTENT);

					JSONObject triggerConfigJSON = triggerJO.getJSONObject(TRIGGER_CONFIG);

					BasicTriggerConfig subConfig = new BasicTriggerConfig(true);

					JSONObject subconfigJSON = null;
					
					if (triggerConfigJSON.has(TRIGGER_SUBCONFIG)){
						subconfigJSON = triggerConfigJSON.getJSONObject(TRIGGER_SUBCONFIG);
						// Copy params from "subconfig" to IntelligentTriggerConfig subConfig
				        Iterator<?> keys = subconfigJSON.keys();
				        while(keys.hasNext()){
				            String key = (String)keys.next();
				            subConfig.addParam(key, subconfigJSON.get(key));
				            Log.d(TAG, "Added config key "+key);
				        }
					}
					
					JSONArray triggerActionContentJA = triggerJO.getJSONArray(TRIGGER_ACTION_CONTENT);
					
					ArrayList<TriggerAction> actions = new ArrayList<TriggerAction>();
					
					TriggerAction triggerAction = null;

					for (int j = 0; j<triggerActionContentJA.length(); j++) {
						
						JSONObject actionJO = triggerActionContentJA.getJSONObject(j);
						int triggerActionType = actionJO.getInt(TRIGGER_ACTION_TYPE);
						String triggerActionID = actionJO.getString(TRIGGER_ACTION_ID);
						UbhaveConditional condition = null;
						
						if (actionJO.has(TRIGGER_ACTION_CONDITION)) {
							JSONObject notificationCondition = actionJO.getJSONObject(TRIGGER_ACTION_CONDITION);
							condition = UbhaveConditionalLoader.loadConditional(notificationCondition);
						}
						
						if (triggerActionType == DynamicApplicationConstants.ACTION_NOTIFICATION) {
							String notificationTitle = UbhaveTextAdaptor.resolveMarkup(actionJO.getString(TRIGGER_NOTIF_TITLE));
							String notificationText = UbhaveTextAdaptor.resolveMarkup(actionJO.getString(TRIGGER_NOTIF_TEXT));	
						
						//String notificationType = triggerActionContentJO.getString(TRIGGER_NOTIF_TYPE);
						//boolean persistent = triggerActionContentJO.getBoolean(TRIGGER_PERSISTENT);
						//boolean notification = triggerActionContentJO.getBoolean(TRIGGER_NOTIFICATION);
						//boolean dialog = triggerActionContentJO.getBoolean(TRIGGER_DIALOG);
						//triggerAction = new NotificationTriggerAction(notificationType, notificationID, 
						//		notificationTitle, notificationText, notification, persistent, dialog);
						
							triggerAction = new NotificationTriggerAction(triggerActionID, notificationTitle, 
																	notificationText, condition);

						} else if (triggerActionType == DynamicApplicationConstants.ACTION_SENSING) {
							
							ArrayList<Integer> sensorIDs = new ArrayList<Integer>();
							JSONArray sensorIDsJA = actionJO.getJSONArray(
									TRIGGER_SENSOR_ID_ARRAY);
							for (int k = 0; k < sensorIDsJA.length(); k++) {
								sensorIDs.add(sensorIDsJA.getInt(k));
							}
							triggerAction = new SensingTriggerAction(triggerActionID, sensorIDs, condition);
						}						

						if (actionJO.has(TRIGGER_ACTION_POST)) {
							
							JSONArray postFunctions = actionJO.getJSONArray(TRIGGER_ACTION_POST);
							
							triggerAction.addPostAction(postFunctions);
							Log.d(TAG, "Post functions loaded: "+postFunctions.toString());
							//for(int k = 0; k<postFunctions.length();k++){
							//	triggerAction.addPostFunction(UbhaveFunctionLoader.loadFunction(postFunctions.getJSONObject(i)));
							//}
						}
						
						actions.add(triggerAction);
					}

					if (triggerType == org.ubhave.intelligenttrigger.utils.Constants.TIME_TRIGGER) {

						Log.d(TAG, "Trigger name " + a_triggerID + " type - time (" + triggerType + ")");

						int triggerSubtype = triggerConfigJSON.getInt(TRIGGER_SUBTYPE);
										        
						switch (triggerSubtype) {
							case TriggerUtils.TYPE_CLOCK_TRIGGER_ONCE:
								if (subconfigJSON != null) {
									// We take the int delay in minutes and convert it to millis
									long triggerDelay = 5*1000*subconfigJSON.getInt(TRIGGER_DELAY_MINS);
									subConfig.addParam(TriggerConfig.CLOCK_TRIGGER_DATE_MILLIS, 
											System.currentTimeMillis() +triggerDelay);
								}
								break;
							case TriggerUtils.TYPE_CLOCK_TRIGGER_ON_INTERVAL:
								if (subconfigJSON != null) {
									long intervalTime = 24*60*60*1000*subconfigJSON.getInt(TRIGGER_INTERVAL_DAYS);
									subConfig.addParam(TriggerConfig.INTERVAL_TIME_MILLIS, intervalTime);
									long intervalStartDelay = 24*60*60*1000*subconfigJSON.getInt(TRIGGER_INTERVAL_START_DAYS);
									subConfig.addParam(TriggerConfig.INTERVAL_TRIGGER_START_DELAY, intervalStartDelay);
								}
								break;
							case TriggerUtils.TYPE_CLOCK_TRIGGER_DAY_INTERVAL:
								if (subconfigJSON != null) {
									long dailyHour = subconfigJSON.getLong(TRIGGER_DAILY_HOUR);
									subConfig.addParam(TRIGGER_DAILY_HOUR, dailyHour);
									long dailyMins = subconfigJSON.getLong(TRIGGER_DAILY_MINS);
									subConfig.addParam(TRIGGER_DAILY_MINS, dailyMins);
									
								}
								break;
							default:
								break;
						}
				        
						triggerDef = new TriggerDefinitionBundle(new TimeTriggerDefinition(triggerSubtype, subConfig), triggerCondition, actions, null);

					} else { // This has to be a sensor trigger.

						Log.d(TAG, "Trigger name " + a_triggerID + " type - sensor (" + triggerType + ")");
						
						SensorTriggerDefinition sensorDef = new SensorTriggerDefinition(subConfig);

						SparseArray<String> variables = new SparseArray<String>();
						
						Log.d(TAG, triggerConfigJSON.toString());
						if (triggerConfigJSON.has(SENSING_CONDITIONS)) {
							JSONArray sensorIDsJA = triggerConfigJSON.getJSONArray(SENSING_CONDITIONS);
	
							
							for (int j = 0; j < sensorIDsJA.length(); j++) {
								
								JSONObject condition = (JSONObject) sensorIDsJA.get(j);
								int modType = condition.getInt(MODALITY_TYPE);
								int relType = condition.getInt(RELATION);
								
								// Get user variable if possible
								
								Object value = condition.get(MODALITY_TARGET_VALUE);
								
								if (UbhaveTextAdaptor.isMarkup((String) value)) {
									
									String variableName = InterventionDataAccess.getVariable((String)value);
									
									value = InterventionDataAccess.getVariable(variableName);
									
									if (value.equals("")) {
										// The variable has not been set yet
										String defaultValue = (String) condition.get(MODALITY_DEFAULT_TARGET_VALUE); 
										InterventionDataAccess.storeCustomVariable(variableName, defaultValue);
										value = defaultValue; 
									}
									
									variables.append(modType, variableName);
								}
							
								sensorDef.addCondition(modType, relType, value);
								
							}
						}

						triggerDef = new TriggerDefinitionBundle(sensorDef, triggerCondition, actions, variables);
					}

					return triggerDef;
				}
			}
		}
		return null;
	}
}
