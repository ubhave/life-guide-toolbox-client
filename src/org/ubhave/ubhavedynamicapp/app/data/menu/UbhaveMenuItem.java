/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.app.data.menu;

import java.util.ArrayList;

import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditional;
import org.ubhave.ubhavedynamicapp.function.UbhaveFunction;

//TODO: should be parcelable
public class UbhaveMenuItem{
	
	final public static int NAV_STATE_ACTIVE = 1;
	final public static int NAV_STATE_HIDDEN = 0;
	final public static int NAV_STATE_GREY = 2;
	
	int type;
	String id;
	String label;
	JSONObject content;
	boolean skipable;
	
	boolean isRef;
	String ref;
	
	UbhaveConditional navCond;
	int navCondFail;
	
	int navState;
	
	ArrayList<UbhaveFunction> postFunctions;
	
	public UbhaveMenuItem(int t, String i, String l, JSONObject c){
		type=t;
		id=i;
		label = l;
		content=c;
		skipable=true; // all activities default to skipable
		
		isRef=false;
		
		navCond=null;
		postFunctions = new ArrayList<UbhaveFunction>(); 
		
		navState=1;
	}
	
	public int getType(){
		return type;
	}
	
	public String getID(){
		return id;		
	}
	
	public String getLabel(){
		return label;
	}
	
	public JSONObject getContent(){
		return content;
	}
	
	//Setting and getting refs
	//TODO: This will work for now but is a clumsy solution. We should consider separating content and structure entirely.
	public void setRef(String r){
		isRef=true;
		ref = r;
	}	
	public boolean getIsRef(){
		return isRef;
	}	
	public String getRef(){
		return ref;
	}
	
	public void setNavCond(UbhaveConditional cond, int condFail){
		navCond=cond;
		navCondFail=condFail;
	}
	public UbhaveConditional getNavCond(){
		return navCond;
	}
	public int getNavCondFail(){
		return navCondFail;
	}
	public boolean evalNavCond(){
		if(navCond!=null&&!navCond.evaluate()){
			navState=navCondFail;
			return false;
		}
		else{
			navState=1;
			return true;
		}			
	}
	public int getNavState(){
		return navState;
	}
	public void addPostFunction(UbhaveFunction function){
		postFunctions.add(function);
	}
	public ArrayList<UbhaveFunction> getPostFunctions(){
		return postFunctions;
	}

	public boolean isSkipable() {
		return skipable;
	}

	public void setSkipable(boolean skipable) {
		this.skipable = skipable;
	}
}