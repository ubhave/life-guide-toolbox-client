/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.app.data.menu;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.JSONLoader;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditionalLoader;
import org.ubhave.ubhavedynamicapp.function.UbhaveFunctionLoader;

public class MenuLoader extends JSONLoader{
	
	private final static String ITEMS = "items";
	private final static String LABEL = "label";
	private final static String TYPE = "type";
	private final static String ID = "id";
	private final static String CONTENT = "content";
	private final static String SKIPABLE = "skipable";
	
	private final static String IS_REF = "isRef";
	private final static String REF = "ref";
	
	private final static String NAV_CONDITION = "navCondition";
	private final static String NAV_CONDITION_FAIL_EFFECT = "navConditionFail";
	private static final String POST_ACTIVITY = "postActivity";
	
	public static UbhaveMenu loadMenu(JSONObject menuJSON) throws JSONException{
		
		//String menuLabel = menuJSON.getString(LABEL);
		ArrayList<UbhaveMenuItem> items = new ArrayList<UbhaveMenuItem>();	
		JSONArray itemsJSON = menuJSON.getJSONArray(ITEMS);
		
		for(int i=0; i<itemsJSON.length();i++){			
			JSONObject itemJSON = itemsJSON.getJSONObject(i);
								
			items.add(loadMenuItem(itemJSON));
			
		}
		
		return new UbhaveMenu(items);
		
	}
	
	public static UbhaveMenuItem loadMenuItem(JSONObject itemJSON) throws JSONException{
		
		int type = itemJSON.getInt(TYPE);
		String id = itemJSON.getString(ID);
		String itemLabel = itemJSON.getString(LABEL);
					
		JSONObject content = itemJSON.getJSONObject(CONTENT);
		
		UbhaveMenuItem item = new UbhaveMenuItem(type,id,itemLabel,content);
		
		//Ref setting
		//TODO: This will work for now but is a clumsy solution. We should consider separating content and structure entirely.
		if(itemJSON.has(IS_REF)&&itemJSON.getBoolean(IS_REF))
			item.setRef(item.getContent().getString(REF));
		
		
		//Nav Condition Loading
		if(itemJSON.has(NAV_CONDITION)){
			JSONObject navCond = itemJSON.getJSONObject(NAV_CONDITION);
			int navCondFail = itemJSON.getInt(NAV_CONDITION_FAIL_EFFECT);
			item.setNavCond(UbhaveConditionalLoader.loadConditional(navCond), navCondFail);			
		}
				
		//post functions loading
		if(itemJSON.has(POST_ACTIVITY)){
			JSONArray postFunctions = itemJSON.getJSONArray(POST_ACTIVITY);
			
			for(int i = 0; i<postFunctions.length();i++){
				item.addPostFunction(UbhaveFunctionLoader.loadFunction(postFunctions.getJSONObject(i)));
			}
		}
		
		if(itemJSON.has(SKIPABLE)) {
			boolean skipable = itemJSON.getBoolean(SKIPABLE);
			item.setSkipable(skipable);
		}
		
		return item; 
			
	}
	
}