/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.settings.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.JSONLoader;

public class SettingsLoader extends JSONLoader{

	private final static String SETTINGS = "settings";
	private static final String SETTING_ID = "setting_id";
	private static final String SETTING_TITLE = "title";
	private static final String OPTIONS = "options";
	private static final String OPTION_TYPE = "option_type";
	private static final String OPTION_KEY = "option_key";
	private static final String OPTION_DEFAULT = "option_default";
	private static final String OPTION_TITLE = "option_title";
	private static final String OPTION_DESCRIPTION = "option_description";
	
	
	public static Settings loadSettings(JSONObject settingsJO, String settingsID) throws JSONException, NullPointerException{
		Settings settings;
		ArrayList<SettingOption> optionList = new ArrayList<SettingOption>();
		String title="";
				
		//String setting_id = settingsJO.getString(SettingsLoader.SETTING_ID);			
			
		title = settingsJO.getString(SettingsLoader.SETTING_TITLE);
								
		JSONArray optionsJA = settingsJO.getJSONArray(SettingsLoader.OPTIONS);
		
		for (int k = 0; k < optionsJA.length(); k++)
		{
			String optionType = optionsJA.getJSONObject(k).getString(SettingsLoader.OPTION_TYPE);
			String optionKey = optionsJA.getJSONObject(k).getString(SettingsLoader.OPTION_KEY);
			int optionDefault = optionsJA.getJSONObject(k).getInt(SettingsLoader.OPTION_DEFAULT);
			String optionTitle = optionsJA.getJSONObject(k).getString(SettingsLoader.OPTION_TITLE);
			String optionDescription = optionsJA.getJSONObject(k).getString(SettingsLoader.OPTION_DESCRIPTION);
			
			SettingOption option=null;
			switch(new Integer(optionType)){
			case 0: 
				option = new BoolOption(optionKey,optionDefault,optionTitle,optionDescription);
				break;
				
			case 1: 
				option = new TimeOption(optionKey,optionDefault,optionTitle,optionDescription);
				break;							
			}
			
			optionList.add(option);							
		}
			
		
		settings = new Settings(title, settingsID, optionList);
		
		return settings;
	}
	
	
}