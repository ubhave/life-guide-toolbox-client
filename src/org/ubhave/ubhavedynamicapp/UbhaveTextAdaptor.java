/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp;

import android.util.Log;

public class UbhaveTextAdaptor{
	
	public static final String TAG = "UbhaveTextAdaptor";
	
	public static boolean isMarkup(String value) {
		return value.contains(DynamicApplicationConstants.MARKUP_TAG);
	}
	
	public static String extractVariableName(String res){ 
		int tagStart = nextTag(res);
		
		//TODO: Dirty but works for now - do this better
		int tagEnd =res.substring(tagStart).length();
		
		int wsEnd = res.substring(tagStart).indexOf(" ");
		int pEnd = res.substring(tagStart).indexOf(".");
		
		if(wsEnd!=-1&&wsEnd<pEnd)
			tagEnd=wsEnd;
		if(pEnd!=-1&&pEnd<wsEnd)
			tagEnd=pEnd;
		
		tagEnd=tagStart+tagEnd;			 
		
		return res.substring(tagStart+1, tagEnd);
	}
	
	
	public static String resolveMarkup(String markup){
		String res=markup;
				
		while(nextTag(res)>-1){
			//System.out.println("loop1 "+res);
			int tagStart = nextTag(res);
			
			//TODO: Dirty but works for now - do this better
			int tagEnd =res.substring(tagStart).length();
			
			int wsEnd = res.substring(tagStart).indexOf(" ");
			int pEnd = res.substring(tagStart).indexOf(".");
			
			if(wsEnd!=-1&&wsEnd<pEnd)
				tagEnd=wsEnd;
			if(pEnd!=-1&&pEnd<wsEnd)
				tagEnd=pEnd;
			
			tagEnd=tagStart+tagEnd;			 
			
			Log.d(TAG, "converting variable: "+res.substring(tagStart+1, tagEnd));
			
			//System.out.println("grab var "+res.substring(tagStart+1, tagEnd)+"-"+InterventionDataAccess.getVariable(res.substring(tagStart+1, tagEnd)));
			res=res.substring(0, tagStart)+InterventionDataAccess.getVariable(res.substring(tagStart+1, tagEnd))+res.substring(tagEnd);
			//res=res.substring(0, tagStart)+"VARIABLE-CALLED-"+res.substring(tagStart+1, tagEnd)+"-GOES-HERE!"+res.substring(tagEnd);
			
		}		
		
		res=res.replace(DynamicApplicationConstants.ESCAPE_TAG+DynamicApplicationConstants.MARKUP_TAG, DynamicApplicationConstants.MARKUP_TAG);
		
		return res;
	}
	
	public static int nextTag(String markup){
		//System.out.println("loop2 "+markup);
		int res = markup.indexOf(DynamicApplicationConstants.MARKUP_TAG);
		if(res>=0&&markup.substring(res-1, res).equals(DynamicApplicationConstants.ESCAPE_TAG)){
			if(markup.length()>res)
				res=nextTag(markup.substring(res+1));
			else
				res=-1;
		}
		return res;
	}
}