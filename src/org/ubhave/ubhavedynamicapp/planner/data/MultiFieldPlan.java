/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;

import android.os.Parcel;
import android.os.Parcelable;

public class MultiFieldPlan extends Plan {
	
	HashMap<String,String> fieldValues;
	
	public MultiFieldPlan(String pID, HashMap<String,String> fv){
		planType = MULTI_FIELD;
		planID = pID;
		fieldValues=fv;
	}
	
	public HashMap<String,String> getFieldValues(){
		return fieldValues;
	}
	
	public String getStringRepresentation(){
		String result = "";
		
		for(String key:fieldValues.keySet()){
			result+=key+": "+fieldValues.get(key)+", ";
		}
		result=result.substring(0,result.length()-1);
		return result;
	}
	
	public JSONObject toJSON() {		
		JSONObject json = new JSONObject();
		JSONObject fields = new JSONObject();
		for (Map.Entry<String, String> entry : fieldValues.entrySet()) { 
			try {
				fields.put(entry.getKey(), entry.getValue());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		try {
			json.put(DynamicApplicationConstants.PLAN_ID, planID);
			json.put(DynamicApplicationConstants.PLAN_FIELDS, fields);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
	

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		ArrayList<String> keys = new ArrayList<String>();	
		ArrayList<String> values = new ArrayList<String>();
		for(String key:fieldValues.keySet()){
			keys.add(key);
			values.add(fieldValues.get(key));
		}
		dest.writeString(planID);
		dest.writeStringList(keys);
		dest.writeStringList(values);
	}
	
	public MultiFieldPlan(Parcel in){
		
		planID = in.readString();
		
		ArrayList<String> keys = new ArrayList<String>();	
		ArrayList<String> values = new ArrayList<String>();			
		in.readStringList(keys);
		in.readStringList(values);
		
		fieldValues = new HashMap<String,String>();
		
		for(int i = 0; i<keys.size(); i++){
			fieldValues.put(keys.get(i), values.get(i));
		}
		
		planType = MULTI_FIELD;
	}
	
	public static final Parcelable.Creator<MultiFieldPlan> CREATOR = new Parcelable.Creator<MultiFieldPlan>()
	{
		public MultiFieldPlan createFromParcel(Parcel in)
		{
			return new MultiFieldPlan(in);
		}

		public MultiFieldPlan[] newArray(int size)
		{
			return new MultiFieldPlan[size];
		}
	};
}