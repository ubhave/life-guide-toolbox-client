/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.settings.data;

import android.os.Parcel;
import android.os.Parcelable;

public class SettingOption implements Parcelable{
	String optionType;
	String optionKey;
	int optionDefault;
	String optionTitle;
	String optionDescription;
	
	public SettingOption(){};
	
	public SettingOption(String ty, String ke, int df, String ti, String de){
		optionType=ty;		
		optionKey=ke;
		optionDefault = df;
		optionTitle=ti;
		optionDescription=de;
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(optionType);	
		dest.writeString(optionKey);
		dest.writeInt(optionDefault);
		dest.writeString(optionTitle);
		dest.writeString(optionDescription);
	}
	
	public static final Parcelable.Creator<SettingOption> CREATOR = new Parcelable.Creator<SettingOption>()
	{
		public SettingOption createFromParcel(Parcel in)
		{
			String optionType=in.readString();
			String optionKey=in.readString();
			int optionDefault = in.readInt();
			String optionTitle=in.readString();
			String optionDescription=in.readString();
			
			switch(new Integer(optionType)){
			case 0: 
				return new BoolOption(optionKey, optionDefault, optionTitle, optionDescription);
			case 1: 
				return new TimeOption(optionKey, optionDefault, optionTitle, optionDescription);
			default:
				return new SettingOption(optionType, optionKey, optionDefault, optionTitle, optionDescription);
			}
		}

		public SettingOption[] newArray(int size)
		{
			return new SettingOption[size];
		}
	};
	
	public String getOptionType(){
		return optionType;
	}
	
	public String getOptionKey(){
		return optionKey;
	}
	
	public int getOptionDefault(){
		return optionDefault;
	}
	
	public String getOptionTitle(){
		return optionTitle;
	}
	
	public String getOptionDescription(){
		return optionDescription;
	}
	
}