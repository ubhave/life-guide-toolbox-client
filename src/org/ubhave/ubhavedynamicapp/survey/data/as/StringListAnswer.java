/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey.data.as;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;

import android.os.Parcel;
import android.os.Parcelable;

public class StringListAnswer extends AbstractAnswer
{
	public static final String ANSWER_LIST_LENGTH = "answerListLength";
	
	private String[] answers;

	public StringListAnswer(String id, int type, ArrayList<String> answers)
	{
		super(id, type);
		this.answers = new String[answers.size()];
		for (int i = 0; i < answers.size(); i++)
		{
			this.answers[i] = answers.get(i);
		}
	}

	public StringListAnswer(Parcel in)
	{
		super(in);
		answers = new String[in.readInt()];
		in.readStringArray(answers);
	}
	

	@Override
	public HashMap<String, String> getFlatEntries() {
		HashMap<String,String> flatAnswers = new HashMap<String, String>();
		flatAnswers.put(this.getQuestionId()+"_"+ANSWER_LIST_LENGTH, Integer.toString(answers.length));
		for (int i = 0; i<answers.length; i++) {
			flatAnswers.put(this.getQuestionId()+"_"+i, answers[i]);
		}
		return flatAnswers;
	}

	public static final Parcelable.Creator<StringListAnswer> CREATOR = new Parcelable.Creator<StringListAnswer>()
	{
		public StringListAnswer createFromParcel(Parcel in)
		{
			return new StringListAnswer(in);
		}

		public StringListAnswer[] newArray(int size)
		{
			return new StringListAnswer[size];
		}
	};

	@Override
	public void writeToParcel(Parcel out, int flags)
	{
		super.writeToParcel(out, flags);
		out.writeInt(answers.length);
		out.writeStringArray(answers);
	}

	@Override
	public JSONObject toJSON() throws JSONException
	{
		JSONObject json = super.toJSON();
		JSONArray answerArray = new JSONArray();
		for (String answer : answers)
		{
			answerArray.put(answer);
		}
		json.put(DynamicApplicationConstants.ANSWER, answerArray);
		return json;
	}

}
