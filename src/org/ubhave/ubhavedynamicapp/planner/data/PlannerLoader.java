/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.JSONLoader;
import org.ubhave.ubhavedynamicapp.planner.PlannerController;

public class PlannerLoader extends JSONLoader {
	
	private static final String HEADER_TEXT = "headerText";
	private static final String TITLE = "title";
	private static final String KEY = "key";
	private static final String FIELDS = "fields";
	private static final String FIELD_KEY = "key";
	private static final String FIELD_LABEL = "label";
	private static final String FIELD_DESCRIPTION = "description";
	private static final String TYPE = "type";
	private static final String OPTIONS = "options";
	private static final String MAX_PLANS = "maxPlans";
	
	public static Planner loadPlanner(JSONObject plannerJO, String id) throws JSONException{
		
		Planner planner = null;
		
		String headerText = plannerJO.getString(HEADER_TEXT);
		String title = plannerJO.getString(TITLE);
		//String key = plannerJO.getString(KEY);
		String key = id;
		int type = plannerJO.getInt(TYPE);
		
		
		if(type==PlannerController.PLANNER_TYPE_TEXT){
			ArrayList<PlannerField> fields = new ArrayList<PlannerField>();
			
			JSONArray fieldsJA = plannerJO.getJSONArray(FIELDS);
			
			for(int i = 0; i<fieldsJA.length(); i++){
				JSONObject fieldJO = fieldsJA.getJSONObject(i);
				String fieldKey = fieldJO.getString(FIELD_KEY);
				String fieldLabel = fieldJO.getString(FIELD_LABEL);
				String fieldDescription = "";
				if(fieldJO.has(FIELD_DESCRIPTION))
					fieldDescription = fieldJO.getString(FIELD_DESCRIPTION);
				
				fields.add(new PlannerField(fieldKey, fieldLabel, fieldDescription));
			}
			
			int maxPlans = plannerJO.getInt(MAX_PLANS);
			
			planner = new TextPlanner(fields,headerText,title,key,maxPlans);
		}
		if(type==PlannerController.PLANNER_TYPE_CHECKLIST){
			JSONArray optionsJA = plannerJO.getJSONArray(OPTIONS);
			ArrayList<String> options = new ArrayList<String>();
			
			for(int i = 0; i<optionsJA.length(); i++){
				options.add(optionsJA.getString(i));
			}
			
			planner = new CheckListPlanner(options,headerText,title,key);
		}
		
		return planner;
		
	}
	
}