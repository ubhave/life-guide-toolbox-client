/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey.ui.lists;

import java.util.ArrayList;

import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.survey.data.as.AbstractAnswer;
import org.ubhave.ubhavedynamicapp.survey.data.as.StringListAnswer;
import org.ubhave.ubhavedynamicapp.survey.data.qs.AbstractQuestion;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

public class MultiCategoryList extends AbstractSurveyListActivity
{
	private final static String TAG = "MultiCategoryList";
	private boolean[] answerSelected;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		answerSelected = new boolean[answers.size()];
		for (int i = 0; i < answerSelected.length; i++)
		{
			answerSelected[i] = false;
		}
		initSubmitButton();
	}

	@Override
	protected AdaptiveSimpleAdapter getAdapter()
	{
		return (new AdaptiveSimpleAdapter(this, data, R.layout.list_item_checked, from, to)
		{
			@Override
			public View getView(int position, View convertView, ViewGroup parent)
			{
				View v = super.getView(position, convertView, parent);
				CheckBox selected = (CheckBox) v.findViewById(R.id.answerCheckbox);
				selected.setChecked(answerSelected[position]);
				return v;
			}
		});
	}

	@Override
	protected int getLayout()
	{
		return R.layout.question_multi_choice_list;
	}

	@Override
	protected void setListOnClick(ListView lv)
	{
		lv.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3)
			{
				answerSelected[position] = !answerSelected[position];
				CheckBox selected = (CheckBox) view.findViewById(R.id.answerCheckbox);
				selected.setChecked(answerSelected[position]);
			}
		});
	}

	private void initSubmitButton()
	{
		Button submit = (Button) findViewById(R.id.submitButton);
		submit.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				ArrayList<String> answers = new ArrayList<String>();
				for (int i = 0; i < answerSelected.length; i++)
				{
					if (answerSelected[i])
					{
						answers.add(data.get(i).get(answerText));
					}
				}
				if (!answers.isEmpty())
				{
					AbstractAnswer answer = new StringListAnswer(question_id, AbstractQuestion.CATEGORICAL_MULTIPLE_CHOICE, answers);
					submitAnswers(answer);
				}
				else
				{
					Toast.makeText(MultiCategoryList.this, "Please select at least one answer.", Toast.LENGTH_SHORT).show();
					Log.d(TAG, "Answers are empty!");
				}

			}
		});
	}

	@Override
	protected String getActivityType() {
		return TAG;
	}
}
