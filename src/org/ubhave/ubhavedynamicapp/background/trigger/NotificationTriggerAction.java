/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.background.trigger;

import org.json.JSONArray;
import org.json.JSONException;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditional;

import android.os.Parcel;
import android.os.Parcelable;

public class NotificationTriggerAction extends TriggerAction implements Parcelable {

	private String d_notificationTitle;
	private String d_notificationText;
	
/*	private String d_notificationType;	
	private boolean d_notification;
	private boolean d_dialog;
	private boolean d_persistent;*/

/*	public NotificationTriggerAction(String a_notificationType, String a_notificationID,
			String a_notificationTitle, String a_notificationText,
			boolean a_notification, boolean a_dialog, boolean a_persistent) {
		super(DynamicApplicationConstants.ACTION_NOTIFICATION);
		d_notificationType = a_notificationType;
		d_notificationID = a_notificationID;
		d_notificationTitle = a_notificationTitle;
		d_notificationText = a_notificationText;
		d_notification = a_notification;
		d_dialog = a_dialog;
		d_persistent = a_persistent;
	}*/
	
	public NotificationTriggerAction(String a_ID,
									String a_notificationTitle, 
									String a_notificationText,
									UbhaveConditional a_condition) {

		super(DynamicApplicationConstants.ACTION_NOTIFICATION);
		d_ID = a_ID;
		d_notificationTitle = a_notificationTitle;
		d_notificationText = a_notificationText;		
		d_condition = a_condition;
	}
	
	public String getNotificationTitle(){
		return d_notificationTitle;
	}
	
	public String getNotificationText(){
		return d_notificationText;
	}
	
	/*public String getNotificationType(){
		return d_notificationType;
	}
	
	public boolean isNotification() {
		return d_notification;
	}
	
	public boolean isDialog() {
		return d_dialog;
	}
	
	public boolean isPersistent() {
		return d_persistent;
	}*/

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(d_type);
		out.writeString(d_ID);
		out.writeString(d_notificationTitle);
		out.writeString(d_notificationText);
		out.writeByte((byte)(d_postAction != null ? 1:0) );
		if (d_postAction != null) out.writeString(d_postAction.toString());
		//out.writeParcelable(d_condition, flags);
		/*out.writeString(d_notificationType);
		out.writeByte((byte) (d_notification ? 1:0));
		out.writeByte((byte) (d_dialog ? 1:0));
		out.writeByte((byte) (d_persistent ? 1:0));*/
	}
	
	public static final Parcelable.Creator<NotificationTriggerAction> CREATOR = new Parcelable.Creator<NotificationTriggerAction>() {
		
		public NotificationTriggerAction createFromParcel(Parcel in) {
			return new NotificationTriggerAction(in);
		}
		
		public NotificationTriggerAction[] newArray(int size) {
			return new NotificationTriggerAction[size];
		}
	};
	
	private NotificationTriggerAction(Parcel in) {
		d_type = in.readInt();		
		d_ID = in.readString();
		d_notificationTitle = in.readString();
		d_notificationText = in.readString();
		boolean isPresent = in.readByte() == 1;
		if (isPresent) {
			try {
				d_postAction = new JSONArray(in.readString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//d_condition = in.readParcelable(UbhaveConditional.class.getClassLoader());
		/*d_notificationType = in.readString();
		d_notification = (boolean) (in.readByte() == 1);
		d_dialog = (boolean) (in.readByte() == 1);
		d_persistent = (boolean) (in.readByte() == 1);*/
	}
	
}
