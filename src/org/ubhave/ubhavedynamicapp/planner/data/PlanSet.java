/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner.data;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;

import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class PlanSet implements Parcelable{
	
	private static final String TAG = "PlanSet";
	
	//TODO: Make plans an array instead
	HashMap<String, Plan> plans;
	String plannerKey;
	
	public PlanSet(String key){
		
		plannerKey=key;
		plans = new HashMap<String, Plan>();		
		
	}
	
	public String getPlannerKey(){
		return plannerKey;
	}
	
	public HashMap<String, Plan> getPlans(){
		return plans;		
	}
	
	public void setPlans(HashMap<String, Plan> p){
		plans=p;
	}
	
	public void addPlan(String id, Plan plan){
		plans.put(id, plan);
	}
	
	public String toStorageJSON() {
		Gson gson = new Gson();
		String JSONstring = gson.toJson(this);
		return JSONstring;
	}
	
	public static PlanSet fromStorageJSON(String a_key, String a_json) {
		
		Gson gson = new GsonBuilder()
			.registerTypeHierarchyAdapter(Plan.class, new PlanAdapter())
			.create();
		
		PlanSet result = (PlanSet) gson.fromJson(a_json, PlanSet.class);
		
		return result;
	}
	
	static class PlanAdapter implements JsonDeserializer<Plan> {
		
		Gson gson;		
		
		PlanAdapter(){
			GsonBuilder gsonBuilder = new GsonBuilder();
		    gson = gsonBuilder.create();		    
		}
		
		public Plan deserialize(JsonElement a_elem, Type a_type, JsonDeserializationContext a_context) throws JsonParseException {
		
			Plan result = null;
			
			JsonObject object = a_elem.getAsJsonObject();
			int type = object.get("planType").getAsInt();
			switch(type){
				case Plan.MULTI_FIELD:
					result = gson.fromJson(a_elem, MultiFieldPlan.class);					
					break;
				case Plan.SINGLE_FIELD:
					result = gson.fromJson(a_elem, SingleFieldPlan.class);					
					break;
				default:
					
			}
			return result;
		}
	}

	public JSONObject toJSON() {
		try {
			SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
			JSONObject data = new JSONObject();
			data.put(DynamicApplicationConstants.CURRENT_TIME, System.currentTimeMillis());
			data.put(DynamicApplicationConstants.UID, settings.getString(DynamicApplicationConstants.UID, null));
			
			JSONArray plansArray = new JSONArray();
			Iterator<Entry<String, Plan>> it = plans.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Plan> entry = it.next();
				JSONObject elem = new JSONObject();
				elem.put(entry.getKey(), entry.getValue().toJSON());
				plansArray.put(elem);
			}
			data.put(DynamicApplicationConstants.PLANNER_KEY, plannerKey);
			data.put(DynamicApplicationConstants.PLANS, plansArray);		
			return data;
		} catch (JSONException e)
		{
			return null;
		}
	}
	
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		/*Bundle b = new Bundle();
		b.putString(DynamicApplicationConstants.PLANNER_KEY, plannerKey);
		b.putSerializable(DynamicApplicationConstants.PLANS, plans);
		dest.writeBundle(b);*/

		dest.writeString(plannerKey);
		dest.writeInt(plans.size());
		for (Map.Entry<String, Plan> entry : plans.entrySet()) {
			dest.writeInt(entry.getValue().getType());
			dest.writeString(entry.getKey());			
			dest.writeParcelable(entry.getValue(), flags);
		}
	}	
	
	public PlanSet(Parcel in){
	
		plans = new HashMap<String, Plan>();
		readFromParcel(in);
/*		plannerKey=in.readString();
		plans=new ArrayList<Plan>();
		in.readTypedList(plans, Plan.CREATOR);*/
	}
	
	public void readFromParcel(Parcel in) {
		/*Bundle b = in.readBundle();
		plannerKey = b.getString(DynamicApplicationConstants.PLANNER_KEY);
		plans = (HashMap<String, Plan>) b.getSerializable(DynamicApplicationConstants.PLANS);*/
		plannerKey = in.readString();
		final int N = in.readInt();
		for (int i=0; i<N; i++) {
			int planType = in.readInt();
			String key = in.readString();
			switch (planType) {
				case Plan.SINGLE_FIELD:
					SingleFieldPlan planS = in.readParcelable(SingleFieldPlan.class.getClassLoader());
					plans.put(key, planS);
					break;
				case Plan.MULTI_FIELD:
					MultiFieldPlan planM = in.readParcelable(MultiFieldPlan.class.getClassLoader());
					plans.put(key, planM);
					break;
			}
		}
	}
	
	public static final Parcelable.Creator<PlanSet> CREATOR = new Parcelable.Creator<PlanSet>()
	{
		public PlanSet createFromParcel(Parcel in)
		{
			return new PlanSet(in);
		}

		public PlanSet[] newArray(int size)
		{
			return new PlanSet[size];
		}
	};
	
}