package org.ubhave.ubhavedynamicapp.function;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.app.web.RandomisationConnector;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditional;

import android.util.Log;

public class GetRandomisationUbhaveFunction extends UbhaveFunction{

	String targetVar;
	String failureString;
	
	public GetRandomisationUbhaveFunction(String fid,String var){
		targetVar=var;
		setCondition(null);
		setId(fid);
		failureString = DynamicApplicationConstants.GET_RANDOMISATION_DEFAULT_FAILURE_STRING;
	}
	
	public GetRandomisationUbhaveFunction(String fid,String var, UbhaveConditional cond){
		targetVar=var;
		setCondition(cond);
		setId(fid);
	}
	
	public void setFailureString(String fs){
		failureString = fs;
	}
	
	@Override
	public void run(DynamicApplicationController dac) {
		if(!this.evalCondition())
			return;
		else{
						
				//InterventionDataAccess.storeCustomVariable(targetVar, RandomisationConnector.getRandomisation());
			
			InterventionDataAccess.storeCustomVariable(targetVar,failureString);
			
			RandomisationConnector randConnector = new RandomisationConnector();
			
			try {
				randConnector.execute();
				String result = randConnector.get(5, TimeUnit.SECONDS);
				if(result!=null){
					InterventionDataAccess.storeCustomVariable(targetVar,result);
					Log.i("RAND STORED", InterventionDataAccess.getVariable(targetVar));
				}
									
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		
	}
	
	
}