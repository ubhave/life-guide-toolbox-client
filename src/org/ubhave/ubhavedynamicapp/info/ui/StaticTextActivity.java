/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.info.ui;

import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.InterventionResourceManager;
import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.UbhaveTextAdaptor;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.info.InformationController;
import org.ubhave.ubhavedynamicapp.info.data.TextInformation;
import org.ubhave.ubhavedynamicapp.log.ApplicationDataLogger;
import org.ubhave.ubhavedynamicapp.log.InteractionLog;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

public class StaticTextActivity extends AbstractInformationActivity
{

	public static final String TAG="StaticTextActivity";
	
	public static final String EXPAND_MARKUP_OPEN="[[[";
	public static final String EXPAND_MARKUP_CLOSE="]]]";
	public static final String EXPAND_OPENING="<button onclick='togglehidden()'>tell me more</button><div id='hidediv' style='display:none'>";
	public static final String EXPAND_CLOSER="</div>";
	public static final String CONTENT_HEAD="<html><head><script type='text/javascript'>function togglehidden(){if(document.getElementById('hidediv').style.display=='none'){document.getElementById('hidediv').style.display='block'}else{document.getElementById('hidediv').style.display='none'};}</script></head><body>";
	public static final String CONTENT_END="</body></html>";
	
	MediaPlayer mediaPlayer;
	boolean mediaPlaying;
	String audio;
	TextInformation currentInformation;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{        
		super.onCreate(savedInstanceState);

		currentInformation = getIntent().getParcelableExtra(InformationController.CURRENT_INFORMATION);
		
		currentInformation.setText(UbhaveTextAdaptor.resolveMarkup(currentInformation.getText()));
		
		WebView text = (WebView) findViewById(R.id.staticText);
		WebSettings webSettings = text.getSettings();
		webSettings.setJavaScriptEnabled(true);
		
		//Slightly risky markup parsing for expandable content
		String content = currentInformation.getText();
		content=content.replace(EXPAND_MARKUP_OPEN,EXPAND_OPENING);
		content=content.replace(EXPAND_MARKUP_CLOSE,EXPAND_CLOSER);
		text.loadData(CONTENT_HEAD+content+CONTENT_END, "text/html", null);
		initButton();
		
		mediaPlaying=false;
		mediaPlayer=null;
		audio=currentInformation.getAudio();
		
		if(audio.equals("")&&findViewById(R.id.audioButton)!=null){
			findViewById(R.id.audioButton).setVisibility(View.GONE);
		}		
	}

	@Override
	protected int getLayout()
	{
		return R.layout.info_static_text;
	}
	
	private void submit()
	{
		this.setResult(DynamicApplicationController.RESULT_INFO_CANCELED);
		this.finish();
	}
	
	//this is untidy but satisfies the need to pass the view on which is unimportant in this method
	public void toggleAudio(View v){
		toggleAudio();
	}
	
	public void toggleAudio(){
		if(mediaPlaying){
			mediaPlayer.release();
			mediaPlaying=false;
			Button button = (Button)findViewById(R.id.audioButton);
			button.setText(getResources().getText(R.string.audioPlay));
			
			ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
			logger.logAppUsage(new InteractionLog(DynamicApplicationConstants.TYPE_INFO, DynamicApplicationConstants.INTERACTION_BUTTON, System.currentTimeMillis(), getIntent().getStringExtra(InformationController.CURRENT_INFORMATION_ID), currentInformation.getTitle(), getResources().getText(R.string.audioPlay).toString(), DynamicApplicationConstants.INTERACTION_INFO_AUDIO_PAUSE));
			
		}
		else{
			Uri uri=InterventionResourceManager.getSoundResourceURI(audio);
			mediaPlayer = MediaPlayer.create(this,uri);
									
			if(mediaPlayer!=null){
				
				ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
				logger.logAppUsage(new InteractionLog(DynamicApplicationConstants.TYPE_INFO, DynamicApplicationConstants.INTERACTION_BUTTON, System.currentTimeMillis(), getIntent().getStringExtra(InformationController.CURRENT_INFORMATION_ID), currentInformation.getTitle(), getResources().getText(R.string.audioPlay).toString(), DynamicApplicationConstants.INTERACTION_INFO_AUDIO_PLAY));
				
				
				Button button = (Button)findViewById(R.id.audioButton);
				button.setText(getResources().getText(R.string.audioStop));
				mediaPlaying=true;
				mediaPlayer.setOnCompletionListener(new OnCompletionListener()
				{
					@Override
					public void onCompletion(MediaPlayer arg0)
					{
						toggleAudio();
					}
				});
				
				mediaPlayer.start();
			}
			else{
				Log.e(TAG,"Media Player Problem - Possible Unsupported Format or Missing File");
				Toast.makeText(this, "Media Player Problem - Possible Unsupported Format", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	@Override
	public void onPause(){
		super.onPause();
		if(mediaPlaying){
			toggleAudio();
		}
	}
	
	

	private void initButton()
	{
		Button submit = (Button) findViewById(R.id.submitButton);
		submit.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				submit();
			}
		});
	}

	@Override
	protected String getActivityType() {
		return TAG;
	}

}
