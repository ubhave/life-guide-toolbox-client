/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.app.ui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.Hasher;
import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.InterventionPreferences;
import org.ubhave.ubhavedynamicapp.InterventionResourceManager;
import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.app.web.DownloadInterventionListTask;
import org.ubhave.ubhavedynamicapp.app.web.DownloadInterventionTask;
import org.ubhave.ubhavedynamicapp.background.auth.AuthenticationTask;
import org.ubhave.ubhavedynamicapp.background.auth.ValidateTokenTask;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class LauncherActivity extends Activity
{
	
	private static final String TAG = "LauncherActivity";
	private final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1;
	private final int REQUEST_ACCOUNT = 2;
	private final int REQUEST_GET_AUTH = 3;
	
	private DynamicApplicationController dac;


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		try {
			dac = new DynamicApplicationController(LauncherActivity.this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		TextView versionTV = (TextView)findViewById(R.id.versionText);
		try {
			versionTV.setText("Version: "+getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				
		//set current menu to 0 to avoid getting trapped in a sub menu
		//Charlie: I don't think we should do this
		/*SharedPreferences sharedPrefs;
		SharedPreferences.Editor editor;
		sharedPrefs=PreferenceManager.getDefaultSharedPreferences(this);
		editor = sharedPrefs.edit();
		editor.putString("currentMenu", "0");
		editor.commit();*/

		Log.d(TAG, "Creating launcher");
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		final String accountName = settings.getString(DynamicApplicationConstants.UID, null);
		final String token = settings.getString(DynamicApplicationConstants.AUTH_TOKEN, null);

		if (accountName == null) {
			if (checkPlayServices()) {
				chooseAccount();
			}
		} else
		if (token == null) {
			authenticateUser(accountName);
		} else {
			startIntervention();
		}
	}

	private boolean checkPlayServices() {
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (status != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
				GooglePlayServicesUtil.getErrorDialog(status, this,
				    REQUEST_CODE_RECOVER_PLAY_SERVICES)
				    .show();
			} else {
				Toast.makeText(this, "This device is not supported.",
				               Toast.LENGTH_LONG).show();
				finish();
			}
			return false;
		}
		return true;
	}

	private void chooseAccount()
	{
		Intent intent = AccountPicker.newChooseAccountIntent(null, null, new String[]{"com.google"},
		                                false, "Please supply a Google account. This app will only " +
		                                "contact Google servers to verify who you are; it will not " +
		                                "access any information Google stores about your nor will " +
		                                "any information you enter into the app be sent to Google.", null, null, null);
		startActivityForResult(intent, REQUEST_ACCOUNT);
	}

	private void storeAuthDetails(String accountName, String token)
	{
		Date now = new Date();
		SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		SharedPreferences.Editor editor = settings.edit();

		editor.putString(DynamicApplicationConstants.AUTH_TOKEN, token);
		editor.putString(DynamicApplicationConstants.UID, accountName);
		editor.putLong(DynamicApplicationConstants.AUTH_TOKEN_EXPIRES,
		               now.getTime() + DynamicApplicationConstants.AUTH_TOKEN_LIFETIME);
		editor.commit();
	}
	private void authenticateUser(final String accountName)
	{
		/* Old logic from AuthenticationTask:
		 * 		SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		String spAccountName = settings.getString(DynamicApplicationConstants.UID, null);

		if (accountName == null) {
			accountName =
			// TODO what to do if accountName is null?
		}
		String token = settings.getString(DynamicApplicationConstants.AUTH_TOKEN, null);
		Long expires = settings.getLong(DynamicApplicationConstants.AUTH_TOKEN_EXPIRES, 0);
		Date now = new Date();

		// Do not do anything if we have a token thats not about (5 mins) to expire
		if ( (token != null) && (now.getTime() < (expires - (1000L * 60 * 5))) ) {
			return token;
		}
		*/
		new AuthenticationTask() {
			protected void onPostExecute(Object result) {
				if (result != null) {
					if        (result instanceof UserRecoverableAuthException) {
						UserRecoverableAuthException e = (UserRecoverableAuthException)result;
						startActivityForResult(e.getIntent(), REQUEST_GET_AUTH);
					} else if (result instanceof GoogleAuthException) {
						// there was a problem with the authentication
						Toast.makeText(LauncherActivity.this, "There was a problem with your Google Account.",
					               Toast.LENGTH_LONG).show();
						finish();
					} else if (result instanceof IOException) {
						// there was a problem contacting auth service
						Toast.makeText(LauncherActivity.this, "Please make sure your internet is working.",
						               Toast.LENGTH_LONG).show();
							finish();
					} else if (result instanceof String) {
						validateWithAppServerStep(accountName, (String)result);
					}
				}
				// shouldnt get here
			}
		}.execute(accountName);	
	}
	
	private void validateWithAppServerStep(final String accountName, final String token)
	{
		Log.d(TAG, "token "+token);
		
		new ValidateTokenTask() {
			protected void onPostExecute(Integer result) {
				if (result == 200) {
					storeAuthDetails(accountName, token);
					startIntervention();
				} else {
					// token is invalid
					// start again
					chooseAccount();
				}
			};
		}.execute(token);
	}
	
	private void startIntervention()
	{
		if (InterventionPreferences.doesInterventionExist())
		{
			
			Log.i(TAG, "Launching Intervention - running fresh start!");
			
			//startActivity(new Intent(UbhaveApplication.getContext(), MenuActivity.class));
			
			dac.freshStart();
			
			finish();
		}
		else
		{
			downloadInterventionList();
			//button.setVisibility(View.VISIBLE);
		}
	}
	
	private void downloadInterventionList() {
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		final String token = settings.getString(DynamicApplicationConstants.AUTH_TOKEN, "default");

		new DownloadInterventionListTask(this) {
			
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				
				if (result != null) {
					
					ListView lv = (ListView) findViewById(R.id.interventionList);
					
					final ArrayList<String> interventionList = new ArrayList<String>();
					
					final ArrayList<HashMap<String, String>> interventionDetails =
								new ArrayList<HashMap<String, String>>();
					
					/* upload, content, uri, id, name */
					
					try {
						JSONArray jArray = new JSONArray(result);
					
						if (jArray != null) {
							for (int i=0;i<jArray.length();i++){
								JSONObject info = jArray.getJSONObject(i);
			
								HashMap<String, String> details=new HashMap<String, String>();
								Iterator<?> keys = info.keys();
								Log.i("INFO JSON", info.toString());
								while( keys.hasNext() ) {
									String key = (String)keys.next();
									//TODO: make these constants
									if(key.equals(DynamicApplicationConstants.RANDOMISATION)){
										JSONObject randJO = info.getJSONObject(key);
										details.put(DynamicApplicationConstants.RANDOMISATIONENDPOINT,randJO.getString(DynamicApplicationConstants.ENDPOINT));
										if(randJO.has(DynamicApplicationConstants.VARS)){
											JSONArray randvarJA = randJO.getJSONArray(DynamicApplicationConstants.VARS);
											if(randvarJA.length()==0)
												details.put(DynamicApplicationConstants.RANDOMISATIONVARS,"");
											else
												details.put(DynamicApplicationConstants.RANDOMISATIONVARS,randvarJA.getString(0));
											for(int j = 1; j<randvarJA.length();j++){
												details.put(DynamicApplicationConstants.RANDOMISATIONVARS,","+randvarJA.getString(j));
											}
										}
									}
									else
										details.put(key, info.getString(key));
								}
								interventionDetails.add(details);
								interventionList.add(info.get("name").toString());
														
																
							}
						}

						ArrayAdapter<String> arrayAdapter =      
							new ArrayAdapter<String>(UbhaveApplication.getContext(),
							                         R.layout.simple_list_item_ubhave, 
							                         interventionList);
						
						
						
						lv.setAdapter(arrayAdapter);
						lv.setOnItemClickListener(new OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> parent,
							                        View view, int position, long id) {
								downloadInterventionConfig(interventionDetails.get(position), token);
							}
						});
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
				}
			}
		}.execute(token);
	}
	
	private void downloadInterventionConfig(final HashMap<String,String> details, String token)
	{
		new DownloadInterventionTask(this, details.get("id"), details.get("content"), details.get("resources"),token)
		{
			@Override
			protected void onPostExecute(String errorMessage)
			{
				super.onPostExecute(errorMessage);
				if (errorMessage == null)
				{
					InterventionPreferences.setIntervention(details.get("id"));
					InterventionPreferences.setInterventionDetails(details);
					
					try {
						InterventionDataAccess.setCurrentInterventionName(Hasher.makeSHA1Hash(details.get("content"))+"-"+details.get("id"));
						InterventionResourceManager.initialiseCurrentInterventionResources();
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//startActivity(new Intent(LauncherActivity.this, MenuActivity.class));
					try {
						new DynamicApplicationController(LauncherActivity.this).start();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					finish();
				}
			}
		}.execute();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		Log.i(TAG,"OAR: "+requestCode+" "+resultCode);
		switch (requestCode) {
			case REQUEST_CODE_RECOVER_PLAY_SERVICES:
				if (resultCode == RESULT_CANCELED) {
					Toast.makeText(this, "Google Play Services must be installed.",
					               Toast.LENGTH_LONG).show();
					finish();
				} else
				if (resultCode == RESULT_OK) {
					chooseAccount();
				}
				return;
			case REQUEST_ACCOUNT:
				if (resultCode == RESULT_OK) {
					authenticateUser(data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME));
				} else {
					// TODO:
					Toast.makeText(this, "Can not continue without an account.",
					               Toast.LENGTH_LONG).show();
					finish();
				}
				break;
			case REQUEST_GET_AUTH:
				if (resultCode == RESULT_OK) {
					String accountName = data.getStringExtra("authAccount" /* AUTH_ACCOUNT_KEY */);
					String token = data.getStringExtra("authtoken" /* AUTHTOKEN_KEY */);

					validateWithAppServerStep(accountName, token);
				} else {
					chooseAccount();
				}
				break;
			default:
				Log.i(TAG,"OAR: Default");
				try {
					dac.onActivityResult(requestCode, resultCode, data);
					//finish();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
		}
		//super.onActivityResult(requestCode, resultCode, data);

		/*try {
			new DynamicApplicationController(this).onActivityResult(requestCode, resultCode, data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
}
