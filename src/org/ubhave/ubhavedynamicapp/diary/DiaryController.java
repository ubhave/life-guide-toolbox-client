/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.diary;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.AbstractComponentController;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveActivity;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.diary.data.Diary;
import org.ubhave.ubhavedynamicapp.diary.data.DiaryLoader;
import org.ubhave.ubhavedynamicapp.diary.data.DiaryMap;
import org.ubhave.ubhavedynamicapp.diary.ui.DiaryActivity;
import org.ubhave.ubhavedynamicapp.log.ApplicationDataLogger;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public class DiaryController extends AbstractComponentController
{
	public final static String DIARY = "diary";
	public final static String DIARY_ID = "diary_id";

	private final static int DIARY_REQUEST_CODE = 101;
	
	private static final String TAG = "DynamicApplicationController";

	private final DateFormat dateFormat;
	private final Calendar calendar;
	
	private final Activity context;
	private DiaryMap diaryEntryMap;
	
	private String diaryID;
	
	private final JSONObject currentDiaryJSON;

	@SuppressLint("SimpleDateFormat")
	public DiaryController(Activity c, JSONObject diaryJO, String id, DynamicApplicationController dynamicApplicationController)
	{
		context = c;
		diaryID = id;
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		calendar = Calendar.getInstance();
		
		currentDiaryJSON = diaryJO;
		
		setAppController(dynamicApplicationController);
	}

	@Override
	public void start()
	{
		try
		{
			diaryEntryMap = DiaryLoader.loadDiary(currentDiaryJSON, diaryID);
			String currentDate = dateFormat.format(calendar.getTime());
			showEntry(currentDate);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void showEntry(final String targetDate)
	{
		Diary entry = diaryEntryMap.getDiary(targetDate);
		Log.i("UDA", DIARY + " " + entry.getTitle());
		
		Intent activityIntent = new Intent(context, DiaryActivity.class);
		activityIntent.putExtra(DIARY, entry);
		activityIntent.putExtra(DIARY_ID, diaryEntryMap.getDiaryId());
		//activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		//activityIntent.setAction(Intent.ACTION_MAIN);
		//activityIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		activityIntent.putExtra(UbhaveActivity.SKIPABLE, isSkipable());
		context.startActivityForResult(activityIntent, DIARY_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.i(TAG, "DC oar resultCode: "+resultCode +" requestCode: "+requestCode);
		System.err.println("onActivityResult "+requestCode);
		//if (requestCode == DIARY_REQUEST_CODE)
		//{
			try
			{
				Diary diary=null;
				String dateString;
				try
				{
					diary = data.getParcelableExtra(DIARY);
					dateString = diary.getDate();
					diaryEntryMap.putDiaryEntry(dateString, diary);
					Date date = dateFormat.parse(dateString);
					calendar.setTime(date);
				}
				catch (ParseException e)
				{
					Log.i(TAG,"e1-1");
					e.printStackTrace();
				}
				catch (NullPointerException e){
					Log.i(TAG,"e1-2");
					e.printStackTrace();
				}
				

				Log.d(TAG, "on res, "+resultCode);

				switch (resultCode)
				{
				case DynamicApplicationController.RESULT_DIARY_OK:
					// TODO: Is RESULT_DIARY_OK an indication that the 
					// diary entry should be saved and uploaded? 
					ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
					logger.logDiaryResponse(diary);
					getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE);
					break;
				case DynamicApplicationController.RESULT_DIARY_PREV:
					Log.i(TAG,"PREV");
					calendar.add(Calendar.DATE, -1);
					dateString = dateFormat.format(calendar.getTime());
					showEntry(dateString);
					break;
				case DynamicApplicationController.RESULT_DIARY_NEXT:
					Log.i(TAG,"NEXT");
					calendar.add(Calendar.DATE, 1);
					dateString = dateFormat.format(calendar.getTime());
					showEntry(dateString);
					break;
				case Activity.RESULT_CANCELED:
					Log.i(TAG,"BACK");
					getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_BACK);
					break;
				case DynamicApplicationController.RESULT_ACTIONBAR_HOME:
					getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_AND_JUMP_TO_FRONT);
					break;
				}
				
					
			}
			catch (NullPointerException e)
			{
				Log.i(TAG,"e2");
				e.printStackTrace();
			}
		//}
	}
}