/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.AbstractComponentController;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.UbhaveActivity;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.log.ApplicationDataLogger;
import org.ubhave.ubhavedynamicapp.planner.data.PlanSet;
import org.ubhave.ubhavedynamicapp.planner.data.Planner;
import org.ubhave.ubhavedynamicapp.planner.data.PlannerLoader;
import org.ubhave.ubhavedynamicapp.planner.ui.CheckListPlannerActivity;
import org.ubhave.ubhavedynamicapp.planner.ui.TextPlannerActivity;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public class PlannerController extends AbstractComponentController{

	public static final String PLANNER = "planner";
	private static final int PLANNER_REQUEST_CODE = 106;
	private static final String TAG = "PlannerController";
	public static final String PLAN_SET = "planSet";
	
	public static final int PLANNER_TYPE_TEXT = 0;
	public static final int PLANNER_TYPE_CHECKLIST = 1;
	
	private Activity context;
	private JSONObject currentPlannerJSON;
	private String planID;
	
	public PlannerController(Activity c, JSONObject plannerJO, String id, DynamicApplicationController dac){
		context=c;
		currentPlannerJSON=plannerJO;
		planID = id;
		setAppController(dac);
	}
	
	@Override
	public void start() {
		Log.d(TAG, "Start planner");
		Planner planner=null;
		try {
			planner = PlannerLoader.loadPlanner(currentPlannerJSON, planID);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		Intent activityIntent=null;
		
		Log.d(TAG, "Type "+planner.getType());
		
		switch(planner.getType()){
			case PLANNER_TYPE_TEXT:
				activityIntent= new Intent(context, TextPlannerActivity.class);
				break;
			case PLANNER_TYPE_CHECKLIST:
				activityIntent= new Intent(context, CheckListPlannerActivity.class);
				break;		
		}
		
		//Test code
		//PlanSet testSet = new PlanSet("plans");
		
		/*HashMap<String,String> testFieldMap = new HashMap<String,String>();
		testFieldMap.put("who", "who test");
		testFieldMap.put("what", "what test");
		testFieldMap.put("where", "where test");
		MultiFieldPlan testPlan = new MultiFieldPlan("testPlan", testFieldMap);
		
		
		SingleFieldPlan testPlan = new SingleFieldPlan("testPlan", "option 2");
		testSet.addPlan("testPlan", testPlan);
		*/
		
		
		//activityIntent.putExtra(PLAN_SET, testSet);
		
		
		//Real code
		
		//TODO: Remove ""+ hack when the ID is a proper string
		PlanSet planSet = InterventionDataAccess.getPlanSet(""+planner.getKey());
		if(planSet==null)
			planSet = new PlanSet(""+planner.getKey());
		activityIntent.putExtra(PLAN_SET, planSet);
		
		
		
		 
		activityIntent.putExtra(PLANNER, planner);
		
		activityIntent.putExtra(UbhaveActivity.SKIPABLE, isSkipable());
		
		context.startActivityForResult(activityIntent, PLANNER_REQUEST_CODE);
		
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "Planner Finished");
		
		switch (resultCode)
		{
		case DynamicApplicationController.RESULT_PLANNER_OK:
			
			
			PlanSet set = data.getParcelableExtra(PLAN_SET);
			
			ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
			logger.logPlanSet(set);
			
			
			//Test Code
			/*Log.i(TAG, "PlanSet Submit"+set.getPlannerKey());
			Iterator<Entry<String, Plan>> it = set.getPlans().entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Plan> entry = it.next();
				MultiFieldPlan plan = (MultiFieldPlan)entry.getValue();
				for(String field:plan.getFieldValues().keySet()){
					Log.i(TAG, "Plan "+field+" "+plan.getFieldValues().get(field));
				}
			}*/
			//
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE);
			
			
			break;
		case DynamicApplicationController.RESULT_ACTIONBAR_HOME:
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_AND_JUMP_TO_FRONT);
			break;
		case Activity.RESULT_CANCELED:
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_BACK);
			break;
		}
		
	}
	
	
	
}