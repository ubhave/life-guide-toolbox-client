/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey.ui;

import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.survey.QuestionnaireController;
import org.ubhave.ubhavedynamicapp.survey.data.as.AbstractAnswer;
import org.ubhave.ubhavedynamicapp.survey.data.as.TimeAnswer;
import org.ubhave.ubhavedynamicapp.survey.data.qs.AbstractQuestion;
import org.ubhave.ubhavedynamicapp.survey.data.qs.OpenEndedQuestion;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

public class TimePickerActivity extends AbstractQuestionActivity
{

	private static final String TAG = "TimePickerActivity";
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		initSubmitButton();

		OpenEndedQuestion question = getIntent().getParcelableExtra(QuestionnaireController.CURRENT_QUESTION);
		String hint = question.getHint();
		if (hint != null)
		{
			EditText answerBox = (EditText) findViewById(R.id.answerBox);
			answerBox.setHint(hint);
		}
	}

	@Override
	protected int getLayout()
	{
		return R.layout.question_timepicker;
	}

	private void initSubmitButton()
	{
		final TimePicker picker = (TimePicker) findViewById(R.id.answerBox);
		Button submit = (Button) findViewById(R.id.submitButton);
		submit.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				int hour = picker.getCurrentHour();
				int minute = picker.getCurrentMinute();
				AbstractAnswer answer = new TimeAnswer(question_id, AbstractQuestion.TIME_QUESTION, hour, minute);
				submitAnswers(answer);
			}
		});
	}

	@Override
	protected String getActivityType() {
		return TAG;
	}

}
