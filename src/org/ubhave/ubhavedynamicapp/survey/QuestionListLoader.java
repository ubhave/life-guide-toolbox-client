/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.JSONLoader;
import org.ubhave.ubhavedynamicapp.survey.data.qs.AbstractQuestion;
import org.ubhave.ubhavedynamicapp.survey.data.qs.CategoricalQuestion;
import org.ubhave.ubhavedynamicapp.survey.data.qs.Instruction;
import org.ubhave.ubhavedynamicapp.survey.data.qs.OpenEndedQuestion;
import org.ubhave.ubhavedynamicapp.survey.data.qs.QuestionList;
import org.ubhave.ubhavedynamicapp.survey.data.qs.RatingList;

public class QuestionListLoader extends JSONLoader
{
	private final static String SURVEY = "survey";
	
	public QuestionList loadSurvey(JSONObject surveyJO, String surveyID) throws JSONException
	{
		try
		{
			JSONObject survey = surveyJO;
			if (survey != null)
			{
				//String survey_id = survey.getString(QuestionList.SURVEY_ID);	
				QuestionList questions = new QuestionList(surveyID);
				JSONArray questionJSON = survey.getJSONArray(QuestionList.QUESTIONS);
				System.err.println("Length: "+questionJSON.length());
				for (int q=0; q<questionJSON.length(); q++)
				{
					JSONObject question = questionJSON.getJSONObject(q);
					System.err.println(question.toString());
					String title = question.getString(AbstractQuestion.TITLE);
					String text = question.getString(AbstractQuestion.TEXT);
					int type = question.getInt(AbstractQuestion.TYPE);
					String id = question.getString(AbstractQuestion.ID);
					
					String fText = "";
					if(question.has(AbstractQuestion.FOOTER_TEXT)){
						fText=question.getString(AbstractQuestion.FOOTER_TEXT);
					}

					if (type == AbstractQuestion.CATEGORICAL_SINGLE_CHOICE || type == AbstractQuestion.CATEGORICAL_MULTIPLE_CHOICE)
					{
						questions.add(new CategoricalQuestion(id, question, title, text, type, fText));
					}
					else if (type == AbstractQuestion.RATING_LIST)
					{
						questions.add(new RatingList(id, question, title, text, type, fText));
					}
					else if (type == AbstractQuestion.FREE_TEXT_QUESTION)
					{
						questions.add(new OpenEndedQuestion(id, type, title, text, question, fText));
					}
					else if (type == AbstractQuestion.INSTRUCTION)
					{
						questions.add(new Instruction(id, question, title, text, fText));
					}
				}
				return questions;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
