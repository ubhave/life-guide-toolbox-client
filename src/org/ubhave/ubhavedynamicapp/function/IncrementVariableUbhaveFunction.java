/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.function;

import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditional;

public class IncrementVariableUbhaveFunction extends UbhaveFunction{

	String varName;
	int increment;
	String sIncrement;
		
	public String loadValue(String key){
		//Log.i("IncrementFunction", "Load Increment "+key);
		String val=key;		
		if(key.length()>1&&key.substring(0,2).equals("v-")){
			val = InterventionDataAccess.getVariable(key.substring(2));
		}		
		return val;
	}
	
	public IncrementVariableUbhaveFunction(String fid,String var, String inc){
		varName=var;
		//Log.i("IncrementFunction", "Const Increment1 "+varName);
		setCondition(null);
		setId(fid);
		sIncrement=inc;
	}
	
	public IncrementVariableUbhaveFunction(String fid,String var, String inc, UbhaveConditional cond){
		varName=var;
		//Log.i("IncrementFunction", "Const Increment2 "+varName);
		setCondition(cond);
		setId(fid);
		sIncrement=inc;
	}
	
	@Override
	public void run(DynamicApplicationController dac) {
		increment=new Integer(loadValue(sIncrement));
		//Log.i("IncrementFunction", "Incrementing "+varName);
		if(!this.evalCondition())
			return;
		else{
			String varVal = InterventionDataAccess.getVariable(varName);
			if(varVal.equals(""))
				varVal="0";
			//Log.i("IncrementFunction", "Incrementing "+varName+" from "+varVal+" by "+increment+" to "+InterventionDataAccess.getVariable(varName));
			InterventionDataAccess.storeCustomVariable(varName, ""+(new Integer(varVal)+increment));
		}
			
	}
	
}