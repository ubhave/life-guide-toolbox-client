/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.log;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UsageLog {

	long d_eventTime; // in millis 
	int d_eventType; // open/close/interaction
	int d_eventSubType; // open/close
	String d_activityID; // given by the designer
	int d_activityType; // menu, survey, diary, etc.
	String d_activityLabel; // given by the designer
	
	public UsageLog(int a_activityType, 
					int a_eventType, 
					int a_eventSubType,
					long a_eventTime, 
					String a_activityID,
					String a_activityLabel) {
		d_activityType = a_activityType;
		d_eventTime = a_eventTime;
		d_eventType = a_eventType;
		d_eventSubType = a_eventSubType;
		d_activityID = a_activityID;
		d_activityLabel = a_activityLabel;		
	}

	public void setActivityType(int a_activityType) {
		d_activityType = a_activityType;
	}

	public void setEventTime(long a_eventTime) {
		d_eventTime = a_eventTime;
	}
	
	public void setEventType(int a_eventType) {
		d_eventType = a_eventType;
	}
	
	public void setEventSubType(int a_eventSubType) {
		d_eventSubType = a_eventSubType;
	}
	
	public void setActivityID(String a_activityID) {
		d_activityID = a_activityID;
	}
	
	public void setActivityLabel(String a_activityLabel) {
		d_activityLabel = a_activityLabel;
	}
	
	public long getEventTime() {
		return d_eventTime;
	}
	
	public int getEventType() {
		return d_eventType;
	}
	
	public int getEventSubType() {
		return d_eventSubType;
	}
	
	public int getActivityType() {
		return d_activityType;
	}
	
	public String getActivityID() {
		return d_activityID;
	}
	
	public String getActivityLabel(){
		return d_activityLabel;
	}
	
	public JSONObject toJSON() {
		try {
			SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
			
			JSONObject data = new JSONObject();
			data.put(DynamicApplicationConstants.UID, settings.getString(DynamicApplicationConstants.UID, null));
			data.put(DynamicApplicationConstants.CURRENT_TIME, d_eventTime);
			data.put(DynamicApplicationConstants.EVENT_TYPE, d_eventType);
			data.put(DynamicApplicationConstants.EVENT_SUBTYPE, d_eventSubType);
			data.put(DynamicApplicationConstants.ACTIVITY_ID, d_activityID);
			data.put(DynamicApplicationConstants.ACTIVITY_LABEL, d_activityLabel);
			data.put(DynamicApplicationConstants.ACTIVITY_TYPE, d_activityType);

			return data;
		}
		catch (JSONException e)
		{
			return null;
		}
	}
	
	public HashMap<String,String> getFlatEntries() {
				
		HashMap<String,String> flatEntries = new HashMap<String,String>();
		
		flatEntries.put(d_activityID+"_"+d_eventType+"_log", Long.toString(d_eventTime));
		
		return flatEntries;
	}
}
