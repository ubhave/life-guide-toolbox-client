package org.ubhave.ubhavedynamicapp.conditional;

import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class UbhaveConditionalQuery{
	
	public static String query(String queryString, String param){
		String res = "InvalidQuery";
		
		if(queryString.equals("checkIfItemAccessed"))
			res=new Boolean(InterventionDataAccess.checkIfItemAccessed(new Integer(param))).toString();
		if(queryString.equals("checkIfItemAccessedToday"))
			res=new Boolean(InterventionDataAccess.checkIfItemAccessedToday(new Integer(param))).toString();
		if(queryString.equals("itemAccessedInPeriodCount"))
			res=new Boolean(InterventionDataAccess.itemAccessedInPeriodCount(new Integer(param.substring(0, param.indexOf(","))),new Integer(param.substring(param.indexOf(",")+1)))).toString();
		if(queryString.equals("varLessThanHoursOld"))
			res=new Boolean(InterventionDataAccess.variableLessThanHoursOld(new String(param.substring(0, param.indexOf(","))),new Integer(param.substring(param.indexOf(",")+1)))).toString();
		if(queryString.equals("networkConnectivity")){
			ConnectivityManager cm = (ConnectivityManager) UbhaveApplication.getContext().getSystemService(UbhaveApplication.getContext().CONNECTIVITY_SERVICE);
		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
		        res="true";
		    }
		    res="false";			
		}
		
		return res;
	}
	
}