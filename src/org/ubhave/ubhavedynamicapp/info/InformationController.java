/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.info;

import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.AbstractComponentController;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveActivity;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.info.data.AbstractInformation;
import org.ubhave.ubhavedynamicapp.info.data.InformationList;
import org.ubhave.ubhavedynamicapp.info.data.InformationListLoader;
import org.ubhave.ubhavedynamicapp.info.ui.StaticTextActivity;
import org.ubhave.ubhavedynamicapp.info.ui.lists.InfoListActivity;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public class InformationController extends AbstractComponentController
{
	public final static String CURRENT_INFORMATION = "info";
	public final static String CURRENT_INFORMATION_ID = "infoId";
	public final static String CURRENT_INFORMATION_PARENT_STATUS = "parentStatus";
	public final static String SELECTED_CHILD = "child";
	private final static int INFO_REQUEST_CODE = 101;
	private final static String TAG = "InformationController";
	
	//private InformationList infoList;
	private AbstractInformation currentInformation;
	private final Activity context;	
	
	private String infoID;
	
	public final JSONObject currentInfoJSON;
	
	public InformationController(Activity c, JSONObject infoJO, String id, DynamicApplicationController dynamicApplicationController)
	{
		this.context = c;
		infoID = id;
		currentInfoJSON = infoJO;
		
		setAppController(dynamicApplicationController);
	}
	
	@Override
	public void start()
	{
		try
		{
			currentInformation = InformationListLoader.loadInformation(currentInfoJSON, infoID);
			
			//currentInformation = infoList;
			showCurrentInformation();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void showCurrentInformation()
	{
		if (currentInformation != null)
		{
			Intent intent = null;
			int type = currentInformation.getType();
						
			if (type == AbstractInformation.INFO_TEXT)
			{
				intent = new Intent(context, StaticTextActivity.class);
			}
			else if (type == AbstractInformation.INFO_LIST)
			{
				intent = new Intent(context, InfoListActivity.class);
			}
			
			if (intent != null)
			{
				intent.putExtra(CURRENT_INFORMATION, currentInformation);
				
				intent.putExtra(CURRENT_INFORMATION_ID, infoID);
				
				if(currentInformation.getParent()!=null)
					intent.putExtra(CURRENT_INFORMATION_PARENT_STATUS, true);
				else
					intent.putExtra(CURRENT_INFORMATION_PARENT_STATUS, false);
				
				intent.putExtra(UbhaveActivity.SKIPABLE, isSkipable());
				
				context.startActivityForResult(intent, INFO_REQUEST_CODE);
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.i(TAG,"Info OAR: "+resultCode);
		if (resultCode == DynamicApplicationController.RESULT_INFO_OK)
		{
			int selectedChild = data.getIntExtra(SELECTED_CHILD, 0);
			currentInformation = ((InformationList) currentInformation).get(selectedChild);
			showCurrentInformation();
		}
		else if(resultCode ==DynamicApplicationController.RESULT_ACTIONBAR_HOME){
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_AND_JUMP_TO_FRONT);
		}
		else // back pressed
		{
			AbstractInformation parent = currentInformation.getParent();
			if (parent != null)
			{
				currentInformation = parent;
				showCurrentInformation();
			}
			else				
			{
				if(resultCode == DynamicApplicationController.RESULT_INFO_CANCELED){
					getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE);
					Log.i("UDA", "Info Parent Null");
					Log.d("Main", "Parent is null");
				}
				else{
					getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_BACK);
				}
			}
		}
	}
}
