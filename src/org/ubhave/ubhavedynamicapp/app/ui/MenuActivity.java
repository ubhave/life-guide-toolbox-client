/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.app.ui;

import org.ubhave.ubhavedynamicapp.InterventionPreferences;
import org.ubhave.ubhavedynamicapp.InterventionResourceManager;
import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.UbhaveActivity;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.app.data.menu.UbhaveMenuItem;
import org.ubhave.ubhavedynamicapp.background.KeepAliveAlarm;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class MenuActivity extends UbhaveActivity
{
	private DynamicApplicationController applicationController;

	private static final String TAG = "MenuActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
		
		//TODO: this is bad - we shouldn't reinitialise the controller with every menu. The controller should pass a Menu object to the activity as it does for other activities.
		try
		{
			applicationController = new DynamicApplicationController(this);
			applicationController.checkAndResolveLaunchActivity();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			super.logout();
		}
		
		// Setup
		if(applicationController.getInterventionLogo()==null){
			((TextView) findViewById(R.id.homeAppName)).setText(applicationController.getInterventionTitle());
			findViewById(R.id.appLogo).setVisibility(View.GONE);
		}
		else{
			((ImageView) findViewById(R.id.appLogo)).setImageURI(InterventionResourceManager.getImageResourceURI(applicationController.getInterventionLogo()));
			findViewById(R.id.homeAppName).setVisibility(View.GONE);
		}
		
		
		addButtons();
		
		
		// Start background services as required
		Log.d(TAG, "Starting background services");
		KeepAliveAlarm.startAlarm(this);

		Bundle callParams = getIntent().getExtras();
		if (callParams != null) {
			applicationController.processCall(callParams);
		}
//		daControl = new DynamicApplicationController(this);
//		Bundle callParams = getIntent().getExtras();
//		daControl.processCall(callParams);
	}
	
	private void addButtons()
	{
		
		LinearLayout buttonLayout = (LinearLayout) findViewById(R.id.componentButtonLayout);
		
		buttonLayout.removeAllViews();
		
		//String[] buttonTexts = applicationController.getItems();
		for(UbhaveMenuItem item:applicationController.getCurrentMenuItems()){
			switch(item.getNavState()){
			case 0:
				break;
			case 1:
				buttonLayout.addView(createButton(item.getLabel(),item.getID()));
				break;
			case 2:
				Button greyButton = createButton(item.getLabel(),item.getID());
				greyButton.setEnabled(false);
				buttonLayout.addView(greyButton);
				break;
			}
			
		}
		
		/*for (String buttonText : buttonTexts)
		{
			buttonLayout.addView(createButton(buttonText));
		}*/
	}
	
	private Button createButton(final String buttonText, final String activityID)
	{
		Button button = (Button) View.inflate(this, R.layout.activity_home_button, null);
		button.setText(buttonText);
		button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				//applicationController.doButtonTextAction(buttonText);
				applicationController.launchActivity(activityID);
			}
		});
		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(100, 100, 100, 100);
		button.setLayoutParams(params);
		button.setPadding(-100, -100, -100, -100);
		
		return button;
	
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		applicationController.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		Log.d(TAG, "onNewIntent");
		super.onNewIntent(intent);
		Bundle callParams = intent.getExtras();
		applicationController.processCall(callParams);
		//intent.removeExtra(DynamicApplicationConstants.NOTIFICATION_EXTRA);
		//intent.removeExtra(DynamicApplicationConstants.CONTROL_INSTRUCTION);
	}
		
	

	@Override
	protected String getActivityType() {
		return TAG;
	}

	@Override
	protected String getActivityID() {
		return InterventionPreferences.getInterventionId();
	}

	@Override
	protected String getActivityExtra() {
		return null;
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		addButtons();
	}
	

	
}
