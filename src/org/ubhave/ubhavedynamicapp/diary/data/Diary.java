/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.diary.data;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;

import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;

public class Diary implements Parcelable
{
	private final String diaryID;
	private final String date;
	private final ArrayList<String> fields;
	private final HashMap<String, String> fieldContent;
	private final String title;
	private final String fieldHeader;
	private final String text;

/*	private final static String USER = "userid";
	private final static String CURRENT_TIME = "time";
	private final static String DIARY_ID = "diray_id";
	private final static String DIARY_ENTRY = "entry";
	private final static String DIARY_FIELDS = "fields";
	private final static String DIARY_FIELDS_CONTENT = "fields_content";
	private final static String DIARY_DATE = "date";
	private final static String DIARY_TITLE = "title";
	private final static String DIARY_FIELD_HEADER = "field_header";
	private final static String DIARY_TEXT = "field_text";*/

	public Diary(String diaryID, String date, String t, String txt, String fh, ArrayList<String> fields)
	{
		fieldContent = new HashMap<String, String>();
		this.fields = fields;
		this.date = date;
		this.title = t;
		this.fieldHeader = fh;
		this.text = txt;
		this.diaryID = diaryID;
	}
	
	public String getDate()
	{
		return date;
	}
	
	public String getFieldHeader()
	{
		return fieldHeader;
	}

	public ArrayList<String> getFields()
	{
		return fields;
	}
	
	public String getField(int position)
	{
		return fields.get(position);
	}

	public void put(String field, String content)
	{
		fieldContent.put(field, content);
	}

	public String getContent(String field)
	{
		return fieldContent.get(field);
	}

	public String getTitle()
	{
		return title;
	}

	public String getText()
	{
		return text;
	}
	
	public boolean isEmpty()
	{
		for (String content : fieldContent.values())
		{
			if (content != null)
			{
				return false;
			}
		}
		return true;
	}

	public HashMap<String,String> getFlatEntries() {
		
		HashMap<String,String> flatEntries = new HashMap<String,String>();
		
		for (String fieldName : fields) {
			String answer = fieldContent.get(fieldName);
			flatEntries.put(diaryID+"_"+fieldName, answer);
		}
		return flatEntries;
	}
		
	@Override
	public int describeContents()
	{
		return 0;
	}

	public static final Parcelable.Creator<Diary> CREATOR = new Parcelable.Creator<Diary>()
	{
		public Diary createFromParcel(Parcel in)
		{
			return new Diary(in);
		}

		public Diary[] newArray(int size)
		{
			return new Diary[size];
		}
	};

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(diaryID);
		dest.writeString(date);
		dest.writeString(title);
		dest.writeString(text);
		dest.writeString(fieldHeader);
		dest.writeStringList(fields);
		for (String key : fields)
		{
			dest.writeString(fieldContent.get(key));
		}
	}

	public Diary(Parcel in)
	{
		diaryID = in.readString();
		date = in.readString();
		title = in.readString();
		text = in.readString();
		fieldHeader = in.readString();
		
		fields = new ArrayList<String>();
		in.readStringList(fields);
		
		fieldContent = new HashMap<String, String>();
		for (String key : fields)
		{
			fieldContent.put(key, in.readString());
		}
	}
	
	public JSONObject toJSON()
	{
		try
		{
			SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
			JSONObject data = new JSONObject();
			data.put(DynamicApplicationConstants.UID, settings.getString(DynamicApplicationConstants.UID, null));
			data.put(DynamicApplicationConstants.CURRENT_TIME, System.currentTimeMillis());

			JSONObject fieldsContentObject = new JSONObject();
			for (String field : fields) {
				fieldsContentObject.put(field, fieldContent.get(field));
			}
			
			data.put(DynamicApplicationConstants.DIARY_FIELDS_CONTENT, fieldsContentObject);
			data.put(DynamicApplicationConstants.DIARY_DATE, date);
			data.put(DynamicApplicationConstants.DIARY_ID, diaryID);
/*			json.put(DIARY_FIELDS, fieldsArray);
			json.put(DIARY_FIELD_HEADER, fieldHeader);
			json.put(DIARY_TITLE, title);
			json.put(DIARY_ID, diaryID);
			json.put(DIARY_TEXT, text);
			data.put(DIARY_ENTRY, json);
*/			
			return data;
		}
		catch (JSONException e)
		{
			return null;
		}
	}

}