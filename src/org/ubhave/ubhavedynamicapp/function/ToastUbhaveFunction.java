package org.ubhave.ubhavedynamicapp.function;

import org.ubhave.ubhavedynamicapp.UbhaveApplication;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditional;

import android.widget.Toast;

public class ToastUbhaveFunction extends UbhaveFunction{

	private String msg;
	public ToastUbhaveFunction(String fid, String msg){
		this.msg = msg;
		setCondition(null);
		setId(fid);
	}
	
	public ToastUbhaveFunction(String fid, String msg, UbhaveConditional cond){
		this.msg = msg;
		setCondition(cond);
		setId(fid);
	}
	
	@Override
	public void run(DynamicApplicationController dac) {
		if(!this.evalCondition())
			return;
		else
			Toast.makeText(UbhaveApplication.getContext(), 
						msg, 
						Toast.LENGTH_LONG).show();
	}


}
