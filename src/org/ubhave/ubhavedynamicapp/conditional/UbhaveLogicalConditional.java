/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.conditional;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class UbhaveLogicalConditional extends UbhaveConditional{

	ArrayList<UbhaveConditional> conditionals;
	
	final static String OR = "OR";
	final static String AND = "AND";
	
	public UbhaveLogicalConditional(String op, ArrayList<UbhaveConditional> conds){
		operand = op;
		conditionals = conds;
	}
	
	@Override
	public boolean evaluate() {
		if(operand.equals(OR)){
			for(UbhaveConditional cond:conditionals){
				if(cond.evaluate()==true)
					return true;
			}
			return false;
		}
		else if(operand.equals(AND)){
			for(UbhaveConditional cond:conditionals){
				if(cond.evaluate()==false)
					return false;
			}
			return true;
		}
		return false;
			
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(type);
		dest.writeString(operand);
		dest.writeTypedList(conditionals);
	}
	
	public UbhaveLogicalConditional(Parcel in) {
		type = in.readInt();
		operand = in.readString();
		conditionals = new ArrayList<UbhaveConditional>();
		in.readTypedList(conditionals, UbhaveConditional.CREATOR);
	}
	
	public static final Parcelable.Creator<UbhaveLogicalConditional> CREATOR = new Parcelable.Creator<UbhaveLogicalConditional>()
			{
				public UbhaveLogicalConditional createFromParcel(Parcel in)
				{
					return new UbhaveLogicalConditional(in);
				}

				public UbhaveLogicalConditional[] newArray(int size)
				{
					return new UbhaveLogicalConditional[size];
				}
			};
			
	
	
}