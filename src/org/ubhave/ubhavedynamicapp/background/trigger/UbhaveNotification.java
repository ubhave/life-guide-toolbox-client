/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.background.trigger;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;

import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;

public class UbhaveNotification implements Parcelable {


	
	private int d_triggerNumber;
	private String d_triggerName;
	private String d_triggerAction;
	private boolean d_trained;
	private boolean d_random;
	private int d_dayOfWeek;
	private int d_hourOfDay;
	private long d_timeMillis;
	
	public UbhaveNotification(int a_triggerID, 
							String a_triggerName, 
							String a_triggerAction,
							boolean a_trained,
							boolean a_random,
							int a_dayOfWeek, 
							int a_hourOfDay, 
							long a_timeMillis){
		d_triggerNumber = a_triggerID;
		d_triggerName = a_triggerName;
		d_triggerAction = a_triggerAction;
		d_trained = a_trained;
		d_random = a_random;
		d_dayOfWeek = a_dayOfWeek;
		d_hourOfDay = a_hourOfDay;
		d_timeMillis = a_timeMillis;
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(d_triggerNumber);
		dest.writeString(d_triggerName);
		dest.writeString(d_triggerAction);
		dest.writeByte((byte) (d_trained ? 1 : 0));
		dest.writeByte((byte) (d_random ? 1 : 0));
		dest.writeInt(d_dayOfWeek);
		dest.writeInt(d_hourOfDay);
		dest.writeLong(d_timeMillis);
	}

	public static final Parcelable.Creator<UbhaveNotification> CREATOR = 
			new Parcelable.Creator<UbhaveNotification>(){

		@Override
		public UbhaveNotification createFromParcel(Parcel source) {
			return new UbhaveNotification(source);
		}

		@Override
		public UbhaveNotification[] newArray(int size) {
			return new UbhaveNotification[size];
		}
		
	};
	
	public UbhaveNotification(Parcel a_parcel) {
		d_triggerNumber = a_parcel.readInt();
		d_triggerName = a_parcel.readString();
		d_triggerAction = a_parcel.readString();
		d_trained = a_parcel.readByte() != 0;
		d_random = a_parcel.readByte() != 0;
		d_dayOfWeek = a_parcel.readInt();
		d_hourOfDay = a_parcel.readInt();
		d_timeMillis = a_parcel.readLong();
	}
	
	public JSONObject toJSON() {
		try
		{
			SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
			JSONObject notifObject = new JSONObject();
			notifObject.put(DynamicApplicationConstants.UID, settings.getString(DynamicApplicationConstants.UID, null));
			notifObject.put(DynamicApplicationConstants.TRIGGER_ID, d_triggerNumber);
			notifObject.put(DynamicApplicationConstants.TRIGGER_NAME, d_triggerName);
			notifObject.put(DynamicApplicationConstants.TRIGGER_ACTION, d_triggerAction);
			notifObject.put(DynamicApplicationConstants.TRIGGER_TRAINED, d_trained);
			notifObject.put(DynamicApplicationConstants.TRIGGER_RANDOM, d_random);
			notifObject.put(DynamicApplicationConstants.CURRENT_TIME, d_timeMillis);
			notifObject.put(DynamicApplicationConstants.DAY_OF_WEEK, d_dayOfWeek);
			notifObject.put(DynamicApplicationConstants.HOUR_OF_DAY, d_hourOfDay);
			return notifObject;
		}
		catch (JSONException e)
		{
			return null;
		}
	}
}
