package org.ubhave.ubhavedynamicapp.data.menu;

import java.util.ArrayList;

public class Menu{
	String label;
	ArrayList<MenuItem> items;
	
	public Menu(String l, ArrayList<MenuItem> i){
		label=l;
		items=i;
	}
	
	public String getLabel(){
		return label;
	}
	
	public ArrayList<MenuItem> getItems(){
		return items;		
	}
	
	
}