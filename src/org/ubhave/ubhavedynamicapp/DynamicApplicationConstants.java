/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp;

import java.util.ArrayList;

public class DynamicApplicationConstants {

	public static final String APP_VERSION = "version_code";	
	public static final String INTERVENTION_ID = "intervention_id";	
	public static final String ITEM_TYPE = "item_type";	
	public static final String UID = "uid";	
	public final static String CURRENT_TIME = "time";
	
	// UsageLog variables	
	public final static String EVENT_TYPE = "event_type";
	public final static String EVENT_SUBTYPE = "event_subtype";
	public final static String ACTIVITY_ID = "activity_id";
	public final static String ACTIVITY_TYPE = "activity_type";
	public final static String ACTIVITY_LABEL = "activity_label";
	public static final String INTERACTION_LABEL = "interactionLabel";
	public static final String INTERACTION_MODE = "interactionMode";
	
	public static final int EVENT_ACTIVITY_START = 0;
	public static final int EVENT_ACTIVITY_FINISH = 1;
	public static final int EVENT_INTERACTION = 2;
	
	public static final int ACTIVITY_FINISH_COMPLETE = 0;
	public static final int ACTIVITY_FINISH_BACK = 1;
	public static final int ACTIVITY_FINISH_SKIPPED = 2;
	public static final int ACTIVITY_FINISH_COMPLETE_AND_JUMP = 3;
	public static final int ACTIVITY_FINISH_COMPLETE_AND_JUMP_TO_FRONT = 4;
	public static final int ACTIVITY_FINISH_STAGE_EXECUTE_FUNCTIONS = 5;
	public static final int ACTIVITY_FINISH_COMPLETE_NO_FUNCTIONS = 6;
	public static final int INTERACTION_BUTTON = 5;
	public static final int INTERACTION_SCROLL = 6;
	public static final int INTERACTION_TOGGLE = 7;
	
	public static int TYPE_MENU = 0;
	public static final int TYPE_SURVEY = 1;
	public static final int TYPE_DIARY = 2;
	public static final int TYPE_INFO = 3;
	public static final int TYPE_SETTINGS = 4;
	public static final int TYPE_SEQUENCE = 5;
	public static final int TYPE_PLANNER = 6;
	public static final int TYPE_PLANNER_REVIEW = 7;
	
	// For logging purposes only
	public static final int TYPE_NOTIFICATION = 10;
	public static final int TYPE_APP_USAGE = 11;
	public static final int TYPE_VARIABLE = 12;
	public static final int TYPE_NOTIFICATION_ANSWER = 13;
	public static final int TYPE_LEARNER = 14;

	// Diary variables
	public final static String DIARY_FIELDS_CONTENT = "fields_content";
	public final static String DIARY_DATE = "date";
	public static final String DIARY_ID = "diary_id";
	
	// Survey variables
	public final static String ANSWERS = "answers";
	public static final String SURVEY_ID = "survey_id";
	public static final String ANSWER = "answer";
	public static final String QUESTION_ID = "question_id";
	public static final String QUESTION_TYPE = "question_type";

	// Planner variables
	public static final String PLAN_ID = "plan_id";
	public static final String PLAN_TYPE = "plan_type";	
	public static final String PLAN_FIELDS = "plan_fields";
	public final static String PLAN_SET = "plan_set";
	public final static String PLAN_REVIEW_RESULTS = "plan_review_results";
	public static final String PLAN_REVIEW_RESULTS_KEYS = "plan_review_results_keys";
	public static final String PLAN_REVIEW_RESULTS_VALUES = "plan_review_results_values";
	public final static String SCORE = "score";
	public static final String PLAN = "plan";
	public static final String PLAN_RESULT = "plan_result";
	public final static String PLANNER_KEY = "planner_key";
	public final static String PLANS = "plans";
	
	// Trigger (Notification) variables
	public static final String TRIGGER_NAME = "trigger_name";
	public static final String TRIGGER_ID = "trigger_number";
	public static final String TRIGGER_ACTION = "action_type";
	public static final String TRIGGER_TRAINED = "trained";
	public static final String TRIGGER_RANDOM = "random";
	public static final String DAY_OF_WEEK = "day_of_week";
	public static final String HOUR_OF_DAY = "hour_of_day";	
	public static final String ANSWER_VALUE = "answer";	
	public static final String WEEK = "week";	

	
	// Trigger shared prefs
	public static final String TRIGGER_SWITCH = "triggerSwitch";

	public static final String SERVICE_INSTRUCTION = "service_instruction";
	public static final int CHECK_TRIGGERS = 1;
	public static final int TRAIN_INTER_LEARNER = 2;
	public static final String TRAIN_WITH_VALUE = "train_with_value";
	
	public static final String CONTROL_INSTRUCTION = "control_instruction";
	public static final String NOTIFICATION_EXTRA = "notification_extra";
	public static final String TRIGGER_TYPE = "trigger_type";	
	public static final String CURRENT_TRIGGER_NO = "triggerNumber";
	// interruptibility trigger notification number
	// needed for training purposes 
	public static final String CURRENT_INTER_TRIGGER_NO = "interTriggerNumber"; 
	public static final String TRIGGER_TIME = "interTriggerTime"; 
	
	public static final String LAST_TRIGGER_TYPE = "lastTriggerType"; 
	public static final String LAST_TRIGGER_TIME = "lastTriggerTime"; 
	public static final String LAST_CLASSIFIER_XMIT_WEEK = "last_classifier_xmit_week"; 

	
	public static final String TRAINED_INTER_TRIGGER_INSTANCE = "trainedInterTriggerInstance";
	// if a notification is answered within 20 mins we label is as "successful" 
	public static final long INTER_TRIGGER_THOLD_TIME = 20*60*1000;
	
	//Max two days without a notification on the interruptibility trigger
	public static final long MAX_NO_TRIGGER_TIME = 2*24*60*60*1000; 

	public static final String CURRENT_INTERVENTION_CONFIG = "current_intervention.json";
	public static final String CURRENT_INTERVENTION_RES = "current_intervention_res.zip";
	public static final int START_APP = 1000;
	public static final int PROCESS_NOTIFICATION = 1001;
	
	public static final int ACTION_NOTIFICATION = 1000;
	public static final int ACTION_SENSING = 1001;
	public static final int ACTION_BOTH = 1002;

	public static final int NOTIFICATION_DIARY = 0;
	public static final int NOTIFICATION_SURVEY = 1;
	public static final int NOTIFICATION_INFO = 2;
	
	public static final int NOTIFICATION_CODE = 100;
	public static final String ACTION_SERVICE_ALARM_RECEIVER = "org.ubhave.ubhavedynamicapp.background.KEEP_ALIVE_ALARM";
	public static final long WAKE_UP_PERIOD = 20*60*1000;

	public static final String AUTH_TOKEN = "token";	
	public static final String AUTH_TOKEN_EXPIRES = "token_expires";
	public static final long AUTH_TOKEN_LIFETIME = 1000L * 60 * 60;

	// NOTE: 
	// Hardcoded random trigger firing probability: p 
	// This is hard to calculate as there is:
	// - Tint: a user-defined period in which we can interrupt (default 14h/day)
	// - Tgap: a minimum gap between interruptions (default 240 mins)
	// - Tsample: a sampling interval (default 11 mins)
	// - Nmax: maximum number of interruptions per day
	// - Nexp: expected number of interruptions per day
	// Without considering Nmax and Tgap:
	// p = Nexp * Tsample / Tint;
	// This is for Tint = 60*14, Tsample = 11 and Nexp = 4;
	// public static final double RANDOM_TRIGGER_P = 0.052380952;
	
	// This is for Tint = 60*12, Tsample = 21 and Nexp = 3;
	public static final double RANDOM_TRIGGER_P = 0.08823529411;	
	
	public static final int MIN_INTERRUPTIBILITY_POINTS = 20;

	public static final long ACCELEROMETER_SENSING_WINDOW_LONG = 60*1000;
	public static final long ACCELEROMETER_SLEEP_INTERVAL = 20*60*1000; // TODO: should be 20 min
	public static final long LOCATION_SLEEP_INTERVAL = 20*60*1000; // TODO: should be 20 min
	
	public static final int DEFAULT_TIME_PICKER_VALUE = 60*8; // in minutes
	public static final int DEFAULT_BOOL_VALUE = 1; // 1- checked, 0 - unchecked
	
	public static final String ESCAPE_TAG = "\\";
	public static final String MARKUP_TAG = "$";
	
	public static final String AUTH_SCOPE = AuthScopeKey.AUTH_SCOPE;
	
	public static final String LOCAL_STORAGE_HOME_DIRECTORY_NAME = "LifeGuideToolbox";

	public static final String UL_DATA_URL = "http://waisvm-cah07r.ecs.soton.ac.uk/UbhaveDynamicApp/ul_data.php";	
	public static final String DL_INTERVENTION_URL = "http://waisvm-cah07r.ecs.soton.ac.uk/UbhaveDynamicApp/dl_intervention.php";
	public static final String DL_INTERVENTION_LIST_URL = "https://ubhave.ecs.soton.ac.uk/UbhaveDynamicApp/interventions/";
	
	public static final String REST_API_CONTENT_TYPE_JSON = "application/json";
	public static final String REST_API_CONTENT_TYPE_ZIP = "application/zip";

	public static final long UPLOAD_INTERVAL_MILLIS = 1000L * 60 * 40;	
	// TODO: copied from Neal's code, not sure why 30 sec is a good value
	public static final long FILE_LIFE_MILLIS = 1000L * 30;
	public static final String DEFAULT_SEQUENCE_ID = "0";
	public static final String DEFAULT_ITEM_ID = "0";
	
	public static final String INTERACTION_INFO_AUDIO_PAUSE = "Info-Audio-Pause";
	public static final String INTERACTION_INFO_AUDIO_PLAY = "Info-Audio-Play";
	
	public static final Object RANDOMISATION = "randomization";
	public static final String RANDOMISATIONENDPOINT = "randomization-endpoint";
	public static final String ENDPOINT = "endpoint";
	public static final String RANDOMISATIONVARS = "randomization-vars";
	public static final String VARS = "variables";
	public static final String GET_RANDOMISATION_DEFAULT_FAILURE_STRING = "RANDOMISATIONFAIL";
	
	public static final String WRAPPER_GROUPS = "groups";
	public static final String WRAPPER_CONTENT = "content";
	
	public static ArrayList<String> allTriggerParams() {
		ArrayList<String> keys = new ArrayList<String>();
		keys.add(TRIGGER_SWITCH);		
		return keys;
	}

}