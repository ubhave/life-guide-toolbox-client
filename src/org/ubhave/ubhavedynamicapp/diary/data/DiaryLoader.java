/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.diary.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.JSONLoader;

public class DiaryLoader extends JSONLoader
{
	private final static String DIARY = "diary";
	private final static String FIELDS = "labels";
	private final static String FIELD_HEADER = "label_header";
	private final static String DIARY_TITLE = "title";
	private final static String DIARY_TEXT = "text";
	private final static String DIARY_ID = "diary_id";

	public static DiaryMap loadDiary(JSONObject diaryJO, String diaryID) throws JSONException, NullPointerException
	{
		try
		{	
			JSONObject diaryJSON = diaryJO;
			
			if (diaryJSON != null)
			{
				//String diary_id = diaryJSON.getString(DiaryLoader.DIARY_ID);
				String diary_title = diaryJSON.getString(DiaryLoader.DIARY_TITLE);
				String diary_text = diaryJSON.getString(DiaryLoader.DIARY_TEXT);
				String field_header = diaryJSON.getString(DiaryLoader.FIELD_HEADER);
				JSONArray fieldsJA = diaryJSON.getJSONArray(DiaryLoader.FIELDS);
				ArrayList<String> fields = new ArrayList<String>();
				for (int k = 0; k < fieldsJA.length(); k++)
				{
					String field = fieldsJA.getString(k);
					fields.add(field);
				}

				DiaryMap diary = new DiaryMap(diaryID, diary_title, diary_text, field_header, fields);
				return diary;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		throw new NullPointerException();
	}
}