/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey.data.qs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class RatingList extends AbstractQuestion
{
	private final RatingQuestion[] questions;
	private static final String RATINGS = "ratings";

	public RatingList(String id, JSONObject data, String title, String text, int answerType, String fText) throws JSONException
	{
		super(id, answerType, title, text, fText);

		JSONArray ratingListJSON = (JSONArray) data.get(RATINGS);
		questions = new RatingQuestion[ratingListJSON.length()];
		for (int i = 0; i < ratingListJSON.length(); i++)
		{
			JSONObject ratingJSON = (JSONObject) ratingListJSON.get(i);
			questions[i] = new RatingQuestion(ratingJSON);
		}
	}

	private RatingList(Parcel in)
	{
		super(in);
		questions = new RatingQuestion[in.readInt()];
		for (int i = 0; i < questions.length; i++)
		{
			questions[i] = in.readParcelable(RatingQuestion.class.getClassLoader());
		}
	}

	public RatingQuestion[] getQuestions()
	{
		return questions;
	}

	public static final Parcelable.Creator<RatingList> CREATOR = new Parcelable.Creator<RatingList>()
	{
		public RatingList createFromParcel(Parcel in)
		{
			return new RatingList(in);
		}

		public RatingList[] newArray(int size)
		{
			return new RatingList[size];
		}
	};

	@Override
	public void writeToParcel(Parcel out, int flags)
	{
		super.writeToParcel(out, flags);
		out.writeInt(questions.length);
		for (RatingQuestion question : questions)
		{
			out.writeParcelable(question, flags);
		}
	}

}
