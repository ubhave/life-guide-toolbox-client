/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp;

import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.app.ui.LauncherActivity;
import org.ubhave.ubhavedynamicapp.log.ApplicationDataLogger;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;

import com.ubhave.datahandler.except.DataHandlerException;

public abstract class UbhaveActivity extends Activity
{
	
protected static final String TAG = "UbhaveActivity";

/*	private final static String USER = "userid";
	private final static String CURRENT_TIME = "time";
	private final static String ACTION = "action";
	private final static String ACTIVITY_ID = "activityID";
	private final static String ACTIVITY_TYPE = "activityID";
	
	// This can be used for surveyID when we have a specific question
	// that already occupies ACTIVITY_ID field
	private final static String ACTIVITY_EXTRA = "activityExtra";
*/

	public static final String SKIPABLE = "skipable";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setTheme(InterventionDataAccess.getCurrentInterventionTheme());
		
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		getActionBar().setHomeButtonEnabled(true);
	}
	
	@Override
	protected void onStart() {

		/*try {
			ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
			JSONObject data = new JSONObject();
			data.put(CURRENT_TIME, System.currentTimeMillis());
			data.put(USER,""); // TODO replace with your own user identifier
			data.put(ACTIVITY_ID, getActivityID());
			data.put(ACTIVITY_TYPE, getActivityType());
			data.put(ACTIVITY_EXTRA, getActivityExtra());
			data.put(ACTION, "start");
			logger.logAppUsage(data);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}*/
		
		super.onStart();
		
	}
	
	@Override
	protected void onPause() {

		/*try {
			ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
			JSONObject data = new JSONObject();
			data.put(CURRENT_TIME, System.currentTimeMillis());
			data.put(USER,""); // TODO replace with your own user identifier
			data.put(ACTIVITY_ID, getActivityID());
			data.put(ACTIVITY_TYPE, getActivityType());
			data.put(ACTIVITY_EXTRA, getActivityExtra());
			data.put(ACTION, "pause");
			logger.logAppUsage(data);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}*/
		
		super.onPause();
	}
	
	@Override
	public void setContentView(int layout)
	{
		//boolean windowFeatureAllowed = this.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.setContentView(layout);
		/*if (windowFeatureAllowed)
		{
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
		}*/
	}

	protected void setActivityTitle(String title)
	{
		if (title == null)
		{
			//title = getResources().getString(R.string.app_name);
			title = InterventionDataAccess.getCurrentInterventionName();
		}
		ActionBar ab = getActionBar();
		ab.setTitle(title);
		ab.setDisplayShowHomeEnabled(true);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	// action with ID action_refresh was selected
	    	case android.R.id.home:
	    		actionBarHome();
	    		break;
	    	case R.id.menu_logout:
	    		logout();
	    		break;	    		
	    	case R.id.menu_settings:
	    		break;
		    default:
		    	break;
		    }
		
		    return true;
	  } 
	
	private void actionBarHome() {
		/*try {
			DynamicApplicationController dac = new DynamicApplicationController(this);
			dac.activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_AND_JUMP_TO_FRONT);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		if (skipable()) {
			this.setResult(DynamicApplicationController.RESULT_ACTIONBAR_HOME);
			this.finish();
		}
	}
	
	public void logout()
	{
		
		new Thread(new Runnable() {
		    public void run() {
		    	ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
				try {
					Log.d(TAG, "Flushing data");
					logger.flush();
				} catch (DataHandlerException e) {
					e.printStackTrace();
				}
		    }
		  }).start();

		InterventionPreferences.removeIntervention();
		startActivity(new Intent(this, LauncherActivity.class));
		finish();
	}
	
	
	protected abstract String getActivityType();
	
	protected abstract String getActivityID();
	
	protected abstract String getActivityExtra();

	protected boolean skipable() {
		return getIntent().getBooleanExtra(SKIPABLE, true);
	}
	
	@Override
	protected void onResume() {
		super.onResume();		
	}
	
}