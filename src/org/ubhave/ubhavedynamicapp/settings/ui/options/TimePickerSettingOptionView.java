/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.settings.ui.options;

import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.settings.data.SettingOption;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TimePicker;


public class TimePickerSettingOptionView extends DialogPreference{

	private static final String TAG = "TimePickerSettingOptionView";

	
	private TimePicker timePicker;
	//private static final int DEFAULT_HOUR = 8;
	//private static final int DEFAULT_MINUTE = 0;
	
	public TimePickerSettingOptionView(Context context, AttributeSet attrs, SettingOption option) {
		super(context, attrs);
		setKey(option.getOptionKey());
		setTitle(option.getOptionTitle());
		setSummary(option.getOptionDescription());
		setPersistent(true);		
		//setDefaultValue(option.getOptionDefault());
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(context);
		
		if (!settings.contains(getKey())) {
			Log.d(TAG, "Default value set "+option.getOptionDefault());
			SharedPreferences.Editor editor = settings.edit();
			editor.putInt(getKey(), option.getOptionDefault());
			persistInt(option.getOptionDefault());
			editor.commit();
			
		} else {
			Log.d(TAG, "Default value already exists");
		}
		this.setDialogLayoutResource(R.layout.time_preference);

	}
	
	@Override
	public void onBindDialogView(View view) {
		super.onBindDialogView(view);
		
		timePicker = (TimePicker) view.findViewById(R.id.prefTimePicker);
		timePicker.setIs24HourView(DateFormat.is24HourFormat(timePicker.getContext()));
		int timeMin = getSharedPreferences().getInt(getKey(), DynamicApplicationConstants.DEFAULT_TIME_PICKER_VALUE);		
		Log.d(TAG, "Time min read "+timeMin);
		
		int hour = (int) Math.floor(timeMin/60);
		int min = timeMin % 60;
		timePicker.setCurrentHour(hour);
		timePicker.setCurrentMinute(min);
	}
	
	@Override
	protected void onDialogClosed(boolean okToSave) {
		super.onDialogClosed(okToSave);
		if (okToSave) {
			int hour = timePicker.getCurrentHour();
			int minute = timePicker.getCurrentMinute() + 60*hour;
			
			if (callChangeListener(minute)) {
				persistInt(minute);
			}
		}
	}
	
	
}