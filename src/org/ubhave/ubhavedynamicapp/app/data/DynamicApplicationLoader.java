/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.app.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.JSONLoader;
import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.app.data.menu.MenuLoader;
import org.ubhave.ubhavedynamicapp.app.data.menu.UbhaveMenuItem;
import org.ubhave.ubhavedynamicapp.function.UbhaveFunction;
import org.ubhave.ubhavedynamicapp.function.UbhaveFunctionLoader;

public class DynamicApplicationLoader extends JSONLoader{
	
	public static DynamicApplication loadIntervention(JSONObject configJSON) throws Exception
	{
		if(configJSON.has(DynamicApplicationConstants.WRAPPER_GROUPS))
			configJSON = configJSON.getJSONObject(DynamicApplicationConstants.WRAPPER_CONTENT);
				
		String title = configJSON.getString(DynamicApplication.TITLE);
		
		ArrayList<UbhaveMenuItem> content = new ArrayList<UbhaveMenuItem>();
			
		JSONArray contentJA = configJSON.getJSONArray(DynamicApplication.CONTENT);
		
		for(int i = 0; i<contentJA.length(); i++){
			content.add(MenuLoader.loadMenuItem(contentJA.getJSONObject(i)));
		}
		
		String frontId = configJSON.getString(DynamicApplication.FRONT);
		String launchId = frontId;
		String launchF = null;
		
		String modelVersion = "0";
			
		if(configJSON.has(DynamicApplication.MODEL_VERSION))
			modelVersion=configJSON.getString(DynamicApplication.MODEL_VERSION);
		
		if(configJSON.has(DynamicApplication.LAUNCH_ITEM))
			launchId=configJSON.getString(DynamicApplication.LAUNCH_ITEM);
		if(configJSON.has(DynamicApplication.LAUNCH_FUNCTION))
			launchF=configJSON.getString(DynamicApplication.LAUNCH_FUNCTION);
		
		int theme = R.style.Theme;
		if(configJSON.has(DynamicApplication.THEME)){
			String themeName = configJSON.getString(DynamicApplication.THEME);
			
			if(themeName.equals(DynamicApplication.THEME_NEUTRAL))
				theme=R.style.Theme_Neutral;
			if(themeName.equals(DynamicApplication.THEME_HEALTHY))
				theme=R.style.Theme_Healthy;
			
		}
		
		ArrayList<UbhaveFunction> functionPool = new ArrayList<UbhaveFunction>();
		
		if(configJSON.has(DynamicApplication.FUNCTION_POOL)){
			JSONArray functionPoolJA = configJSON.getJSONArray(DynamicApplication.FUNCTION_POOL);
			for(int i = 0;i<functionPoolJA.length();i++){
				functionPool.add(UbhaveFunctionLoader.loadFunction(functionPoolJA.getJSONObject(i)));
			}
		}
		
		DynamicApplication app = new DynamicApplication(title,content,functionPool, frontId, launchId,theme,modelVersion);
		
		app.setLaunchFunction(launchF);
		
		if(configJSON.has(DynamicApplication.LOGO))
			app.setLogo(configJSON.getString(DynamicApplication.LOGO));
		
		if(configJSON.has(DynamicApplication.ICON))
			app.setIcon(configJSON.getString(DynamicApplication.ICON));
		
		if(configJSON.has(DynamicApplication.NOTIF_ICON))
			app.setNotifIcon(configJSON.getString(DynamicApplication.NOTIF_ICON));
		
		return app;
		
	}
	
}
