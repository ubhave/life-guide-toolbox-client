/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner.data;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;

import android.os.Parcel;
import android.os.Parcelable;

public class PlanReviewResult implements Parcelable {

	private Plan plan;
	private int planResult;
	
	public PlanReviewResult(Plan plan, int planResult) {		
		this.plan = plan;
		this.planResult = planResult;
	}
	
	public Plan getPlan() {
		return plan;
	}
	
	public int getPlanResult() {
		return planResult;
	}
	
	public void setPlan(Plan plan){
		this.plan = plan;
	}
	
	public void setPlanResult(int planResult){
		this.planResult = planResult;	
	}

	public JSONObject toJSON(){
		JSONObject json = new JSONObject();
		try {
			json.put(DynamicApplicationConstants.PLAN, plan.toJSON());
			json.put(DynamicApplicationConstants.PLAN_RESULT, planResult);			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)  {
		dest.writeParcelable(plan, flags);
		dest.writeInt(planResult);		
	}
	
	public PlanReviewResult(Parcel in) {
		plan = in.readParcelable(Plan.class.getClassLoader());
		planResult = in.readInt();
	}
	
	public static final Parcelable.Creator<PlanReviewResult> CREATOR = new Parcelable.Creator<PlanReviewResult>(){
		public PlanReviewResult createFromParcel(Parcel in){
			return new PlanReviewResult(in);
		}

		public PlanReviewResult[] newArray(int size) {
			return new PlanReviewResult[size];
		}
	};
	
}
