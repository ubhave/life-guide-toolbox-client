/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.diary.ui;

import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.UbhaveActivity;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.diary.DiaryController;
import org.ubhave.ubhavedynamicapp.diary.data.Diary;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class DiaryActivity extends UbhaveActivity
{
	private static final String TAG = "DiaryActivity";

	private Diary diary;
	private EditText content;
	private int currentCategory;

	private String diaryID;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.diary);

		diaryID = getIntent().getStringExtra(DiaryController.DIARY_ID);
		diary = (Diary) getIntent().getParcelableExtra(DiaryController.DIARY);
		setActivityTitle(diary.getTitle());

		TextView textView = (TextView) findViewById(R.id.diaryText);
		textView.setText(diary.getText()+" "+diary.getDate());
		
		textView = (TextView) findViewById(R.id.fieldHeader);
		textView.setText(diary.getFieldHeader());
		
		currentCategory = 0;
		content = (EditText) findViewById(R.id.diaryContent);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.diary_spinner, diary.getFields());
		Spinner categoryOptions = (Spinner) findViewById(R.id.fieldSpinner);
		categoryOptions.setAdapter(adapter);
		addListeners(categoryOptions);
		initButtons();
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		storeCurrentState();
	}

	private void addListeners(Spinner spinner)
	{
		spinner.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				storeCurrentState();
				return false;
			}
		});
		
		spinner.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3)
			{
				currentCategory = position;
				String key = diary.getField(position);
				String diaryContent = diary.getContent(key);
				if (diaryContent != null)
				{
					content.setText(diaryContent);
				}
				else
				{
					content.setText("");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{}
		});
	}
	
	private void storeCurrentState()
	{
		String diaryContent = content.getText().toString();
		String key = diary.getField(currentCategory);
		diary.put(key, diaryContent);
	}

	private void submit(int resultCode)
	{
		Log.i("UDA", "DA sub resultCode: "+resultCode);
		storeCurrentState();
		
		Intent result = getIntent();
		result.putExtra(DiaryController.DIARY, diary);

		this.setResult(resultCode, result);
		this.finish();
	}
	
	private OnClickListener getButtonListener(final int previous, final int submit, final int next)
	{
		return new OnClickListener()
		{
			@Override
			public void onClick(View button)
			{
				int id = button.getId();
				if (id == previous)
				{
					submit(DynamicApplicationController.RESULT_DIARY_PREV);
				}
				else if (id == next)
				{
					submit(DynamicApplicationController.RESULT_DIARY_NEXT);
				}
				else if (id == submit)
				{
					submit(DynamicApplicationController.RESULT_DIARY_OK);
				}
				
			}
		};
	}

	private void initButtons()
	{
		final Button previous = (Button) findViewById(R.id.diaryNavL);
		final Button submit = (Button) findViewById(R.id.submitButton);
		final Button next = (Button) findViewById(R.id.diaryNavR);

		OnClickListener listener = getButtonListener(previous.getId(), submit.getId(), next.getId());
		previous.setOnClickListener(listener);
		submit.setOnClickListener(listener);
		next.setOnClickListener(listener);
	}

	@Override
	protected String getActivityType() {
		return TAG;
	}

	@Override
	protected String getActivityID() {
		return diaryID;
	}

	@Override
	protected String getActivityExtra() {
		return null;
	}

	
}