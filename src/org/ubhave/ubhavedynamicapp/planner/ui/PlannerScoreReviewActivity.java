/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.UbhaveActivity;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.planner.PlannerReviewController;
import org.ubhave.ubhavedynamicapp.planner.data.Plan;
import org.ubhave.ubhavedynamicapp.planner.data.PlanReview;
import org.ubhave.ubhavedynamicapp.planner.data.PlanReviewResult;
import org.ubhave.ubhavedynamicapp.planner.data.PlanScoreReviewer;
import org.ubhave.ubhavedynamicapp.planner.data.PlanSet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class PlannerScoreReviewActivity extends UbhaveActivity{

	PlanScoreReviewer reviewer;
	
	HashMap<String,PlanReviewResult> results;
	
	PlanSet planSet;
	
	ArrayList<Spinner> spinners;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reviewer);
		reviewer = (PlanScoreReviewer) getIntent().getParcelableExtra(PlannerReviewController.REVIEWER);
		results = new HashMap<String,PlanReviewResult>();
		
		spinners = new ArrayList<Spinner>();
				
		setActivityTitle(reviewer.getTitle());
		TextView textView = (TextView) findViewById(R.id.reviewerText);
		textView.setText(reviewer.getHeaderText());
		
		planSet = (PlanSet) getIntent().getParcelableExtra(PlannerReviewController.PLAN_SET);
		
		addPlans(planSet);
		
		
	}
	
	
	
	private void addPlans(PlanSet planSet) {
		
		LinearLayout plansListView = (LinearLayout) findViewById(R.id.reviewerLayout);
		
		HashMap<String, Plan> plans = planSet.getPlans();
		Iterator<Entry<String, Plan>> it = plans.entrySet().iterator();
		
		while (it.hasNext()) {
			
			Map.Entry<String, Plan> entry = (Map.Entry<String, Plan>)it.next();
			
			Plan plan = entry.getValue();
			
			results.put(plan.getID(),new PlanReviewResult(plan, 0));
			
			RelativeLayout planView = (RelativeLayout) View.inflate(this, R.layout.score_review, null);
			
			TextView spinnerTV = (TextView)planView.findViewById(R.id.reviewPlanScoreText);
			spinnerTV.setText(plan.getStringRepresentation());
			
			Spinner spinner = (Spinner)planView.findViewById(R.id.reviewPlanScore);

			ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
			spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner.setAdapter(spinnerAdapter);
			
			spinner.setTag(plan);
			
			spinners.add(spinner);
			
			for(int i = reviewer.getBounds()[0];i <= reviewer.getBounds()[1];i++){
				spinnerAdapter.add(""+i);
			}
			
			spinnerAdapter.notifyDataSetChanged();
						
			plansListView.addView(planView);
		}
		
	}
		
	public void submit(View v){
		for(Spinner spinner:spinners){
			results.put(((Plan)spinner.getTag()).getStringRepresentation(), new PlanReviewResult(((Plan)spinner.getTag()),new Integer(""+spinner.getSelectedItem())));
		}
		
		PlanReview review = new PlanReview(planSet, results);
		Intent result = getIntent();		
		result.putExtra(PlannerReviewController.REVIEW, review);		
		this.setResult(DynamicApplicationController.RESULT_REVIEWER_OK, result);
		
		this.finish();
	}



	@Override
	protected String getActivityType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getActivityID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getActivityExtra() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}