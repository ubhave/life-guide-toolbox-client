/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.background.trigger;

import java.util.ArrayList;

import org.ubhave.intelligenttrigger.triggers.definitions.TriggerDefinition;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditional;

import android.util.SparseArray;

public class TriggerDefinitionBundle {

	private TriggerDefinition d_definition;
	
	private UbhaveConditional d_condition;
	
	private ArrayList<TriggerAction> d_actions;
	
	private SparseArray<String> d_variables;
	
	public TriggerDefinitionBundle(TriggerDefinition a_definition, 
			UbhaveConditional a_condition,
			ArrayList<TriggerAction> a_actions,
			SparseArray<String> a_variables) {
		d_definition = a_definition;
		d_condition = a_condition;
		d_actions = a_actions;		
		d_variables = a_variables;
	}
	
	public TriggerDefinition getDefinition() {
		return d_definition;
	}
	
	public UbhaveConditional getCondition() {
		return d_condition;
	}
	
	public ArrayList<TriggerAction> getActions() {
		return d_actions;
	}
	
	public SparseArray<String> getVariables(){
		return d_variables;
	}
}
