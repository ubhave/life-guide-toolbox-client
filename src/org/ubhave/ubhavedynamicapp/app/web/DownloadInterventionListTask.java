/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.app.web;

import java.util.HashMap;

import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.WebConnection;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

public class DownloadInterventionListTask extends AsyncTask<String, Void, String>{

	private final Activity d_context;
	private final ProgressDialog d_dialog;

	public DownloadInterventionListTask(Activity a_context) {
		d_context = a_context;
		d_dialog = new ProgressDialog(a_context);
		d_dialog.setMessage("Downloading App Set-Up...");
		d_dialog.setIndeterminate(false);
		d_dialog.setCancelable(false);
	}
	
	@Override
	protected void onPreExecute(){		
		if (!d_context.isFinishing())
			d_dialog.show();
	}
	
	@Override
	protected String doInBackground(String... args) {
		
		String token = args[0];
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", DynamicApplicationConstants.REST_API_CONTENT_TYPE_JSON);
		headers.put("Authentication", "IDToken "+token);
 		String result = null;
		try {
			result = WebConnection.getData( 
					DynamicApplicationConstants.DL_INTERVENTION_LIST_URL, 
					null,
					headers);
		} catch (Exception e) {
			//return e.getMessage();
		}

		return result;
	}

	@Override
	protected void onPostExecute(String result)
	{
		try
		{
			if (result == null)
			{
				Toast.makeText(d_context, "Problem contacting server.", Toast.LENGTH_LONG).show();
			}
			d_dialog.dismiss();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
}
