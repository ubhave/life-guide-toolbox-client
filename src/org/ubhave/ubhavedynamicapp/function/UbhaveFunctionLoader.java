/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.function;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.JSONLoader;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditional;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditionalLoader;

public class UbhaveFunctionLoader extends JSONLoader{
	
	final static private String FUNCTION = "function";
	final static private String CONDITION = "condition";
	
	final static private String TYPE_INCREMENT = "increment";
	final static private String TYPE_SET = "set";
	final static private String TYPE_SET_TIME = "setTime";
	final static private String TYPE_LAUNCH = "launch";
	final static private String TYPE_TOAST = "toast";
	final static private String TYPE_REF = "ref";
	final static private String TYPE_SUBROUTINE = "subroutine";
	private static final Object TYPE_RANDOMISE = "randomise";
	
	final static private String VAR = "var";
	final static private String INCREMENT = "increment";
	final static private String VALUE = "value";
	final static private String ID = "id";
	final static private String ACTIVITY_ID = "activityId";
	final static private String TOAST_TEXT = "toastText";
	final static private String REF_ID = "refId";
	final static private String FUNCTIONS = "functions";
	private static final String FAILURE_STRING = "failureString";
	
		
	//passing a ref to the controller feels clumsy but is currently necessary for the launch activity function - might be better to store the dac in the app singleton?
	public static UbhaveFunction loadFunction(JSONObject functionJO) throws JSONException{
		UbhaveFunction function=null;
		
		String functionType = functionJO.getString(FUNCTION);
		
		JSONObject conditionalJO = null;
		UbhaveConditional condition = null;
		
		if(functionJO.has(CONDITION)){
			conditionalJO = functionJO.getJSONObject(CONDITION);
			condition = UbhaveConditionalLoader.loadConditional(conditionalJO);
		}
		
		String id = functionJO.getString(ID);
		
		if(functionType.equals(TYPE_INCREMENT)){
			function = new IncrementVariableUbhaveFunction(id,functionJO.getString(VAR), functionJO.getString(INCREMENT), condition);			
		}
		else if(functionType.equals(TYPE_SET)){
			function= new SetVariableUbhaveFunction(id,functionJO.getString(VAR), functionJO.getString(VALUE), condition);			
		}
		else if(functionType.equals(TYPE_SET_TIME)){
			function= new SetVarToTimeUbhaveFunction(id,functionJO.getString(VAR), condition);			
		}
		else if(functionType.equals(TYPE_LAUNCH)){
			function=new LaunchActivityUbhaveFunction(id,functionJO.getString(ACTIVITY_ID), condition);			
		}
		else if(functionType.equals(TYPE_TOAST)){
			function=new ToastUbhaveFunction(id,functionJO.getString(TOAST_TEXT), condition);			
		}
		else if(functionType.equals(TYPE_REF)){
			function=new UbhaveFunctionReference(id,functionJO.getString(REF_ID), condition);			
		}
		else if(functionType.equals(TYPE_SUBROUTINE)){
			ArrayList<UbhaveFunction> functions = new ArrayList<UbhaveFunction>();
			JSONArray functionsJA = functionJO.getJSONArray(FUNCTIONS);
			for(int i = 0; i<functionsJA.length();i++){
				functions.add(loadFunction(functionsJA.getJSONObject(i)));
			}
			function=new SubRoutineUbhaveFunction(id,functions, condition);			
		}
		else if(functionType.equals(TYPE_RANDOMISE)){
						
			GetRandomisationUbhaveFunction randfunc = new GetRandomisationUbhaveFunction(id,functionJO.getString(VAR), condition);
			
			if(functionJO.has(FAILURE_STRING)){
				randfunc.setFailureString(functionJO.getString(FAILURE_STRING));
			}
			
			function=randfunc;
		}
		
		return function;		
	}
	
}