/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.info.data;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class InformationList extends AbstractInformation
{
	private final ArrayList<AbstractInformation> informationList;

	public InformationList(String a_id)
	{
		super(AbstractInformation.INFO_LIST, a_id);
		informationList = new ArrayList<AbstractInformation>();
	}

	public int size()
	{
		return informationList.size();
	}

	public void add(AbstractInformation info)
	{
		informationList.add(info);
	}

	public AbstractInformation get(int index)
	{
		return informationList.get(index);
	}

	public static final Parcelable.Creator<InformationList> CREATOR = new Parcelable.Creator<InformationList>()
	{
		public InformationList createFromParcel(Parcel in)
		{
			return new InformationList(in);
		}

		public InformationList[] newArray(int size)
		{
			return new InformationList[size];
		}
	};

	@Override
	public void writeToParcel(Parcel out, int flags)
	{
		super.writeToParcel(out, flags);
		out.writeInt(informationList.size());
		for (AbstractInformation info : informationList)
		{
			out.writeParcelable(info, flags);
		}
	}

	public InformationList(Parcel in)
	{
		super(in);
		informationList = new ArrayList<AbstractInformation>();
		int infoItems = in.readInt();
		for (int i = 0; i < infoItems; i++)
		{
			AbstractInformation info = in.readParcelable(AbstractInformation.class.getClassLoader());
			informationList.add(info);
		}
	}
}
