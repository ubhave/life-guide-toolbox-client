/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.info.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.JSONLoader;

public class InformationListLoader extends JSONLoader
{
	private final static String INFORMATION = "information";
	//private final static String INFORMATION_LIST = "info_list";
	//private final static String INFORMATION_ID = "info_id";
	private final static String INFORMATION_ID = "id";
	private final static String TYPE = "type";
	private final static String MEMBERS = "members";
	private final static String TITLE = "title";
	private final static String TEXT = "text";
	private static final String AUDIO = "audio";
	
	public static AbstractInformation loadInformation(JSONObject infoJO, String infoID) throws JSONException
	{
		try
		{
			JSONObject information = infoJO;
			//String informationID = information.getString(INFORMATION_ID);
			int infoType = information.getInt(TYPE);
			//String informationID = "0";
			
			if(infoType==0){
			
				JSONArray infoItems = information.getJSONArray(MEMBERS);
				if (infoItems != null)
				{
					InformationList infoList = new InformationList(infoID);
					infoList.setTitle("Information");
					for (int i=0; i<infoItems.length(); i++)
					{
						JSONObject infoObject = infoItems.getJSONObject(i);
						addObject(infoObject, infoList, null);
					}
					return infoList;
				}
			}
			else{
				//info is single item
				TextInformation textInfo = new TextInformation(infoID);//information.getString(INFORMATION_ID));
				textInfo.setTitle(information.getString(TITLE));
				textInfo.setText(information.getString(TEXT));
				if(information.has(AUDIO)){
					textInfo.setAudio(information.getString(AUDIO));
				}
				else{
					textInfo.setAudio("");
				}
				return textInfo;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	private static void addObject(JSONObject json, InformationList list, AbstractInformation parent)
	{
		try
		{
			int type = json.getInt(TYPE);
			String id = json.getString(INFORMATION_ID);
			if (type == AbstractInformation.INFO_TEXT)
			{
				TextInformation textInfo = new TextInformation(id);
				textInfo.setTitle(json.getString(TITLE));
				textInfo.setText(json.getString(TEXT));
				if(json.has(AUDIO)){
					textInfo.setAudio(json.getString(AUDIO));
				}
				else{
					textInfo.setAudio("");
				}
				textInfo.setParent(list);
				list.add(textInfo);
			}
			else if (type == AbstractInformation.INFO_LIST)
			{
				InformationList sublist = new InformationList(id);
				sublist.setTitle(json.getString(TITLE));
				sublist.setParent(list);
				JSONArray subitems = json.getJSONArray(MEMBERS);
				for (int i=0; i<subitems.length(); i++)
				{
					JSONObject subitem = subitems.getJSONObject(i);
					addObject(subitem, sublist, list);
				}
				list.add(sublist);
			}
			// TODO add other types as required
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
