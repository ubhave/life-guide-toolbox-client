/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.AbstractComponentController;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.UbhaveActivity;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.info.InformationController;
import org.ubhave.ubhavedynamicapp.info.data.TextInformation;
import org.ubhave.ubhavedynamicapp.log.ApplicationDataLogger;
import org.ubhave.ubhavedynamicapp.planner.data.PlanReview;
import org.ubhave.ubhavedynamicapp.planner.data.PlanReviewer;
import org.ubhave.ubhavedynamicapp.planner.data.PlanSet;
import org.ubhave.ubhavedynamicapp.planner.data.ReviewerLoader;
import org.ubhave.ubhavedynamicapp.planner.ui.FeedbackActivity;
import org.ubhave.ubhavedynamicapp.planner.ui.PlannerCheckReviewActivity;
import org.ubhave.ubhavedynamicapp.planner.ui.PlannerScoreReviewActivity;
import org.ubhave.ubhavedynamicapp.planner.ui.PlannerViewReviewActivity;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public class PlannerReviewController extends AbstractComponentController{

	public static final String REVIEWER = "reviewer";
	private static final int REVIEWER_REQUEST_CODE = 0;
	public static final String PLAN_SET = "planSet";
	public static final String REVIEW = "review";
	private static final String TAG = "PlannerReviewController";
	public static final int REVIEWER_TYPE_CHECK = 0;
	public static final int REVIEWER_TYPE_SCORE = 1;
	public static final int REVIEWER_TYPE_VIEW = 2;
	
	Activity context;
	JSONObject currentReviewerJSON;
	PlanReviewer currentReviewer;
	String planReviewID;
	
	
	public PlannerReviewController(Activity c, JSONObject content, String id, DynamicApplicationController dynamicApplicationController) {
		context=c;
		currentReviewerJSON=content;
		planReviewID = id;
		setAppController(dynamicApplicationController);
	}

	@Override
	public void start(){		
		currentReviewer=null;
		try {
			currentReviewer = ReviewerLoader.loadReviewer(currentReviewerJSON, planReviewID);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		Intent activityIntent = null;
		if(currentReviewer.getType()==REVIEWER_TYPE_CHECK)
			activityIntent=new Intent(context, PlannerCheckReviewActivity.class);
		else if(currentReviewer.getType()==REVIEWER_TYPE_SCORE)
			activityIntent=new Intent(context, PlannerScoreReviewActivity.class);
		else if(currentReviewer.getType()==REVIEWER_TYPE_VIEW)
			activityIntent=new Intent(context, PlannerViewReviewActivity.class);
		
		activityIntent.putExtra(REVIEWER, currentReviewer);
		
		//Test code
		/*String planID = "testPlan";
		HashMap<String,String> testFieldMap = new HashMap<String,String>();
		testFieldMap.put("who", "who test");
		testFieldMap.put("what", "what test");
		testFieldMap.put("where", "where test");
		MultiFieldPlan testPlan = new MultiFieldPlan(planID, testFieldMap);
		 
		PlanSet testSet = new PlanSet("plans");
		testSet.addPlan(planID, testPlan);
		
		activityIntent.putExtra(PLAN_SET, testSet);*/
		//
		
		//Real code
		
		PlanSet planSet = InterventionDataAccess.getPlanSet(currentReviewer.getEditPlannerId());
		if(planSet==null)
			planSet = new PlanSet(currentReviewer.getEditPlannerId());
		activityIntent.putExtra(PLAN_SET, planSet);
		
		activityIntent.putExtra(UbhaveActivity.SKIPABLE, isSkipable());
		
		context.startActivityForResult(activityIntent, REVIEWER_REQUEST_CODE);
	}
	
	public void launchFeedback(PlanReview review){
		Intent activityIntent = new Intent(context, FeedbackActivity.class);
		activityIntent.putExtra(REVIEW, review);
		
		TextInformation feedback=null;
		feedback = currentReviewer.getFeedback();
				
		activityIntent.putExtra(InformationController.CURRENT_INFORMATION, feedback);
		
		if(feedback.getParent()!=null)
			activityIntent.putExtra(InformationController.CURRENT_INFORMATION_PARENT_STATUS, true);
		else
			activityIntent.putExtra(InformationController.CURRENT_INFORMATION_PARENT_STATUS, false);
		
		activityIntent.putExtra(UbhaveActivity.SKIPABLE, isSkipable());
		
		context.startActivityForResult(activityIntent, REVIEWER_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
				
		switch (resultCode)
		{
		case DynamicApplicationController.RESULT_REVIEWER_OK:
			Log.i(TAG, "Reviewer Finished");
			PlanReview review = data.getParcelableExtra(REVIEW);
			
			ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
			logger.logPlanReview(review);
			
			launchFeedback(review);
			
			Log.i(TAG, "Logging score "+review.getScore());
			InterventionDataAccess.storeCustomVariable("lastScore", ""+review.getScore());
			
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_STAGE_EXECUTE_FUNCTIONS);
			
			break;
		case DynamicApplicationController.RESULT_REVIEWER_FEEDBACK_OK:
			Log.i(TAG, "Feedback Finished");
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_NO_FUNCTIONS);
			//getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE);
			break;
		case DynamicApplicationController.RESULT_REVIEWER_FEEDBACK_EDIT:
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_AND_JUMP, currentReviewer.getEditPlannerId());
			break;
		case Activity.RESULT_CANCELED:
			Log.i(TAG, "Reviewer Back");
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_BACK);
			break;
		case DynamicApplicationController.RESULT_ACTIONBAR_HOME:
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_AND_JUMP_TO_FRONT);
			break;
		}
		
	}
	
	
}