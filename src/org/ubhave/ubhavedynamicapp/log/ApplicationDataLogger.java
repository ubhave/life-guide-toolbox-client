/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.log;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.InterventionPreferences;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;
import org.ubhave.ubhavedynamicapp.background.trigger.UbhaveNotification;
import org.ubhave.ubhavedynamicapp.background.trigger.UbhaveNotificationAnswer;
import org.ubhave.ubhavedynamicapp.diary.data.Diary;
import org.ubhave.ubhavedynamicapp.planner.data.PlanReview;
import org.ubhave.ubhavedynamicapp.planner.data.PlanSet;
import org.ubhave.ubhavedynamicapp.survey.data.as.AnswerList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.preference.PreferenceManager;
import android.util.Log;

import com.ubhave.datahandler.loggertypes.AbstractAsyncTransferLogger;

public class ApplicationDataLogger extends AbstractAsyncTransferLogger{

	private final static String TAG = "ApplicationDataLogger";

	private final static String CUSTOM_VARIABLE = "CustomVariable";
	private final static String SURVEY_RESPONSE = "Surveys";
	private final static String DIARY_RESPONSE = "Diaries";
	private final static String PLAN = "Plan";
	private final static String PLAN_REVIEW = "PlanReview";	
	private final static String APP_USAGE_LOG = "AppUsageLog";
	private final static String NOTIFICATION = "TriggerNotification";
	private final static String NOTIFICATION_ANSWER = "TriggerNotificationAnswer";
	private final static String LEARNER = "Learner";

	private static ApplicationDataLogger instance;
	
	public static ApplicationDataLogger getInstance()
	{
		if (instance == null)
		{
			instance = new ApplicationDataLogger(UbhaveApplication.getContext());
		}
		return instance;
	}
	
	private ApplicationDataLogger(Context a_context) {
		super(a_context);
	}
	
	public void logSurveyResponse(AnswerList answers)
	{
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		SharedPreferences.Editor editor = sharedPrefs.edit();
		
		HashMap<String,String> flatEntries = answers.getFlatEntries();
		for (Map.Entry<String,String> entry : flatEntries.entrySet()){
			String key = InterventionDataAccess.getCurrentInterventionName()+"_"+entry.getKey();
			editor.putString(key, entry.getValue());
			//Log.i("Store Survey Answer", key+" "+entry.getValue());
			
		}
		editor.commit();
		
		try {
			JSONObject response = answers.toJSON();
			response.put(DynamicApplicationConstants.APP_VERSION, getVersion());
			response.put(DynamicApplicationConstants.INTERVENTION_ID, InterventionDataAccess.getCurrentInterventionName());
			response.put(DynamicApplicationConstants.ITEM_TYPE, DynamicApplicationConstants.TYPE_SURVEY);
			log(SURVEY_RESPONSE, response.toString());			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void logDiaryResponse(Diary diary)
	{
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		SharedPreferences.Editor editor = sharedPrefs.edit();
		
		HashMap<String,String> flatEntries = diary.getFlatEntries();
		for (Map.Entry<String,String> entry : flatEntries.entrySet()){
			String key = InterventionDataAccess.getCurrentInterventionName()+"_"+entry.getKey();
			editor.putString(key, entry.getValue());
		}
		editor.commit();
		
		try {
			JSONObject response = diary.toJSON();
			response.put(DynamicApplicationConstants.APP_VERSION, getVersion());
			response.put(DynamicApplicationConstants.INTERVENTION_ID, InterventionDataAccess.getCurrentInterventionName());
			response.put(DynamicApplicationConstants.ITEM_TYPE, DynamicApplicationConstants.TYPE_DIARY);
			log(DIARY_RESPONSE, response.toString());			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public void logPlanSet(PlanSet a_set) {
		
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		SharedPreferences.Editor editor = sharedPrefs.edit();
		String storePlanSet = a_set.toStorageJSON();
		editor.putString(a_set.getPlannerKey(), storePlanSet);
		editor.commit();
		
		Log.d(TAG, storePlanSet);
		
		try {
			JSONObject set = a_set.toJSON();
			set.put(DynamicApplicationConstants.APP_VERSION, getVersion());
			set.put(DynamicApplicationConstants.INTERVENTION_ID, InterventionDataAccess.getCurrentInterventionName());
			set.put(DynamicApplicationConstants.ITEM_TYPE, DynamicApplicationConstants.TYPE_PLANNER);
			log(PLAN, set.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void logPlanReview(PlanReview a_review) {
		try {
			JSONObject set = a_review.toJSON();
			set.put(DynamicApplicationConstants.APP_VERSION, getVersion());
			set.put(DynamicApplicationConstants.INTERVENTION_ID, InterventionDataAccess.getCurrentInterventionName());
			set.put(DynamicApplicationConstants.ITEM_TYPE, DynamicApplicationConstants.TYPE_PLANNER_REVIEW);
			log(PLAN_REVIEW, set.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	public void logNotificationAnswer(UbhaveNotificationAnswer a_notif) {
		
		try {
			JSONObject notif = a_notif.toJSON();
			notif.put(DynamicApplicationConstants.APP_VERSION, getVersion());
			notif.put(DynamicApplicationConstants.INTERVENTION_ID, InterventionDataAccess.getCurrentInterventionName());
			notif.put(DynamicApplicationConstants.ITEM_TYPE, DynamicApplicationConstants.TYPE_NOTIFICATION_ANSWER);
			log(NOTIFICATION_ANSWER, notif.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void logNotification(UbhaveNotification a_notif) {
		
		try {
			JSONObject notif = a_notif.toJSON();
			notif.put(DynamicApplicationConstants.APP_VERSION, getVersion());
			notif.put(DynamicApplicationConstants.INTERVENTION_ID, InterventionDataAccess.getCurrentInterventionName());
			notif.put(DynamicApplicationConstants.ITEM_TYPE, DynamicApplicationConstants.TYPE_NOTIFICATION);
			log(NOTIFICATION, notif.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void logAppUsage(UsageLog log)
	{
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		SharedPreferences.Editor editor = sharedPrefs.edit();
		
		HashMap<String,String> flatEntries = log.getFlatEntries();
		for (Map.Entry<String,String> entry : flatEntries.entrySet()){
			String key = InterventionDataAccess.getCurrentInterventionName()+"_"+entry.getKey();
			editor.putString(key, entry.getValue());
		}
		editor.commit();
		
		try {
			JSONObject entry = log.toJSON();
			entry.put(DynamicApplicationConstants.APP_VERSION, getVersion());
			entry.put(DynamicApplicationConstants.INTERVENTION_ID, InterventionDataAccess.getCurrentInterventionName());
			entry.put(DynamicApplicationConstants.ITEM_TYPE, DynamicApplicationConstants.TYPE_APP_USAGE);
			log(APP_USAGE_LOG, entry.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public void logAppInteraction(InteractionLog log)
	{
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		SharedPreferences.Editor editor = sharedPrefs.edit();
		
		HashMap<String,String> flatEntries = log.getFlatEntries();
		for (Map.Entry<String,String> entry : flatEntries.entrySet()){
			String key = InterventionDataAccess.getCurrentInterventionName()+"_"+entry.getKey();
			editor.putString(key, entry.getValue());
		}
		editor.commit();
		
		try {
			JSONObject entry = log.toJSON();
			entry.put(DynamicApplicationConstants.APP_VERSION, getVersion());
			entry.put(DynamicApplicationConstants.INTERVENTION_ID, InterventionDataAccess.getCurrentInterventionName());
			entry.put(DynamicApplicationConstants.ITEM_TYPE, DynamicApplicationConstants.TYPE_APP_USAGE);
			log(APP_USAGE_LOG, entry.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
		
	// Store the given value in SharedPreferences
	// If there is a change in value, record to JSON
	public void logCustomVariable(String a_key, String a_value) {

		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		String currentValue = sharedPrefs.getString(a_key,"");
		
		if (!currentValue.equals(a_value)) {

			SharedPreferences.Editor editor = sharedPrefs.edit();
			editor.putString(a_key, a_value);
			editor.commit();
						
			try {
				JSONObject json = new JSONObject();
				json.put(DynamicApplicationConstants.CURRENT_TIME, 
						System.currentTimeMillis());
				json.put(DynamicApplicationConstants.UID,
						sharedPrefs.getString(DynamicApplicationConstants.UID, null));
				json.put(DynamicApplicationConstants.APP_VERSION, getVersion());
				json.put(DynamicApplicationConstants.INTERVENTION_ID, 
						InterventionDataAccess.getCurrentInterventionName());
				
				json.put(a_key, a_value);
				json.put(DynamicApplicationConstants.ITEM_TYPE, DynamicApplicationConstants.TYPE_VARIABLE);
				log(CUSTOM_VARIABLE, json.toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	
	
	public void logLearner(String  a_learner, int a_week) {
		
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		
		try {
			JSONObject data = new JSONObject(a_learner);
			data.put(DynamicApplicationConstants.WEEK, a_week);
			data.put(DynamicApplicationConstants.CURRENT_TIME, 
					System.currentTimeMillis());
			data.put(DynamicApplicationConstants.UID,
					sharedPrefs.getString(DynamicApplicationConstants.UID, null));
			data.put(DynamicApplicationConstants.INTERVENTION_ID, 
					InterventionDataAccess.getCurrentInterventionName());
			data.put(DynamicApplicationConstants.APP_VERSION, getVersion());
			data.put(DynamicApplicationConstants.ITEM_TYPE, 
					DynamicApplicationConstants.TYPE_LEARNER);
			
			log(LEARNER, data.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	protected long getFileLifeMillis() {
		return DynamicApplicationConstants.FILE_LIFE_MILLIS;
	}

	@Override
	protected long getTransferAlarmLengthMillis() {
		return DynamicApplicationConstants.UPLOAD_INTERVAL_MILLIS;
	}

	@Override
	protected String getDataPostURL() {
		return InterventionPreferences.getInterventionDetails("upload");
		// return DynamicApplicationConstants.UL_DATA_URL;
	}


	@Override
	protected String getLocalStorageDirectoryName() {
		return DynamicApplicationConstants.LOCAL_STORAGE_HOME_DIRECTORY_NAME;
	}

	@Override
	protected String getUniqueUserId() {
		SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		String uuid = settings.getString(DynamicApplicationConstants.UID, "default");
		return uuid;
	}

	@Override
	protected HashMap<String, String> getPostParameters() {
		HashMap<String, String> postParams = new HashMap<String, String>();
		SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		String token = settings.getString(DynamicApplicationConstants.AUTH_TOKEN, "");
		String uId = settings.getString(DynamicApplicationConstants.UID, "");
		postParams.put(DynamicApplicationConstants.AUTH_TOKEN, token);
		postParams.put(DynamicApplicationConstants.UID, uId);
		return postParams;
	}

	@Override
	protected String getSuccessfulPostResponse() {
		return "success";
	}

	@Override
	protected boolean shouldPrintLogMessages() {
		// TODO Auto-generated method stub
		return true;
	}

	public int getVersion() {
	    int v = 0;
	    try {
	        v = UbhaveApplication.getContext().getPackageManager()
	        		.getPackageInfo(UbhaveApplication.getContext().getPackageName(), 0)
	        		.versionCode;
	    } catch (NameNotFoundException e) {
	        // Shouldn't go here
	    }
	    return v;
	}

}
