/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;

import android.content.Context;
import android.util.Log;

public abstract class WebConnection
{
	private static final String TAG = "WebConnection";

	public static String postData(final String targetURL, final ArrayList<BasicNameValuePair> nameValuePairs) throws Exception
	{
		return postData(targetURL, nameValuePairs, null);
	}

	public static String postData(final String targetURL, final ArrayList<BasicNameValuePair> nameValuePairs, final HashMap<String, String> headersMap) throws Exception
	{
		HttpClient httpclient = new DefaultHttpClient();
		httpclient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 60 * 1000);
		HttpPost httppost = new HttpPost(targetURL);

		httppost.addHeader("User-Agent", "ubhave-client/"+UbhaveApplication.getContext().getPackageManager().getPackageInfo(UbhaveApplication.getContext().getPackageName(), 0).versionName);
		
		if (headersMap != null)
		{
			for (String key: headersMap.keySet())
			{
				httppost.addHeader(key, headersMap.get(key));
			}
		}

		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		HttpResponse response = httpclient.execute(httppost);

		String line;
		final BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		final StringBuilder buffer = new StringBuilder();
		while ((line = in.readLine()) != null)
		{
			buffer.append(line);
		}
		in.close();
		httpclient.getConnectionManager().shutdown();
		return buffer.toString();
	}

	public static String getData(final String targetURL, final HashMap<String, String> paramsMap) throws Exception
	{
		return getData(targetURL, paramsMap, null);
	}
	
	public static String getData(final String targetURL, final HashMap<String, String> paramsMap, final HashMap<String, String> headersMap) throws Exception
	{
		String response = null;
		HttpClient httpclient = new DefaultHttpClient();
		httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
		httpclient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 60 * 1000);
		
		
		HttpGet httpget;
		
		if (paramsMap != null)
		{
			List<NameValuePair> qparams = new ArrayList<NameValuePair>();
			for (String key : paramsMap.keySet())
			{
				String value = paramsMap.get(key);
				qparams.add(new BasicNameValuePair(key, value));
			}
			httpget = new HttpGet(targetURL + "?" + URLEncodedUtils.format(qparams, "UTF-8"));
		}
		else
		{
			httpget = new HttpGet(targetURL);
		}
		
		httpget.addHeader("User-Agent", "ubhave-client/"+UbhaveApplication.getContext().getPackageManager().getPackageInfo(UbhaveApplication.getContext().getPackageName(), 0).versionName);
				
		if (headersMap != null)
		{
			for (String key: headersMap.keySet())
			{
				httpget.addHeader(key, headersMap.get(key));
			}
		}
		HttpResponse httpResponse = httpclient.execute(httpget);

		response = convertStreamToString(httpResponse);
		httpclient.getConnectionManager().shutdown();
		Log.d(TAG, "Got this from the server: "+response);
		return response;
	}
	
	public static void getAndWriteData(final String targetURL, String fileDestination, final HashMap<String, String> paramsMap, final HashMap<String, String> headersMap) throws Exception
	{
		String response = null;
		HttpClient httpclient = new DefaultHttpClient();
		httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
		httpclient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 60 * 1000);

		HttpGet httpget;
		if (paramsMap != null)
		{
			List<NameValuePair> qparams = new ArrayList<NameValuePair>();
			for (String key : paramsMap.keySet())
			{
				String value = paramsMap.get(key);
				qparams.add(new BasicNameValuePair(key, value));
			}
			httpget = new HttpGet(targetURL + "?" + URLEncodedUtils.format(qparams, "UTF-8"));
		}
		else
		{
			httpget = new HttpGet(targetURL);
		}
		if (headersMap != null)
		{
			for (String key: headersMap.keySet())
			{
				httpget.addHeader(key, headersMap.get(key));
			}
		}
		HttpResponse httpResponse = httpclient.execute(httpget);

		FileOutputStream fos = UbhaveApplication.getContext().openFileOutput(fileDestination, Context.MODE_PRIVATE);
		
		InputStream is = httpResponse.getEntity().getContent();
		
		byte[] buffer = new byte[1024];
	    int read;
	    while((read = is.read(buffer)) != -1){
	    	fos.write(buffer, 0, read);
	    }
	    	    
	    fos.close();
		
		httpclient.getConnectionManager().shutdown();
		Log.d(TAG, "Got this from the server: "+response);		
	}

	private static String convertStreamToString(final HttpResponse response) throws Exception
	{
		InputStream is = response.getEntity().getContent();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try
		{
			while ((line = reader.readLine()) != null)
			{
				sb.append((line));
			}
			reader.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	public static void getData(Context context, String url, String fileDestination, HashMap<String, String> params) throws Exception
	{
		getData(context, url, fileDestination, params, null);
	}
	public static void getData(Context context, String url, String fileDestination, HashMap<String, String> params, HashMap<String, String>headersMap) throws Exception
	{
		String data = getData(url, params, headersMap);
		try
		{
			FileOutputStream fos = context.openFileOutput(fileDestination, Context.MODE_PRIVATE);
			fos.write(data.getBytes());
			fos.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
}
