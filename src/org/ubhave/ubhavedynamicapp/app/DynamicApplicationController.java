/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.app;

import java.util.ArrayList;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.intelligenttrigger.learners.InterruptibilityLearner;
import org.ubhave.intelligenttrigger.utils.Constants;
import org.ubhave.ubhavedynamicapp.AbstractComponentController;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.JSONLoader;
import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;
import org.ubhave.ubhavedynamicapp.app.data.DynamicApplication;
import org.ubhave.ubhavedynamicapp.app.data.DynamicApplicationLoader;
import org.ubhave.ubhavedynamicapp.app.data.menu.MenuLoader;
import org.ubhave.ubhavedynamicapp.app.data.menu.UbhaveMenu;
import org.ubhave.ubhavedynamicapp.app.data.menu.UbhaveMenuItem;
import org.ubhave.ubhavedynamicapp.app.ui.MenuActivity;
import org.ubhave.ubhavedynamicapp.background.DynamicApplicationService;
import org.ubhave.ubhavedynamicapp.background.trigger.NotificationTriggerAction;
import org.ubhave.ubhavedynamicapp.background.trigger.TriggerAction;
import org.ubhave.ubhavedynamicapp.background.trigger.UbhaveNotificationAnswer;
import org.ubhave.ubhavedynamicapp.diary.DiaryController;
import org.ubhave.ubhavedynamicapp.function.UbhaveFunction;
import org.ubhave.ubhavedynamicapp.function.UbhaveFunctionLoader;
import org.ubhave.ubhavedynamicapp.info.InformationController;
import org.ubhave.ubhavedynamicapp.log.ApplicationDataLogger;
import org.ubhave.ubhavedynamicapp.log.UsageLog;
import org.ubhave.ubhavedynamicapp.planner.PlannerController;
import org.ubhave.ubhavedynamicapp.planner.PlannerReviewController;
import org.ubhave.ubhavedynamicapp.sequence.data.Sequence;
import org.ubhave.ubhavedynamicapp.sequence.data.SequenceLoader;
import org.ubhave.ubhavedynamicapp.settings.SettingsController;
import org.ubhave.ubhavedynamicapp.survey.QuestionnaireController;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

public class DynamicApplicationController extends AbstractComponentController
{	
    private static final String TAG = "DynamicApplicationController";

	private final Activity context;
	private final DynamicApplication applicationConfiguration;
	private AbstractComponentController currentController;

	public static final int RESULT_MENU_OK = 2;
	public static final int RESULT_MENU_CANCELED = 3;
	public static final int RESULT_INFO_OK = 6;
	public static final int RESULT_INFO_CANCELED = 7;
	public static final int RESULT_DIARY_OK = 8;
	public static final int RESULT_DIARY_NEXT = 9;
	public static final int RESULT_DIARY_PREV = 10;
	public static final int RESULT_DIARY_CANCELED = 11;
	public static final int RESULT_SETTINGS_OK = 12;
	public static final int RESULT_SETTINGS_CANCELED = 13;
	public static final int RESULT_PLANNER_OK = 13;
	public static final int RESULT_REVIEWER_OK = 14;
	public static final int RESULT_REVIEWER_FEEDBACK_OK = 15;
	public static final int RESULT_REVIEWER_FEEDBACK_EDIT = 16;
	public static final int RESULT_ACTIONBAR_HOME = 17;
	
    private UbhaveMenu currentMenu;
    private Sequence currentSequence;
    private int currentSequencePosition;
    private int currentSequenceDirection;
        
    SharedPreferences sharedPrefs;
    SharedPreferences.Editor editor;
	
    public DynamicApplicationController(final Activity context) throws Exception 
	{
		this.context = context;
		
		JSONObject appJO = null;
		try{		
			appJO = JSONLoader.getFileContents(DynamicApplicationConstants.CURRENT_INTERVENTION_CONFIG);
			Log.i(TAG, "Using local downloaded JSON");
		} catch(Exception e){
			
			Log.i(TAG, "FAILED TO LOAD LOCAL DOWNLOADED JSON - USING RAW JSON");
			
			String packageName = context.getPackageName();
			
			int intRes = context.getResources().getIdentifier("intervention", "raw", packageName);
			appJO = new JSONObject(JSONLoader.loadFileContents(intRes)).getJSONObject("content");			
		}
		
		
		///Debug
		//appJO = new JSONObject(JSONLoader.loadFileContents(R.raw.intervention)).getJSONObject("content");
		//appJO = new JSONObject(JSONLoader.loadFileContents(R.raw.example)).getJSONObject("content");
		// 
		
		applicationConfiguration = DynamicApplicationLoader.loadIntervention(appJO);
		InterventionDataAccess.setCurrentInterventionTheme(applicationConfiguration.getTheme());
		
		UbhaveApplication.setConfig(applicationConfiguration);
		
		currentController = null;
		sharedPrefs=PreferenceManager.getDefaultSharedPreferences(context);
		editor = sharedPrefs.edit();
		currentMenu= MenuLoader.loadMenu(getItem(sharedPrefs.getString("currentMenu", applicationConfiguration.getFrontId())).getContent());
		
		if(sharedPrefs.getBoolean("inSequence", false)){
			currentSequence = SequenceLoader.loadSequence(getItem(sharedPrefs.getString("currentSequence", 
					DynamicApplicationConstants.DEFAULT_SEQUENCE_ID)).getContent());
			currentSequencePosition = sharedPrefs.getInt("currentSequencePosition", 0);
		}
		else
			currentSequence=null;
		
		currentSequenceDirection=1;		
		
	}
	
	public String getInterventionTitle()
	{
		return applicationConfiguration.getTitle();
	}
	
	public String getInterventionLogo()
	{
		return applicationConfiguration.getLogo();
	}
	
	public void freshStart(){
		
		Log.i(TAG, "Fresh Start Run! Setting Position to Front Menu");
		
		editor.putString("currentItem", applicationConfiguration.getFrontId());
		editor.putString("currentMenu", applicationConfiguration.getFrontId());
		editor.putBoolean("inSequence", false);
		editor.commit();
				
		start();
		
	}
	
	@Override
	public void start() {
		Log.i(TAG, "Starting DAC");
		if(!checkAndResolveSequence())
		{
			Log.i(TAG, "Starting DAC - Not in Sequence");
				Intent activityIntent = new Intent(context, MenuActivity.class);
				//context.getIntent().get
				//int instruction = callParams.getInt(DynamicApplicationConstants.CONTROL_INSTRUCTION, DynamicApplicationConstants.START_APP);
				
				if (context.getIntent().getExtras() != null) {
					Set<String> keys = context.getIntent().getExtras().keySet();
					for (String key : keys) {
						Log.d(TAG, "Key "+key);
					}
				}
				else {
					Log.d(TAG, "Keys : none");
				}
				if (context.getIntent().hasExtra(DynamicApplicationConstants.CONTROL_INSTRUCTION)) {
					if (context.getIntent().getIntExtra(DynamicApplicationConstants.CONTROL_INSTRUCTION, 
									DynamicApplicationConstants.START_APP) == DynamicApplicationConstants.PROCESS_NOTIFICATION){
						Log.d(TAG, "Start from notif");
						context.startActivity(activityIntent);
					} else {
						Log.d(TAG, "Start from not notif");
						activityIntent.putExtra("label", context.getIntent().getExtras());
						context.startActivity(activityIntent);
					}
				}
				
				else {
					Log.d(TAG, "Start from elsewhere");
					activityIntent.putExtra("label", context.getIntent().getExtras());
					context.startActivity(activityIntent);
					//context.startActivity(activityIntent, context.getIntent().getExtras());
				}
				
				Log.i(TAG, "START MENU");
		}
	}
	
	
	//dirty - allows launch activity to be called with a boolean to ignore the skipper in the case of a feedback edit jump
	public void launchActivity(final String id){
		launchActivity(id, false);
	}
	
	public void launchActivity(final String id, boolean ignoreSkipper)
	{
		Log.d(TAG, "Launch id "+id);
		UbhaveMenuItem item = getItem(id);		
		editor.putString("currentItem", id);
		editor.commit();		
		
		UsageLog log = new UsageLog(item.getType(),
				 DynamicApplicationConstants.EVENT_ACTIVITY_START,
				 -1, // No event subtype for EVENT_ACTIVITY_START
				 System.currentTimeMillis(),
				 id,
				 item.getLabel());
				
		ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
		
		logger.logAppUsage(log);
		
		//Grab the items content and check if its a ref, if it is grab the refs content.
		//TODO: This will work for now but is a clumsy solution. We should consider separating content and structure entirely.
		if(item.getIsRef()){
			launchActivity(item.getRef());
		}
		else if(item.evalNavCond()||ignoreSkipper){		
			
			assignCurrentController(item);
			
			currentController.start();
		}
		else if(!item.evalNavCond()){
			Log.i(TAG, "Navigation condition not satisfied");
			activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_SKIPPED);
		}
	}
	
	private void assignCurrentController(UbhaveMenuItem item){
		
		if(item.getType()==DynamicApplicationConstants.TYPE_MENU){
			editor.putString("currentMenu", item.getID());
			editor.commit();
			try {
				//currentController = new DynamicApplicationController(context);
				currentController = this;
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.i(TAG, "Launch Menu");
			
		}
		else if(item.getType()==DynamicApplicationConstants.TYPE_SURVEY){
			currentController = new QuestionnaireController(context, item.getContent(), item.getID(), this);
			Log.i(TAG, "Launch Survey");
		}
		else if(item.getType()==DynamicApplicationConstants.TYPE_DIARY){
			currentController = new DiaryController(context, item.getContent(), item.getID(), this);
			Log.i(TAG, "Launch Diary");
		}
		else if(item.getType()==DynamicApplicationConstants.TYPE_INFO){
			currentController = new InformationController(context, item.getContent(), item.getID(), this);
			Log.i(TAG, "Launch Info");
		}
		else if(item.getType()==DynamicApplicationConstants.TYPE_SETTINGS){
			currentController = new SettingsController(context,item.getContent(),item.getID(),this);
			Log.i(TAG, "Launch Settings");
		}
		else if(item.getType()==DynamicApplicationConstants.TYPE_SEQUENCE){
			editor.putString("currentSequence", item.getID());
			editor.putInt("currentSequencePosition", 0);
			editor.putBoolean("inSequence", true);
			editor.commit();
			try {
				currentController = new DynamicApplicationController(context);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.i(TAG, "Launch Sequence");
		}
		else if(item.getType()==DynamicApplicationConstants.TYPE_PLANNER){
			currentController = new PlannerController(context,item.getContent(), item.getID(), this);
			Log.i(TAG, "Launch Planner");
		}
		else if(item.getType()==DynamicApplicationConstants.TYPE_PLANNER_REVIEW){
			currentController = new PlannerReviewController(context,item.getContent(), item.getID(), this);
			Log.i(TAG, "Launch Planner Review");
		}
		
		currentController.setSkipable(item.isSkipable());
		
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.i(TAG, "DAC oar resultCode: "+resultCode);
		Log.i(TAG, "DAC oar CC: "+currentController);
		if (currentController != null)
		{
			currentController.onActivityResult(requestCode, resultCode, data);
		}
		//Have lost current control for some reason so reassign it
		else{
			String id = sharedPrefs.getString("currentItem", applicationConfiguration.getFrontId());
			UbhaveMenuItem item = getItem(id);	
			assignCurrentController(item);
			
			currentController.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	
	public void processCall(Bundle callParams) {
		Log.d(TAG, "processCall");
		for (String key : callParams.keySet()) {
			Log.d(TAG, "callParam "+key);
		}
		int instruction = callParams.getInt(DynamicApplicationConstants.CONTROL_INSTRUCTION, DynamicApplicationConstants.START_APP);
		
		
		Log.d(TAG, "processCall instruction "+instruction);
		switch (instruction) {
	//		case DynamicApplicationConstants.START_APP:
	//			startApp(R.raw.app);
	//			break;
			case DynamicApplicationConstants.PROCESS_NOTIFICATION:
				
				NotificationTriggerAction action = (NotificationTriggerAction) callParams.getParcelable(DynamicApplicationConstants.NOTIFICATION_EXTRA);
				int triggerType = callParams.getInt(DynamicApplicationConstants.TRIGGER_TYPE);
				long triggerTime = callParams.getLong(DynamicApplicationConstants.TRIGGER_TIME, -1);
				int notificationNum = callParams.getInt(DynamicApplicationConstants.CURRENT_TRIGGER_NO);
				String triggerName = callParams.getString(DynamicApplicationConstants.TRIGGER_NAME);
				processNotification(action, triggerType, triggerName, triggerTime, notificationNum);							
		}
		//callParams.getParcelable(DynamicApplicationConstants.NOTIFICATION_EXTRA);
	}
	
	public void processNotification(NotificationTriggerAction action, 
									int triggerType, 
									String triggerName,
									long triggerTime, 
									int notificationNum) {
		
		NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		nm.cancel(DynamicApplicationConstants.NOTIFICATION_CODE);
		
		/*if (action.isDialog()) {
			Log.d(TAG, "Requires a dialog");
		}
		String notificationType = action.getNotificationType();*/

		String value = "time_error";
		
		long currentTime = System.currentTimeMillis();
		
		if (triggerTime >0) {
			
			value = InterruptibilityLearner.INTERRUPTIBLE_NO;
			
			if (currentTime - triggerTime < DynamicApplicationConstants.INTER_TRIGGER_THOLD_TIME) {
				value = InterruptibilityLearner.INTERRUPTIBLE_YES;
			} 
		}
		
		// Log notification success:
		UbhaveNotificationAnswer answer = new UbhaveNotificationAnswer(
				notificationNum, 
				triggerName, 
				value, 
				System.currentTimeMillis());
		ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
		logger.logNotificationAnswer(answer);
		
		if ((triggerType == Constants.MOD_INTERRUPTIBILITY) &&
			!value.equals("time_error")) {
		
			Intent serviceIntent = new Intent(UbhaveApplication.getContext(),DynamicApplicationService.class);
			serviceIntent.putExtra(DynamicApplicationConstants.SERVICE_INSTRUCTION,
									DynamicApplicationConstants.TRAIN_INTER_LEARNER);
			serviceIntent.putExtra(DynamicApplicationConstants.TRAIN_WITH_VALUE, value);		
			UbhaveApplication.getContext().startService(serviceIntent);				
		}
		
		String nactionID = action.getID();

		Log.d(TAG, "Notification action name: "+nactionID);
		
		//processFunctions(action, false);
		
		//doAction(notificationType);
		launchActivity(nactionID);

		processFunctions(action, true);
		
		//switch (notificationType) {
			
		//	case DynamicApplicationConstants.NOTIFICATION_DIARY:
		//		diaryController.startDiary(notificationID);
		//		break;
		//	
		//	case DynamicApplicationConstants.NOTIFICATION_INFO:
		//		informationController.showInformation();
		//		break;
		//		
		//	case DynamicApplicationConstants.NOTIFICATION_SURVEY:
		//		questionnaireController.startSurvey(notificationID);
		//		break;
		//}
	}
	
	public UbhaveMenuItem getItem(String id){
		
		for(UbhaveMenuItem item:applicationConfiguration.getContent()){			
			
			if(item.getID().equals(id)){
				return item;
			}
			else if(item.getType()==DynamicApplicationConstants.TYPE_MENU||
					item.getType()==DynamicApplicationConstants.TYPE_SEQUENCE){
				try{
					UbhaveMenuItem smItem = getItemFromMenu(id, MenuLoader.loadMenu(item.getContent()));
					if(smItem!=null&&smItem.getID().equals(id)){
						return smItem;
					}
				}
				catch(JSONException e){
					e.printStackTrace();
					Log.e(TAG, "JSONException parsing content for sub menu "+id);
					Log.e(TAG,e.getMessage());
				}
			}
		}
		
		return null;
		
	}
	
	public ArrayList<UbhaveMenuItem> getCurrentMenuItems(){
		
		for(UbhaveMenuItem item:currentMenu.getItems()){
			item.evalNavCond();
		}
		
		return currentMenu.getItems();
	}
	
	public UbhaveMenuItem getItemFromMenu(String id, UbhaveMenu m){
		for(UbhaveMenuItem item:m.getItems()){
			if(item.getID().equals(id)){
				return item;
			}
			else if(item.getType()==DynamicApplicationConstants.TYPE_MENU||
					item.getType()==DynamicApplicationConstants.TYPE_SEQUENCE){
				try{
					UbhaveMenuItem smItem = getItemFromMenu(id, MenuLoader.loadMenu(item.getContent()));
					if(smItem!=null&&smItem.getID().equals(id)){
						return smItem;
					}
				}
				catch(JSONException e){
					e.printStackTrace();
					Log.e(TAG, "JSONException parsing content for sub menu "+id);
					Log.e(TAG,e.getMessage());
				}
			}
		}
		
		return null;
	}
	
	
	public UbhaveFunction getFunction(String id){
		for(UbhaveMenuItem item:applicationConfiguration.getContent()){	
			UbhaveFunction function = getFunctionFromItem(id, item);
			if(function!=null)
				return function;
		}
		
		for(UbhaveFunction function:applicationConfiguration.getFunctionPool()){	
			if(function!=null&&function.getId().equals(id))
				return function;
		}
		
		return null;
		
	}
	
	public UbhaveFunction getFunctionFromItem(String id, UbhaveMenuItem item){
		
		
		for(UbhaveFunction function: item.getPostFunctions()){
			if(function.getId().equals(id))
				return function;
		}
		
		if(item.getType()==DynamicApplicationConstants.TYPE_MENU||item.getType()==DynamicApplicationConstants.TYPE_SEQUENCE){
			try {
				UbhaveMenu menu = MenuLoader.loadMenu(item.getContent());
				for(UbhaveMenuItem mItem:menu.getItems()){
					UbhaveFunction function = getFunctionFromItem(id, mItem);
					if(function!=null)
						return function;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	
	public void activityFinished(int finishMode, String extra){
		
		String currentItemID = sharedPrefs.getString("currentItem","");
		if (!currentItemID.equals("")) {
			UbhaveMenuItem currentItem = getItem(currentItemID);
			UsageLog log = new UsageLog(currentItem.getType(),
					 DynamicApplicationConstants.EVENT_ACTIVITY_FINISH,
					 finishMode,
					 System.currentTimeMillis(),
					 currentItemID,
					 currentItem.getLabel());
					
			ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
			
			logger.logAppUsage(log);
		}
		
		Log.i(TAG,"ActivityFinished "+finishMode);
		

		switch(finishMode){
		case DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE: 
			Log.i(TAG, "ActivityFinished - Complete");
			currentSequenceDirection=1;
			currentSequencePosition++;
			processPostFunctions();
			checkAndResolveSequence();
			break;
		case DynamicApplicationConstants.ACTIVITY_FINISH_BACK: 
			Log.i(TAG, "ActivityFinished - Back");
			currentSequenceDirection=-1;
			currentSequencePosition--;
			checkAndResolveSequence();
			break;
		case DynamicApplicationConstants.ACTIVITY_FINISH_SKIPPED: 
			Log.i(TAG, "ActivityFinished - Skipped");
			currentSequencePosition+=currentSequenceDirection;
			checkAndResolveSequence();
			break;
		case DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_AND_JUMP:
			//TODO: This is for the edit plans aspect of the planner so that the feedback system can have a option diversion to edit plans. This jumping out of sequence is a bit scary and could lead to bad things but its the only way it will work for now.
			Log.i(TAG, "ActivityFinished - Complete and Jump");
			processPostFunctions();
			launchActivity(extra,true);
			break;
		case DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_AND_JUMP_TO_FRONT:
			//TODO: This is for the ActionBar Home Jump - as with above the "goto" aspect of this is a bit dodgy
			Log.i(TAG, "ActivityFinished - Complete and Jump To Front");
			editor.putBoolean("inSequence", false);
			launchActivity(applicationConfiguration.getFrontId());
			break;
		case DynamicApplicationConstants.ACTIVITY_FINISH_STAGE_EXECUTE_FUNCTIONS: 
			Log.i(TAG, "ActivityFinished - Stage Execute Func");
			processPostFunctions();
			break;
		case DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_NO_FUNCTIONS: 
			Log.i(TAG, "ActivityFinished - Complete No Functions");
			currentSequenceDirection=1;
			currentSequencePosition++;
			checkAndResolveSequence();
			break;
		}
		
	}
	
	public void activityFinished(int finishMode){
		activityFinished(finishMode, DynamicApplicationConstants.DEFAULT_ITEM_ID);		
	}
		
	
	private boolean checkAndResolveSequence(){
		if(sharedPrefs.getBoolean("inSequence",false)){
			if(currentSequencePosition==currentSequence.getItems().size()){
				editor.putBoolean("inSequence", false);
				editor.commit();
				processPostFunctions(sharedPrefs.getString("currentSequence", 
						DynamicApplicationConstants.DEFAULT_SEQUENCE_ID));
				return false;
			}
			else if(currentSequencePosition<0){
				editor.putBoolean("inSequence", false);
				editor.commit();
				return false;
			}
			else{				
				launchActivity(currentSequence.getItems().get(currentSequencePosition).getID());
				return true;
			}
		}
		return false;
	}
	
	public void checkAndResolveLaunchActivity(){
		Log.i(TAG,"ResolveLaunch");
		if(InterventionDataAccess.checkIfInterventionFirstRun()){
			//TODO: place holder until first run method works full
			InterventionDataAccess.storeCustomVariable("firstRun", "false");
			////
			launchActivity(applicationConfiguration.getLaunchId());
			
			String launchF = applicationConfiguration.getLaunchFunction();
			if(launchF!=null)
				getFunction(launchF).run(this);
			
		}
	}
	
	private void processPostFunctions(){
		Log.i(TAG,"ProcessPostFunc");
		processPostFunctions(sharedPrefs.getString("currentItem",
				DynamicApplicationConstants.DEFAULT_ITEM_ID));		
	}
	
	private void processPostFunctions(String a_itemID){
		Log.i(TAG,"ProcessPostFunc for "+a_itemID);
		ArrayList<UbhaveFunction> functions = getItem(a_itemID).getPostFunctions();
		for(UbhaveFunction function:functions){
			function.run(this);
		}
	}
	
	// TODO: This would process only functions for notifications that are answered.
	// We should think of a way to decouple function eval from DAC and Activities.
	private void processFunctions(TriggerAction a_action, boolean a_post){
		Log.i(TAG,"processFunctions for notifs");
		JSONArray functions = null;
		if (a_post) 
			functions = a_action.getPostAction();
		// TODO: implement pre-function
		//else 
		//	functions = a_action.getPreAction();
		if (functions != null) {
			for(int k = 0; k<functions.length();k++){
				UbhaveFunction f;
				try {
					f = UbhaveFunctionLoader.loadFunction(functions.getJSONObject(k));
					Log.i(TAG,"Function "+f.getId());
					f.run(this);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}