/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey.data.as;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

public class AnswerList
{
	private String surveyId;
	private ArrayList<AbstractAnswer> answers;

	public AnswerList(String id)
	{
		surveyId = id;
		answers = new ArrayList<AbstractAnswer>();
	}

	public String getId()
	{
		return surveyId;
	}

	public void clear()
	{
		answers.clear();
	}

	public void add(AbstractAnswer answer)
	{
		answers.add(answer);
	}

	public void remove(int index)
	{
		answers.remove(index);
	}

	public int size()
	{
		return answers.size();
	}

	public JSONObject toJSON()
	{
		try
		{
			SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
			JSONObject data = new JSONObject();
			data.put(DynamicApplicationConstants.UID, settings.getString(DynamicApplicationConstants.UID, null));
			data.put(DynamicApplicationConstants.CURRENT_TIME, System.currentTimeMillis());
			data.put(DynamicApplicationConstants.SURVEY_ID, surveyId);

			JSONArray answerJSON = new JSONArray();
			for (AbstractAnswer answer : answers)
			{
				answerJSON.put(answer.toJSON());
			}
			data.put(DynamicApplicationConstants.ANSWERS, answerJSON);
			return data;
		}
		catch (JSONException e)
		{
			return null;
		}
	}
	
	public HashMap<String,String> getFlatEntries() {
		HashMap<String,String> flatEntries = new HashMap<String,String>();
		for (AbstractAnswer answer : answers) {
			HashMap<String,String> tmpEntries = answer.getFlatEntries();
			for (Map.Entry<String,String> entry : tmpEntries.entrySet()){
				String key = this.getId()+"_"+entry.getKey();
				flatEntries.put(key, entry.getValue());
			}
		}
		return flatEntries;
	}

	private static String getImei()
	{
		String imeiPhone = null;
		Context context = UbhaveApplication.getContext();
		if (context != null)
		{
			TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			if (manager != null)
			{
				imeiPhone = manager.getDeviceId();
			}
		}
		return imeiPhone;
	}
}
