/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner.data;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.JSONLoader;
import org.ubhave.ubhavedynamicapp.info.data.InformationListLoader;
import org.ubhave.ubhavedynamicapp.info.data.TextInformation;
import org.ubhave.ubhavedynamicapp.planner.PlannerReviewController;

public class ReviewerLoader extends JSONLoader{
	
	private static final String PLANNER_KEY = "plannerKey";
	private static final String FEEDBACK = "feedback";
	private static final String HEADER_TEXT = "headerText";
	private static final String TITLE = "title";
	private static final String EDIT_PLANNER = "editPlanner";
	private static final String TYPE = "type";
	private static final String UPPER_BOUND = "upperBound";
	private static final String LOWER_BOUND = "lowerBound";

	public static PlanReviewer loadReviewer(JSONObject reviewerJO, String id) throws JSONException{
		String plannerKey = id; //reviewerJO.getString(PLANNER_KEY);
		String title = reviewerJO.getString(TITLE);
		String headerText = reviewerJO.getString(HEADER_TEXT);
		JSONObject feedbackJO = reviewerJO.getJSONObject(FEEDBACK);
		
		// TODO: check if null key is OK:
		TextInformation feedback = (TextInformation)InformationListLoader
				.loadInformation(feedbackJO, null);
		
		String editPlanner = reviewerJO.getString(EDIT_PLANNER);
		int type = PlannerReviewController.REVIEWER_TYPE_CHECK;
		if(reviewerJO.has(TYPE))
			type=reviewerJO.getInt(TYPE);
		
		if(type==PlannerReviewController.REVIEWER_TYPE_SCORE){
			int[] bounds = new int[]{reviewerJO.getInt(LOWER_BOUND),reviewerJO.getInt(UPPER_BOUND)};
			return new PlanScoreReviewer(plannerKey,title,headerText,feedback,editPlanner,bounds);
		}
		else if(type==PlannerReviewController.REVIEWER_TYPE_VIEW){
			return new PlanViewReviewer(plannerKey,title,headerText,feedback,editPlanner);
		}
		else
			return new PlanReviewer(plannerKey,title,headerText,feedback,editPlanner);
			
	}
	
}