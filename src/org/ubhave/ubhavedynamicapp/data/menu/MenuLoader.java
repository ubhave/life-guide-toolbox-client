package org.ubhave.ubhavedynamicapp.data.menu;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.JSONLoader;

public class MenuLoader extends JSONLoader{
	
	private final static String ITEMS = "items";
	private final static String LABEL = "label";
	private final static String TYPE = "type";
	private final static String ID = "id";
	private final static String CONTENT = "content";
	
	public static Menu loadMenu(JSONObject menuJSON) throws JSONException{
		
		String menuLabel = menuJSON.getString(LABEL);
		ArrayList<MenuItem> items = new ArrayList<MenuItem>();	
		JSONArray itemsJSON = menuJSON.getJSONArray(ITEMS);
		
		for(int i=0; i<itemsJSON.length();i++){			
			JSONObject itemJSON = itemsJSON.getJSONObject(i);
			
			int type = itemJSON.getInt(TYPE);
			int id = itemJSON.getInt(ID);
			String itemLabel = itemJSON.getString(LABEL);
						
			JSONObject content = itemJSON.getJSONObject(CONTENT);
			
			items.add(new MenuItem(type,id,itemLabel,content));
			
		}
		
		return new Menu(menuLabel, items);
		
	}
	
}