/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.app.data;

import java.util.ArrayList;

import org.ubhave.ubhavedynamicapp.app.data.menu.UbhaveMenuItem;
import org.ubhave.ubhavedynamicapp.function.UbhaveFunction;

public class DynamicApplication{
	
	public final static String TITLE = "applicationName";
	public final static String FRONT = "front";
	public final static String LAUNCH_ITEM = "launchItem";
	public static final String CONTENT = "content";
	public static final String LOGO = "logo";
	public static final String ICON = "icon";
	public static final String NOTIF_ICON = "notifIcon";
	public static final String THEME = "theme";
	public static final Object THEME_NEUTRAL = "neutral";
	public static final Object THEME_HEALTHY = "healthy";
	public static final String FUNCTION_POOL = "functionPool";
	public static final String LAUNCH_FUNCTION = "launchFunction";
	public static final String MODEL_VERSION = "modelVersion";
		
	private final String applicationTitle;
	private String logo;
	private String icon;
	private String notifIcon;	
	private final int theme;
	//private final UbhaveMenuItem front; //should potentially be some kind of generic activity at some point
	private final ArrayList<UbhaveMenuItem> content;
	private final ArrayList<UbhaveFunction> functionPool;
	private final String frontId;
	private final String launchId;
	private String launchFunction;
	private final String modelVersion;
	
	
	public DynamicApplication(final String title, final ArrayList<UbhaveMenuItem> c, final ArrayList<UbhaveFunction> fp, String front, String launch, int t, String mv)
	{
		this.applicationTitle = title;
		this.content = c;
		this.functionPool=fp;
		this.frontId=front;
		this.launchId=launch;
		this.theme = t;
		this.modelVersion=mv;
	}
	
	public String getTitle()
	{
		return applicationTitle;
	}
	
	public ArrayList<UbhaveMenuItem> getContent()
	{
		return content;
	}
	
	public ArrayList<UbhaveFunction> getFunctionPool()
	{
		return functionPool;
	}
	
	public String getFrontId()
	{
		return frontId;
	}
	
	public String getLaunchId()
	{
		return launchId;
	}
	
	public int getTheme()
	{
		return theme;
	}
	
	public String getLogo(){
		return logo;
	}
	
	public String getNotifIcon(){
		return notifIcon;
	}
	
	public void setLogo(String l){
		logo=l;
	}
	
	public String getIcon(){
		return icon;
	}
	
	public void setIcon(String i){
		icon=i;
	}
	
	public void setNotifIcon(String i){
		notifIcon=i;
	}

	public void setLaunchFunction(String lf) {
		launchFunction=lf;
	}
	
	public String getLaunchFunction() {
		return launchFunction;
	}
		
}