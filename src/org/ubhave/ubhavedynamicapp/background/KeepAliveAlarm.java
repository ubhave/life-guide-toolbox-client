/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.background;

import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.InterventionPreferences;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

// TODO: Do we need to make sure that the same alarm does not exist
// before insantiating it?

public class KeepAliveAlarm extends BroadcastReceiver {

	private static final String TAG = "KeepAliveAlarm";

	private static final Object lock = new Object();

	public void onReceive(Context context, Intent intent) {
		
		Log.d(TAG, "onReceive");
		
		String action = intent.getAction();
		
		if (action != null) {
			
			Log.d(TAG, "Action "+action);
		
			// Safeguard against triggers firing without the rest 
			// of the structure initialised. 
			if (InterventionPreferences.doesInterventionExist()) {
				keepAliveService(context);
			}
			
			if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
				startAlarm(context);
			}
		} 
		
	}

	public static void startAlarm(Context context) {
		
		Intent serviceIntent = new Intent(UbhaveApplication.getContext(),DynamicApplicationService.class);
		serviceIntent.putExtra(DynamicApplicationConstants.SERVICE_INSTRUCTION,
								DynamicApplicationConstants.CHECK_TRIGGERS);
				
		UbhaveApplication.getContext().startService(serviceIntent);
		
		/* OLD CODE, DIDN'T CHECK TRIGGERS	
		 * UbhaveApplication.getContext().startService(
				new Intent(UbhaveApplication.getContext(), 
						DynamicApplicationService.class)
				);*/
		
		
		synchronized (lock) {
			
			Log.d(TAG, "startAlarm");
			AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			Intent i = new Intent(DynamicApplicationConstants.ACTION_SERVICE_ALARM_RECEIVER);
			PendingIntent pi = PendingIntent.getBroadcast(context, 1, i, 0);
			
			int type = AlarmManager.ELAPSED_REALTIME_WAKEUP;
			long interval = DynamicApplicationConstants.WAKE_UP_PERIOD;
			long triggerTime = SystemClock.elapsedRealtime() + interval;
			
			am.setRepeating(type, triggerTime, interval, pi);
		}
	}

	private static void keepAliveService(Context context)  {
		
		Log.d(TAG, "keepAliveService");
		
		Intent serviceIntent = new Intent(UbhaveApplication.getContext(),DynamicApplicationService.class);
		serviceIntent.putExtra(DynamicApplicationConstants.SERVICE_INSTRUCTION,
								DynamicApplicationConstants.CHECK_TRIGGERS);
				
		UbhaveApplication.getContext().startService(serviceIntent);
		
		/*synchronized (lock) {
			DynamicApplicationService service =  DynamicApplicationService.getInstance();
			if (service != null) service.checkTriggers();
		}*/
	}
}
