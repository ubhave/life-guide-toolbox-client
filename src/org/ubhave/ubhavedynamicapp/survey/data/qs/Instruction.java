/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey.data.qs;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Instruction extends AbstractQuestion
{

	private static final String DIRECTIONS = "directions";
	private static final String BUTTON_TEXT = "buttontext";
	private final String directions;
	private final String buttonText;

	public Instruction(String identifier, JSONObject question, String title, String text, String fText) throws JSONException
	{
		super(identifier, AbstractQuestion.INSTRUCTION, title, text, fText);
		directions = question.getString(DIRECTIONS);
		buttonText = question.getString(BUTTON_TEXT);
	}

	public Instruction(String identifier, String title, String subtitle, String text, String button, String fText)
	{
		super(identifier, AbstractQuestion.INSTRUCTION, title, subtitle, fText);
		directions = text;
		buttonText = button;
	}

	public Instruction(Parcel in)
	{
		super(in);
		directions = in.readString();
		buttonText = in.readString();
	}

	public String getDirections()
	{
		return directions;
	}

	public String getButtonText()
	{
		return buttonText;
	}

	public static final Parcelable.Creator<Instruction> CREATOR = new Parcelable.Creator<Instruction>()
	{
		public Instruction createFromParcel(Parcel in)
		{
			return new Instruction(in);
		}

		public Instruction[] newArray(int size)
		{
			return new Instruction[size];
		}
	};

	@Override
	public void writeToParcel(Parcel out, int flags)
	{
		super.writeToParcel(out, flags);
		out.writeString(directions);
		out.writeString(buttonText);
	}

}
