/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp;


import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class InterventionPreferences
{
	private final static String preferenceFile = "interventionPreferences";
	private final static String interventionSet = "interventionExists";
	private final static String currentInterventionId = "currentInterventionId";
	private final static String interventionDetails = "interventionDetails";
	private static final String TAG = "InterventionPreferences";

	protected static SharedPreferences getSettings()
	{
		return UbhaveApplication.getContext().getSharedPreferences(preferenceFile, Context.MODE_PRIVATE);
	}
	
//	public static void setLong(String preferenceFile, String key, long value)
//	{
//		SharedPreferences.Editor editor = getSettings().edit();
//		editor.putLong(key, value);
//		editor.commit();
//	}
	
//	protected static long getLong(String preferenceFile, String key)
//	{
//		return getSettings().getLong(key, Long.MIN_VALUE);
//	}
	
	private static void setString(String key, String value)
	{
		SharedPreferences.Editor editor = getSettings().edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	private static String getString(String key)
	{
		return getSettings().getString(key, null);
	}
//	
//	public static void setInteger(String preferenceFile, String key, int value)
//	{
//		SharedPreferences.Editor editor = getSettings().edit();
//		editor.putInt(key, value);
//		editor.commit();
//	}
//	
//	protected static Integer getInteger(String preferenceFile, String key)
//	{
//		return getSettings().getInt(key, 0);
//	}
	
	private static void removeKey(String key)
	{
		SharedPreferences.Editor editor = getSettings().edit();
		editor.remove(key);
		editor.commit();
	}
	
	private static void setBoolean(String key, boolean value)
	{
		SharedPreferences.Editor editor = getSettings().edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	
	private static boolean getBoolean(String key)
	{
		return getSettings().getBoolean(key, false);
	}
	
	public static void setIntervention(String interventionId)
	{
		setString(currentInterventionId, interventionId);
		setBoolean(interventionSet, true);
	}
	
	public static String getInterventionId()
	{
		return getString(currentInterventionId);
	}
	
	public static boolean doesInterventionExist()
	{
		return getBoolean(interventionSet);
	}
	
	public static void removeIntervention()
	{
		removeKey(interventionSet);
		removeKey(currentInterventionId);
	}

	public static void setInterventionDetails(HashMap<String, String> details)
	{
		SharedPreferences.Editor editor = getSettings().edit();
		for( String key: details.keySet() ) {
			editor.putString(interventionDetails+'_'+key, details.get(key) );
			//Log.d(TAG, "Stored key "+key+" value "+details.get(key));
		}
		editor.commit();
	}
	public static String getInterventionDetails(String key) {
		return getString(interventionDetails+'_'+key);
	}
	
	
}
