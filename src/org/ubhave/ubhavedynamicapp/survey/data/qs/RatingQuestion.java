/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey.data.qs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class RatingQuestion implements Parcelable
{
	private static final String TITLE = "title";
	private static final String MINVALUE = "minvalue";
	private static final String MAXVALUE = "maxvalue";
	private static final String INITVALUE = "initvalue";
	private static final String CHOICES = "choices";
	private static final String ID = "question_id";

	private final String title;
	private final String[] descriptions;
	private final int minValue, maxValue, initValue, id;

	public RatingQuestion(JSONObject data) throws JSONException
	{
		title = data.getString(TITLE);
		minValue = data.getInt(MINVALUE);
		maxValue = data.getInt(MAXVALUE);
		initValue = data.getInt(INITVALUE);
		id = data.getInt(ID);

		JSONArray answerJSON = (JSONArray) data.get(CHOICES);
		descriptions = new String[answerJSON.length()];
		for (int i = 0; i < answerJSON.length(); i++)
		{
			descriptions[i] = answerJSON.getString(i);
		}
	}

	public RatingQuestion(Parcel in)
	{
		title = in.readString();
		id = in.readInt();
		minValue = in.readInt();
		maxValue = in.readInt();
		initValue = in.readInt();
		descriptions = new String[in.readInt()];
		in.readStringArray(descriptions);
	}

	public String getTitle()
	{
		return title;
	}

	public String[] getDescriptions()
	{
		return descriptions;
	}

	public int getMinValue()
	{
		return minValue;
	}

	public int getIdentifier()
	{
		return id;
	}

	public int getMaxValue()
	{
		return maxValue;
	}

	public int getInitialValue()
	{
		return initValue - 1;
	}

	public static final Parcelable.Creator<RatingQuestion> CREATOR = new Parcelable.Creator<RatingQuestion>()
	{
		public RatingQuestion createFromParcel(Parcel in)
		{
			return new RatingQuestion(in);
		}

		public RatingQuestion[] newArray(int size)
		{
			return new RatingQuestion[size];
		}
	};

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flag)
	{
		out.writeString(title);
		out.writeInt(id);
		out.writeInt(minValue);
		out.writeInt(maxValue);
		out.writeInt(initValue);
		out.writeInt(descriptions.length);
		out.writeStringArray(descriptions);
	}

}
