/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;

import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;

public class PlanReview implements Parcelable{
	
	PlanSet planSet;
	
	HashMap<String, PlanReviewResult> planReviewResults;
	
	int score;
	
	public PlanReview(PlanSet set, HashMap<String,PlanReviewResult> results){
		planSet=set;
		planReviewResults=results;
		
		score = 0;
		Iterator<Entry<String, PlanReviewResult>> it = planReviewResults.entrySet().iterator();
		while (it.hasNext()){
			Map.Entry<String, PlanReviewResult> entry = it.next();
			score += entry.getValue().getPlanResult();
		}
	}
	
	public PlanSet getPlanSet(){
		return planSet;
	}
	
	public HashMap<String, PlanReviewResult> getReviewResults(){
		return planReviewResults;
	}
	
	public int getScore(){
		return score;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	
	
	public JSONObject toJSON() {
		
		JSONObject data = new JSONObject();

		JSONArray planResultsArray = new JSONArray();
		Iterator<Entry<String, PlanReviewResult>> it = planReviewResults.entrySet().iterator();
		while (it.hasNext()) {
			try {
				Map.Entry<String, PlanReviewResult> entry = it.next();
				JSONObject elem = new JSONObject();				
				elem.put(entry.getKey(), entry.getValue().toJSON());
				planResultsArray.put(elem);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		try {
			SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
			data.put(DynamicApplicationConstants.UID, settings.getString(DynamicApplicationConstants.UID, null));
			data.put(DynamicApplicationConstants.CURRENT_TIME, System.currentTimeMillis());			
			data.put(DynamicApplicationConstants.PLAN_SET, planSet.toJSON());
			data.put(DynamicApplicationConstants.SCORE, score);
			data.put(DynamicApplicationConstants.PLAN_REVIEW_RESULTS, planResultsArray);			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		
		/*Bundle b = new Bundle();
		b.putParcelable(DynamicApplicationConstants.PLAN_SET, planSet);
		
		ArrayList<String> plans = new ArrayList<String>();
		ArrayList<PlanReviewResult> results = new ArrayList<PlanReviewResult>();
		
		for(String plan:planReviewResults.keySet()){
			plans.add(plan);
			results.add(planReviewResults.get(plan));
		}
		
		b.putStringArrayList(DynamicApplicationConstants.PLAN_REVIEW_RESULTS_KEYS, plans);
		b.putParcelableArrayList(DynamicApplicationConstants.PLAN_REVIEW_RESULTS_VALUES, results);
				
		b.putInt(DynamicApplicationConstants.SCORE, score);
		dest.writeBundle(b);*/
		
		/*Bundle b = new Bundle();
		b.putParcelable(DynamicApplicationConstants.PLAN_SET, planSet);
		b.putSerializable(DynamicApplicationConstants.PLAN_REVIEW_RESULTS, planReviewResults);
		b.putInt(DynamicApplicationConstants.SCORE, score);
		dest.writeBundle(b);*/
		
		dest.writeParcelable(planSet, 0);
		
		ArrayList<String> plans = new ArrayList<String>();
		ArrayList<PlanReviewResult> results = new ArrayList<PlanReviewResult>();
		
		for(String plan:planReviewResults.keySet()){
			plans.add(plan);
			results.add(planReviewResults.get(plan));
		}
		
		dest.writeStringList(plans);
		dest.writeTypedList(results);
		dest.writeInt(score);
	}

	
	public PlanReview(Parcel in){
		
		planReviewResults = new HashMap<String, PlanReviewResult>();
		readFromParcel(in);
		
	}
	public void readFromParcel(Parcel in) {
		
		/*Bundle b = in.readBundle();
		planSet = b.getParcelable(DynamicApplicationConstants.PLAN_SET);
		
		planReviewResults = new HashMap<String, PlanReviewResult>();
		
		ArrayList<String> plans = b.getStringArrayList(DynamicApplicationConstants.PLAN_REVIEW_RESULTS_KEYS);
		ArrayList<PlanReviewResult> results = b.getParcelableArrayList(DynamicApplicationConstants.PLAN_REVIEW_RESULTS_VALUES);
		
		for(int i = 0; i<plans.size();i++){
			planReviewResults.put(plans.get(i), results.get(i));
		}
		
		score = b.getInt(DynamicApplicationConstants.SCORE);*/
		
		/*Bundle b = in.readBundle();
		planSet = b.getParcelable(DynamicApplicationConstants.PLAN_SET);
		planReviewResults = (HashMap<String, PlanReviewResult>) b.getSerializable(DynamicApplicationConstants.PLAN_REVIEW_RESULTS);
		score = b.getInt(DynamicApplicationConstants.SCORE);*/
		
		planSet=in.readParcelable(PlanSet.class.getClassLoader());
		
		ArrayList<String> plans = new ArrayList<String>();
		in.readStringList(plans);
		
		ArrayList<PlanReviewResult> results = new ArrayList<PlanReviewResult>();
		in.readTypedList(results, PlanReviewResult.CREATOR);
		
		planReviewResults = new HashMap<String,PlanReviewResult>();
		
		for(int i = 0; i<plans.size();i++){
			planReviewResults.put(plans.get(i), results.get(i));
		}
		
		score = in.readInt();
		
		
	}
	
	public static final Parcelable.Creator<PlanReview> CREATOR = new Parcelable.Creator<PlanReview>()
	{
		public PlanReview createFromParcel(Parcel in)
		{
			return new PlanReview(in);
		}

		public PlanReview[] newArray(int size)
		{
			return new PlanReview[size];
		}
	};
	
}