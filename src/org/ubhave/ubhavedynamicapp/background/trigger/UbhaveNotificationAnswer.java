package org.ubhave.ubhavedynamicapp.background.trigger;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;

import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;

public class UbhaveNotificationAnswer implements Parcelable {


	private int d_triggerNumber;
	private String d_triggerName;
	private String d_value;
	private long d_timeMillis;
	
	public UbhaveNotificationAnswer(int a_triggerID, 
							String a_triggerName,
							String a_value,
							long a_timeMillis){
		d_triggerNumber = a_triggerID;
		d_triggerName = a_triggerName;
		d_value = a_value;
		d_timeMillis = a_timeMillis;
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(d_triggerNumber);
		dest.writeString(d_triggerName);
		dest.writeString(d_value);
		dest.writeLong(d_timeMillis);
	}

	public static final Parcelable.Creator<UbhaveNotificationAnswer> CREATOR = 
			new Parcelable.Creator<UbhaveNotificationAnswer>(){

		@Override
		public UbhaveNotificationAnswer createFromParcel(Parcel source) {
			return new UbhaveNotificationAnswer(source);
		}

		@Override
		public UbhaveNotificationAnswer[] newArray(int size) {
			return new UbhaveNotificationAnswer[size];
		}
		
	};
	
	public UbhaveNotificationAnswer(Parcel a_parcel) {
		d_triggerNumber = a_parcel.readInt();
		d_triggerName = a_parcel.readString();
		d_value = a_parcel.readString();
		d_timeMillis = a_parcel.readLong();
	}
	
	public JSONObject toJSON() {
		try
		{
			SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
			JSONObject notifObject = new JSONObject();
			notifObject.put(DynamicApplicationConstants.UID, settings.getString(DynamicApplicationConstants.UID, null));
			notifObject.put(DynamicApplicationConstants.TRIGGER_ID, d_triggerNumber);
			notifObject.put(DynamicApplicationConstants.TRIGGER_NAME, d_triggerName);
			notifObject.put(DynamicApplicationConstants.ANSWER_VALUE, d_value);
			notifObject.put(DynamicApplicationConstants.CURRENT_TIME, d_timeMillis);
			return notifObject;
		}
		catch (JSONException e)
		{
			return null;
		}
	}
}
