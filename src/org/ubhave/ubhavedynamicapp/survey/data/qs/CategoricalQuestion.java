/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey.data.qs;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class CategoricalQuestion extends AbstractQuestion
{
	private static final String CHOICES = "choices";
	private static final String DETAILS = "details";
	private ArrayList<String> answers, details;

	public CategoricalQuestion(String id, JSONObject question, String title, String text, int type, String fText) throws JSONException
	{
		super(id, type, title, text, fText);
		JSONArray answerJSON = (JSONArray) question.get(CHOICES);
		JSONArray detailJSON = (JSONArray) question.get(DETAILS);

		answers = new ArrayList<String>();
		details = new ArrayList<String>();
		for (int i=0; i<answerJSON.length(); i++)
		{
			answers.add(answerJSON.getString(i));
			String detail;
			try
			{
				detail = detailJSON.getString(i);
			}
			catch (IndexOutOfBoundsException e)
			{
				detail = "";
			}
			catch (NullPointerException e)
			{
				detail = "";
			}
			details.add(detail);
		}
	}

	public CategoricalQuestion(Parcel in)
	{
		super(in);
		answers = new ArrayList<String>();
		in.readStringList(answers);

		details = new ArrayList<String>();
		in.readStringList(details);
	}

	public ArrayList<String> getAnswers()
	{
		return answers;
	}

	public ArrayList<String> getDetails()
	{
		return details;
	}

	public static final Parcelable.Creator<CategoricalQuestion> CREATOR = new Parcelable.Creator<CategoricalQuestion>()
	{
		public CategoricalQuestion createFromParcel(Parcel in)
		{
			return new CategoricalQuestion(in);
		}

		public CategoricalQuestion[] newArray(int size)
		{
			return new CategoricalQuestion[size];
		}
	};

	@Override
	public void writeToParcel(Parcel out, int flags)
	{
		super.writeToParcel(out, flags);
		out.writeStringList(answers);
		out.writeStringList(details);
	}

}
