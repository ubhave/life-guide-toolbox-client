/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.conditional;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.JSONLoader;

public class UbhaveConditionalLoader extends JSONLoader{
	
	final static String OP = "op";
	final static String CONDITIONS = "conditions";
	final static String VAR_A = "varA";
	final static String VAR_B = "varB";
	
	public static UbhaveConditional loadConditional(JSONObject condJO) throws JSONException{
		UbhaveConditional conditional = null;
		JSONObject conditionalJO = condJO;
		
		String operand = conditionalJO.getString(OP);
		
		if(operand.equals(UbhaveLogicalConditional.OR)||operand.equals(UbhaveLogicalConditional.AND)){
			
			ArrayList<UbhaveConditional> conds = new ArrayList<UbhaveConditional>();
			JSONArray condsArray = conditionalJO.getJSONArray(CONDITIONS);
			
			for(int i = 0; i<condsArray.length(); i++){
				conds.add(loadConditional(condsArray.getJSONObject(i)));
			}
			
			conditional=new UbhaveLogicalConditional(operand,conds);			
		}
		else if(operand.equals(UbhaveComparativeConditional.EQUALS)||operand.equals(UbhaveComparativeConditional.NOT_EQUALS)||operand.equals(UbhaveComparativeConditional.GREATER_THAN)||operand.equals(UbhaveComparativeConditional.LESS_THAN)||operand.equals(UbhaveComparativeConditional.GREATER_THAN_EQUAL)||operand.equals(UbhaveComparativeConditional.LESS_THAN_EQUAL)){
			String a = conditionalJO.getString(VAR_A);
			String b = conditionalJO.getString(VAR_B);
			conditional=new UbhaveComparativeConditional(operand,a,b);
		}
		
		
		return conditional;
	}
	
}