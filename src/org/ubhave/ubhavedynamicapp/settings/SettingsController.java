/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.settings;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.AbstractComponentController;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.settings.data.Settings;
import org.ubhave.ubhavedynamicapp.settings.data.SettingsLoader;
import org.ubhave.ubhavedynamicapp.settings.ui.SettingsActivity;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public class SettingsController extends AbstractComponentController{
	public static final String SETTINGS = "settings";

	private static final int SETTING_REQUEST_CODE = 101;

	private final Activity context;
	
	private Settings settings;
	
	private final JSONObject settingsJSON;
	
	private String settingsID; 
	
	public SettingsController(Activity a, JSONObject settingsJO, String id, DynamicApplicationController dynamicApplicationController){
		context = a;
		settingsID = id;
		settingsJSON = settingsJO;
		
		setAppController(dynamicApplicationController);
	}
		
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == SETTING_REQUEST_CODE)
		{
			try
			{				
				Log.d("UDA", "on res, "+resultCode);

				switch (resultCode)
				{
				case DynamicApplicationController.RESULT_SETTINGS_OK:
					start();
					break;
				}
			}
			catch (NullPointerException e)
			{}
		}
		else if(resultCode == DynamicApplicationController.RESULT_ACTIONBAR_HOME){
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_AND_JUMP_TO_FRONT);
		}
		else{
			//doesn't work yet
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE);
		}
	}

	@Override
	public void start() {
		try {
			settings = SettingsLoader.loadSettings(settingsJSON, settingsID);
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Intent activityIntent = new Intent(context, SettingsActivity.class);
		activityIntent.putExtra(SETTINGS, settings);
		context.startActivityForResult(activityIntent, SETTING_REQUEST_CODE);	
	}
}