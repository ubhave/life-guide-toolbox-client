package org.ubhave.ubhavedynamicapp.data.menu;

import org.json.JSONObject;

public class MenuItem{
	
	int type;
	int id;
	String label;
	JSONObject content;
	
	public MenuItem(int t, int i, String l, JSONObject c){
		type=t;
		id=i;
		label = l;
		content=c;
	}
	
	public int getType(){
		return type;
	}
	
	public int getID(){
		return id;		
	}
	
	public String getLabel(){
		return label;
	}
	
	public JSONObject getContent(){
		return content;
	}
	
}