package org.ubhave.ubhavedynamicapp.log;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class InteractionLog extends UsageLog{
	
	String d_interactionLabel; //Custom for each activity
	String d_interactionMode; //Custom for each activity

	public InteractionLog(int a_activityType, int a_eventSubType, long a_eventTime, String a_activityID, String a_activityLabel, String iLable, String iMode) {
		super(a_activityType, DynamicApplicationConstants.EVENT_INTERACTION, a_eventSubType, a_eventTime, a_activityID, a_activityLabel);
		
		d_interactionLabel=iLable;
		d_interactionMode=iMode;
		
	}
	
	public JSONObject toJSON() {
		try {
			SharedPreferences settings=PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
			
			JSONObject data = new JSONObject();
			data.put(DynamicApplicationConstants.UID, settings.getString(DynamicApplicationConstants.UID, null));
			data.put(DynamicApplicationConstants.CURRENT_TIME, d_eventTime);
			data.put(DynamicApplicationConstants.EVENT_TYPE, d_eventType);
			data.put(DynamicApplicationConstants.EVENT_SUBTYPE, d_eventSubType);
			data.put(DynamicApplicationConstants.ACTIVITY_ID, d_activityID);
			data.put(DynamicApplicationConstants.ACTIVITY_LABEL, d_activityLabel);
			data.put(DynamicApplicationConstants.ACTIVITY_TYPE, d_activityType);
			data.put(DynamicApplicationConstants.INTERACTION_LABEL, d_interactionLabel);
			data.put(DynamicApplicationConstants.INTERACTION_MODE, d_interactionMode);

			return data;
		}
		catch (JSONException e)
		{
			return null;
		}
	}
	
	public HashMap<String,String> getFlatEntries() {
		
		HashMap<String,String> flatEntries = new HashMap<String,String>();
		
		flatEntries.put(d_activityID+"_"+d_eventType+"_log", Long.toString(d_eventTime));
		
		return flatEntries;
	}
	
	
	
}