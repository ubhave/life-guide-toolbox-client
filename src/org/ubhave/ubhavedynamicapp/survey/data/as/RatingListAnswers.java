/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey.data.as;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.survey.data.qs.AbstractQuestion;

import android.os.Parcel;
import android.os.Parcelable;

public class RatingListAnswers extends AbstractAnswer
{
	private final RatingAnswer[] questions;

	public RatingListAnswers(String id, int[] questionIds, int[] answers)
	{
		super(id, AbstractQuestion.RATING_LIST);

		questions = new RatingAnswer[questionIds.length];
		for (int i = 0; i < questions.length; i++)
		{
			questions[i] = new RatingAnswer(questionIds[i], answers[i]);
		}
	}

	public RatingListAnswers(String id, int length)
	{
		super(id, AbstractQuestion.RATING_LIST);
		questions = new RatingAnswer[length];
	}

	private RatingListAnswers(Parcel in)
	{
		super(in);
		questions = new RatingAnswer[in.readInt()];
		for (int i = 0; i < questions.length; i++)
		{
			questions[i] = in.readParcelable(RatingAnswer.class.getClassLoader());
		}
	}

	public RatingAnswer[] getQuestions()
	{
		return questions;
	}

	@Override
	public HashMap<String, String> getFlatEntries() {
		HashMap<String,String> flatAnswers = new HashMap<String, String>();
		for (RatingAnswer rating : questions) {
			String answerID = this.getQuestionId()+"_"+rating.getId();
			String answerValue = Integer.toString(rating.getValue());
			flatAnswers.put(answerID, answerValue);
		}
		return flatAnswers;
	}
	
	
	public static final Parcelable.Creator<RatingListAnswers> CREATOR = new Parcelable.Creator<RatingListAnswers>()
	{
		public RatingListAnswers createFromParcel(Parcel in)
		{
			return new RatingListAnswers(in);
		}

		public RatingListAnswers[] newArray(int size)
		{
			return new RatingListAnswers[size];
		}
	};

	@Override
	public void writeToParcel(Parcel out, int flags)
	{
		super.writeToParcel(out, flags);
		out.writeInt(questions.length);
		for (RatingAnswer question : questions)
		{
			out.writeParcelable(question, flags);
		}
	}

	@Override
	public JSONObject toJSON() throws JSONException
	{
		JSONObject json = super.toJSON();
		JSONArray answerArray = new JSONArray();
		for (RatingAnswer rating : questions)
		{
			answerArray.put(rating.toJSON());
		}
		json.put(DynamicApplicationConstants.ANSWER, answerArray);
		return json;
	}

}
