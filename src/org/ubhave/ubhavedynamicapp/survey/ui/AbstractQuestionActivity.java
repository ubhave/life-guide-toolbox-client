/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey.ui;

import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.UbhaveActivity;
import org.ubhave.ubhavedynamicapp.survey.QuestionnaireController;
import org.ubhave.ubhavedynamicapp.survey.data.as.AbstractAnswer;
import org.ubhave.ubhavedynamicapp.survey.data.qs.AbstractQuestion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public abstract class AbstractQuestionActivity extends UbhaveActivity
{
	public final static String ANSWER_VALUE = "answer";
	public final static String F_TIME = "createTime", S_TIME = "resumeTime", E_TIME = "finishTime";

	protected String question_id;
	protected String survey_id;
	
	private long firstTime, startTime;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(getLayout());
		Intent intent = getIntent();	
		AbstractQuestion currentQuestion = intent.getParcelableExtra(QuestionnaireController.CURRENT_QUESTION);
		question_id = currentQuestion.getIdentifier();
		survey_id = intent.getStringExtra(QuestionnaireController.SURVEY_ID);	
		
		setActivityTitle(currentQuestion.getTitle());
		setActivityQuestion(currentQuestion.getText());
		setActivityFooterText(currentQuestion.getFooterText());
		firstTime = System.currentTimeMillis();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		startTime = System.currentTimeMillis();
	}
	
	protected abstract int getLayout();

	protected void setActivityQuestion(final String question)
	{
		((TextView) findViewById(R.id.questionText)).setText(question);
	}
	
	protected void setActivityFooterText(final String fText)
	{
		if(fText.equals(""))
			((TextView) findViewById(R.id.questionFooterText)).setVisibility(View.INVISIBLE);
		else
			((TextView) findViewById(R.id.questionFooterText)).setText(fText);
	}

	protected void submitAnswers(final AbstractAnswer answer)
	{
		long endTime = System.currentTimeMillis();
		answer.setTime(firstTime, startTime, endTime);

		Intent result = getIntent();
		result.putExtra(ANSWER_VALUE, answer);
		this.setResult(Activity.RESULT_OK, result);
		this.finish();
	}
	
	@Override
	protected String getActivityID() {
		return question_id;
	}

	@Override
	protected String getActivityExtra() {
		return survey_id;
	}
}
