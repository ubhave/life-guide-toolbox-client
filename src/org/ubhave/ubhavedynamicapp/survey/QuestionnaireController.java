/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey;

import org.json.JSONObject;
import org.ubhave.ubhavedynamicapp.AbstractComponentController;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveActivity;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.log.ApplicationDataLogger;
import org.ubhave.ubhavedynamicapp.survey.data.as.AbstractAnswer;
import org.ubhave.ubhavedynamicapp.survey.data.as.AnswerList;
import org.ubhave.ubhavedynamicapp.survey.data.qs.AbstractQuestion;
import org.ubhave.ubhavedynamicapp.survey.data.qs.QuestionList;
import org.ubhave.ubhavedynamicapp.survey.ui.AbstractQuestionActivity;
import org.ubhave.ubhavedynamicapp.survey.ui.FreeTextActivity;
import org.ubhave.ubhavedynamicapp.survey.ui.InstructionActivity;
import org.ubhave.ubhavedynamicapp.survey.ui.RatingListActivity;
import org.ubhave.ubhavedynamicapp.survey.ui.lists.MultiCategoryList;
import org.ubhave.ubhavedynamicapp.survey.ui.lists.SingleCategoryList;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class QuestionnaireController extends AbstractComponentController
{
	public final static String CURRENT_QUESTION = "question";
	public final static String SURVEY_ID = "survey_id";
	
	public final static String TAG = "QuestionnaireController";

	
	private final Activity context;
	private QuestionList questions;
	private AnswerList answers;
	private int currentQuestion;
	private String surveyID;
	
	private final JSONObject currentSurveyJSON;
	
	public QuestionnaireController(Activity c, JSONObject surveyJO, String id, DynamicApplicationController appCon)
	{
		this.context = c;
		surveyID = id;
		currentSurveyJSON = surveyJO;
		setAppController(appCon);
	}
	
	@Override
	public void start()
	{
		try
		{
			questions = (new QuestionListLoader()).loadSurvey(currentSurveyJSON, surveyID);
			answers = new AnswerList(questions.getSurveyIdentifier());
			currentQuestion = 0;
			popQuestion();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void popQuestion()
	{
		if (questions != null)
		{
			if (currentQuestion < questions.size())
			{
				AbstractQuestion question = questions.get(currentQuestion);
				int question_type = question.getType();
				Intent intent = null;
				if (question_type == AbstractQuestion.CATEGORICAL_SINGLE_CHOICE)
				{
					intent = new Intent(context, SingleCategoryList.class);
				}
				else if (question_type == AbstractQuestion.CATEGORICAL_MULTIPLE_CHOICE)
				{
					intent = new Intent(context, MultiCategoryList.class);
				}
				else if (question_type == AbstractQuestion.RATING_LIST)
				{
					intent = new Intent(context, RatingListActivity.class);
				}
				else if (question_type == AbstractQuestion.FREE_TEXT_QUESTION)
				{
					intent = new Intent(context, FreeTextActivity.class);
				}
				else if (question_type == AbstractQuestion.INSTRUCTION)
				{
					intent = new Intent(context, InstructionActivity.class);
				}

				if (intent != null)
				{
					intent.putExtra(CURRENT_QUESTION, question);
					intent.putExtra(SURVEY_ID, questions.getSurveyIdentifier());
					intent.putExtra(UbhaveActivity.SKIPABLE, isSkipable());
					context.startActivityForResult(intent, question_type);
				}
			}
			else
			{
				Toast.makeText(context, "Thank you!", Toast.LENGTH_LONG).show();
				ApplicationDataLogger logger = ApplicationDataLogger.getInstance();
				logger.logSurveyResponse(answers);
				
				getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE);
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.i(TAG, "oar "+resultCode);
		if (resultCode == Activity.RESULT_OK)
		{
			AbstractAnswer answer = data.getParcelableExtra(AbstractQuestionActivity.ANSWER_VALUE);
			answers.add(answer);
			currentQuestion++;
			popQuestion();
		}
		else if(resultCode ==DynamicApplicationController.RESULT_ACTIONBAR_HOME){
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_COMPLETE_AND_JUMP_TO_FRONT);
		}
		else if (currentQuestion > 0) // back pressed
		{
			currentQuestion--;
			answers.remove(answers.size() - 1);
			Log.i(TAG, "Back "+currentQuestion);
			popQuestion();
		}
		else{
			Log.i(TAG, "BACK "+currentQuestion);
			getAppController().activityFinished(DynamicApplicationConstants.ACTIVITY_FINISH_BACK);
		}
	}
}
