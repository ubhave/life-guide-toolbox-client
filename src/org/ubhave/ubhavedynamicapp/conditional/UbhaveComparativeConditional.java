/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.conditional;

import org.ubhave.ubhavedynamicapp.InterventionDataAccess;

import android.os.Parcel;
import android.os.Parcelable;

public class UbhaveComparativeConditional extends UbhaveConditional{

	String varAKey;
	String varBKey;
	String varAVal;
	String varBVal;
	
	final static String EQUALS = "=";
	final static String NOT_EQUALS = "!=";
	final static String GREATER_THAN = ">";
	final static String LESS_THAN = "<";
	final static String GREATER_THAN_EQUAL = ">=";
	final static String LESS_THAN_EQUAL = "<=";
	
	public UbhaveComparativeConditional(String op, String aKey, String bKey){
		varAKey = aKey;
		varBKey = bKey;
		operand=op;
		//loadValues();		
	}
	
	private void loadValues(){
		if(varAKey.length()>1&&varAKey.substring(0,2).equals("v-")){
			//varAVal = sharedPrefs.getString(varAKey.substring(2), "");
			varAVal = InterventionDataAccess.getVariable(varAKey.substring(2));
		}
		else if(varAKey.length()>1&&varAKey.substring(0,2).equals("q-")){
			String query = varAKey.substring(2, varAKey.indexOf('('));
			String param = varAKey.substring(varAKey.indexOf('(')+1, varAKey.indexOf(')'));
			
			/*if(query.equals("checkIfItemAccessed"))
				varAVal=new Boolean(InterventionDataAccess.checkIfItemAccessed(new Integer(param))).toString();
			if(query.equals("checkIfItemAccessedToday"))
				varAVal=new Boolean(InterventionDataAccess.checkIfItemAccessedToday(new Integer(param))).toString();
			if(query.equals("itemAccessedInPeriodCount"))
				varAVal=new Boolean(InterventionDataAccess.itemAccessedInPeriodCount(new Integer(param.substring(0, param.indexOf(","))),new Integer(param.substring(param.indexOf(",")+1)))).toString();
			if(query.equals("varLessThanHoursOld"))
				varAVal=new Boolean(InterventionDataAccess.variableLessThanHoursOld(new String(param.substring(0, param.indexOf(","))),new Integer(param.substring(param.indexOf(",")+1)))).toString();*/
			
			varAVal=UbhaveConditionalQuery.query(query, param);
		}
		else
			varAVal=varAKey;
		
		if(varBKey.length()>1&&varBKey.substring(0,2).equals("v-")){
			//varBVal = sharedPrefs.getString(varBKey.substring(2), "");
			varBVal = InterventionDataAccess.getVariable(varBKey.substring(2));
		}
		else if(varBKey.length()>1&&varBKey.substring(0,2).equals("q-")){
			String query = varBKey.substring(2, varBKey.indexOf('('));
			String param = varBKey.substring(varBKey.indexOf('(')+1, varBKey.indexOf(')'));
			
			/*if(query.equals("checkIfItemAccessed"))
				varBVal=new Boolean(InterventionDataAccess.checkIfItemAccessed(new Integer(param))).toString();
			if(query.equals("checkIfItemAccessedToday"))
				varBVal=new Boolean(InterventionDataAccess.checkIfItemAccessedToday(new Integer(param))).toString();
			if(query.equals("itemAccessedInPeriodCount"))
				varBVal=new Boolean(InterventionDataAccess.itemAccessedInPeriodCount(new Integer(param.substring(0, param.indexOf(","))),new Integer(param.substring(param.indexOf(",")+1)))).toString();
			if(query.equals("varLessThanHoursOld"))
				varBVal=new Boolean(InterventionDataAccess.variableLessThanHoursOld(new String(param.substring(0, param.indexOf(","))),new Integer(param.substring(param.indexOf(",")+1)))).toString();*/
		
			varBVal=UbhaveConditionalQuery.query(query, param);
		
		}
		else
			varBVal=varBKey;
				
	}
	
	@Override
	public boolean evaluate() {
		loadValues();
		if(operand.equals(EQUALS)){
			return varAVal.equals(varBVal);
		}
		else if(operand.equals(NOT_EQUALS)){
			return !varAVal.equals(varBVal);
		}
		else if (operand.equals(GREATER_THAN)){
			try{
				return new Double(varAVal)>new Double(varBVal);
			}
			catch(NumberFormatException e){
				e.printStackTrace();
				return false;
			}
		}
		else if (operand.equals(LESS_THAN)){
			try{
				return new Double(varAVal)<new Double(varBVal);
			}
			catch(NumberFormatException e){
				e.printStackTrace();
				return false;
			}
		}
		else if (operand.equals(GREATER_THAN_EQUAL)){
			try{
				return new Double(varAVal)>=new Double(varBVal);
			}
			catch(NumberFormatException e){
				e.printStackTrace();
				return false;
			}
		}
		else if (operand.equals(LESS_THAN_EQUAL)){
			try{	
				return new Double(varAVal)<=new Double(varBVal);
			}
			catch(NumberFormatException e){
				e.printStackTrace();
				return false;
			}
		}
		
		return false;
		
	}

	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(type);
		dest.writeString(operand);
		dest.writeString(varAKey);
		dest.writeString(varBKey);
		dest.writeString(varAVal);
		dest.writeString(varBVal);
	}
	
	public UbhaveComparativeConditional(Parcel in) {
		type = in.readInt();
		operand = in.readString();
		varAKey = in.readString();
		varBKey = in.readString();
		varAVal = in.readString();
		varBVal = in.readString();
	}
	
	public static final Parcelable.Creator<UbhaveComparativeConditional> CREATOR = new Parcelable.Creator<UbhaveComparativeConditional>()
			{
				public UbhaveComparativeConditional createFromParcel(Parcel in)
				{
					return new UbhaveComparativeConditional(in);
				}

				public UbhaveComparativeConditional[] newArray(int size)
				{
					return new UbhaveComparativeConditional[size];
				}
			};
	
	
}