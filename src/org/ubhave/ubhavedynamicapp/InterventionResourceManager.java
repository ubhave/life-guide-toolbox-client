/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.content.Context;
import android.net.Uri;

public class InterventionResourceManager{
	
	
	public static void initialiseCurrentInterventionResources() throws FileNotFoundException, IOException{
			
		System.out.println("Initialising resources from "+ DynamicApplicationConstants.CURRENT_INTERVENTION_RES);
		storeResourceZipContents(UbhaveApplication.getContext().openFileInput(DynamicApplicationConstants.CURRENT_INTERVENTION_RES));
		
	}
	
	public static void storeResourceZipContents(InputStream zipFile) throws IOException{
		System.out.println("Storing resources ...");
		ZipInputStream zis =new ZipInputStream(zipFile);
		
		ZipEntry ze = zis.getNextEntry();
		
		while(ze!=null){
			String fileName = ze.getName();
						
			File newFile = new File(fileName);
			
			String type = newFile.getParentFile().getName();
						
			if(type.equals("sound")){
				storeSoundResource(zis, newFile.getName());
			}
			else if(type.equals("image")){
				storeImageResource(zis, newFile.getName());
			}
			else if(type.equals("video")){
				storeVideoResource(zis, newFile.getName());
			}
			
			ze = zis.getNextEntry();
		}
		System.out.println("... resources stored");		
		
	}	
	
	
	
	public static void storeResource(InputStream fis, String type, String id) throws IOException{
		
		System.out.println("Storing "+type+" "+id+"...");
		
		FileOutputStream fos = UbhaveApplication.getContext().openFileOutput(InterventionDataAccess.getCurrentInterventionName()+"-"+type+"-"+id, Context.MODE_PRIVATE);
				
		byte[] buffer = new byte[1024];
	    int read;
	    while((read = fis.read(buffer)) != -1){
	    	fos.write(buffer, 0, read);
	    }
	    	    
	    fos.close();
	    System.out.println("..."+InterventionDataAccess.getCurrentInterventionName()+"-"+type+"-"+id+" stored!");
	}
	
	public static void storeSoundResource(InputStream fis, String id) throws IOException{
		storeResource(fis,"sound",id);
	}
	public static void storeImageResource(InputStream fis, String id) throws IOException{
		storeResource(fis,"image",id);
	}
	public static void storeVideoResource(InputStream fis, String id) throws IOException{
		storeResource(fis,"video",id);
	}
	
	
	public static FileDescriptor getSoundResource(String id) throws IOException{
		FileInputStream fis = UbhaveApplication.getContext().openFileInput(InterventionDataAccess.getCurrentInterventionName()+"-sound-"+id);
		FileDescriptor des = fis.getFD();
		fis.close();
		return des;
	}
		
	public static Uri getSoundResourceURI(String id){
		Uri uri=Uri.parse(UbhaveApplication.getContext().getFilesDir() + "/" +InterventionDataAccess.getCurrentInterventionName()+"-sound-"+id);
		return uri;		
	}
	
	public static Uri getImageResourceURI(String id){
		Uri uri=Uri.parse("file://"+UbhaveApplication.getContext().getFilesDir() + "/" +InterventionDataAccess.getCurrentInterventionName()+"-image-"+id);
		return uri;		
	}
	
	public static Uri getVideoResourceURI(String id){
		Uri uri=Uri.parse(UbhaveApplication.getContext().getFilesDir() + "/" +InterventionDataAccess.getCurrentInterventionName()+"-video-"+id);
		return uri;		
	}
	
}