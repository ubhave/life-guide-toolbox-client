/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.background.trigger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.ubhave.intelligenttrigger.IntelligentTriggerManager;
import org.ubhave.intelligenttrigger.IntelligentTriggerReceiver;
import org.ubhave.intelligenttrigger.learners.InterruptibilityLearner;
import org.ubhave.intelligenttrigger.learners.Learner;
import org.ubhave.intelligenttrigger.learners.LearnerResultBundle;
import org.ubhave.intelligenttrigger.triggers.SensorTrigger;
import org.ubhave.intelligenttrigger.triggers.TimeTrigger;
import org.ubhave.intelligenttrigger.triggers.Trigger;
import org.ubhave.intelligenttrigger.triggers.config.BasicTriggerConfig;
import org.ubhave.intelligenttrigger.triggers.definitions.SensorTriggerDefinition;
import org.ubhave.intelligenttrigger.triggers.definitions.TimeTriggerDefinition;
import org.ubhave.intelligenttrigger.utils.Constants;
import org.ubhave.intelligenttrigger.utils.ITException;
import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.InterventionDataAccess;
import org.ubhave.ubhavedynamicapp.InterventionResourceManager;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;
import org.ubhave.ubhavedynamicapp.app.data.DynamicApplication;
import org.ubhave.ubhavedynamicapp.app.ui.MenuActivity;
import org.ubhave.ubhavedynamicapp.background.sense.SenseAndSave;
import org.ubhave.ubhavedynamicapp.conditional.UbhaveConditional;
import org.ubhave.ubhavedynamicapp.log.ApplicationDataLogger;

//import android.R;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.SparseArray;

import com.ubhave.mltoolkit.utils.MLException;
import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.ESSensorManager;
import com.ubhave.sensormanager.config.GlobalConfig;
import com.ubhave.sensormanager.config.sensors.pull.LocationConfig;
import com.ubhave.sensormanager.config.sensors.pull.PullSensorConfig;
import com.ubhave.sensormanager.sensors.SensorUtils;
import com.ubhave.triggermanager.config.TriggerConfig;

/**
 * LIMITATION: When changing the interval of times to interrupt, 
 * the app takes into account the last active interval (from the current trigger)
 * and the new interval defined by the user. The original interval is not taken
 * into account, unless the app is not restarted.  
 * 
 */
public class IntelligentTriggerController implements
							IntelligentTriggerReceiver, 
							OnSharedPreferenceChangeListener {

	private static final String TAG = "IntelligentTriggerController";

	private Context d_context;

	private IntelligentTriggerManager d_manager;

	private HashMap<String, ArrayList<TriggerAction>> d_actionMap;
	
	private HashMap<String, SparseArray<String>> d_variableMap;
	
	public IntelligentTriggerController(Context a_c) {

		d_context = a_c;
		d_actionMap = new HashMap<String, ArrayList<TriggerAction>>();
		d_variableMap = new HashMap<String, SparseArray<String>>();
		
		try {
			d_manager = IntelligentTriggerManager.getTriggerManager(d_context);
		
			SharedPreferences settings = PreferenceManager
					.getDefaultSharedPreferences(d_context);
			settings.registerOnSharedPreferenceChangeListener(this);

			ESSensorManager sensorManager;
			sensorManager = ESSensorManager.getSensorManager(d_context);
			sensorManager.setGlobalConfig(GlobalConfig.ACQUIRE_WAKE_LOCK, true);

			sensorManager.setSensorConfig(
					SensorUtils.SENSOR_TYPE_ACCELEROMETER,
					PullSensorConfig.SENSE_WINDOW_LENGTH_MILLIS,
					DynamicApplicationConstants.ACCELEROMETER_SENSING_WINDOW_LONG);

			sensorManager
					.setSensorConfig(
							SensorUtils.SENSOR_TYPE_ACCELEROMETER,
							PullSensorConfig.POST_SENSE_SLEEP_LENGTH_MILLIS,
							DynamicApplicationConstants.ACCELEROMETER_SLEEP_INTERVAL);

			sensorManager.setSensorConfig(SensorUtils.SENSOR_TYPE_LOCATION,
					PullSensorConfig.POST_SENSE_SLEEP_LENGTH_MILLIS,
					DynamicApplicationConstants.LOCATION_SLEEP_INTERVAL);

			sensorManager.setSensorConfig(SensorUtils.SENSOR_TYPE_LOCATION,
					LocationConfig.ACCURACY_TYPE,
					LocationConfig.LOCATION_ACCURACY_FINE);
		} catch (ESException e) {
			e.printStackTrace();
		} catch (ITException e) {
			e.printStackTrace();
		}

		// TODO: WHY?
		// Interruptibility learner has to exist
		// And is giving random notifs if it's not sufficiently trained
		try {
			Learner interLearner = d_manager
					.getLearner(org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY);
			if (interLearner.instanceQueueSize() < DynamicApplicationConstants.MIN_INTERRUPTIBILITY_POINTS){
				interLearner.randomTriggerSwitch(true, DynamicApplicationConstants.RANDOM_TRIGGER_P);
			} else {
				if (interLearner.isTrained()) {
					interLearner.randomTriggerSwitch(false, DynamicApplicationConstants.RANDOM_TRIGGER_P);
				}
			}
		} catch (ITException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Checks if triggers are all instantiated, and instantiates them if needed.
	 */
	public void checkTriggers() {

		Log.d(TAG, "checkTriggers");
		
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(d_context);
		
		try {
			ArrayList<String> triggers = IntelligentTriggerLoader
					.listAllTriggers();

			for (String triggerID : triggers) {

				// Check if a condition is satisfied and ensure that
				// the right trigger is instantiated.
				TriggerDefinitionBundle triggerDef = IntelligentTriggerLoader
						.loadTrigger(triggerID);
				
				UbhaveConditional triggerCondition = triggerDef.getCondition();
				
				Log.d(TAG, "check trigger "+triggerID);
				
				if (triggerCondition == null || triggerCondition.evaluate()) {
					// This trigger should run
				
					Log.d(TAG, "condition satisfied");
					
					if (d_manager.triggerExists(triggerID)) {
						
						Log.d(TAG, "trigger exists");

						// Unpause a trigger if the user says that there should be notifs 
						if (!d_manager.isTriggerRunning(triggerID)) {
							
							if (settings.contains(DynamicApplicationConstants.TRIGGER_SWITCH)) {
								
								boolean triggerOn = settings.getBoolean(DynamicApplicationConstants.TRIGGER_SWITCH, true);
								
								if (triggerOn) {
									Log.d(TAG, "trigger unpause");
									d_manager.unpauseIntelligentTrigger(triggerID);
								} 
							} else {
								Log.d(TAG, "trigger unpause");	
								d_manager.unpauseIntelligentTrigger(triggerID);
							}
						}
						
						// NOTE: if it is an intelligent trigger and 
						// it didn't fire for a long time,
						// switch to random until it fires						
						if (triggerDef.getDefinition().getTriggerType() == Constants.SENSOR_TRIGGER) {
							
							Log.d(TAG, "sensor trigger");

							SensorTriggerDefinition sensTriggerDef = (SensorTriggerDefinition) triggerDef.getDefinition();
							
							if (sensTriggerDef.usesModality(org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY)) {
								long lastTriggerTime = settings.getLong(DynamicApplicationConstants.LAST_TRIGGER_TIME, -1);
								long currentTime = System.currentTimeMillis();
								if ((lastTriggerTime > -1) &&
									(currentTime - lastTriggerTime > DynamicApplicationConstants.MAX_NO_TRIGGER_TIME)) {
									Learner interLearner = d_manager
										.getLearner(org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY);
									interLearner.randomTriggerSwitch(true, DynamicApplicationConstants.RANDOM_TRIGGER_P);
								}
							}
						}
					} else {
						Log.d(TAG, "instantiate trigger");

						instantiateTrigger(triggerID, triggerDef);
						
						// In case trigger manager was killed, settings should be reset in the lib.
						ArrayList<String> triggerConfigKeys = org.ubhave.intelligenttrigger.utils.Constants
								.allConfigParams();
						
						String beforeString = org.ubhave.intelligenttrigger.utils.Constants.DO_NOT_DISTURB_BEFORE_MINUTES;
						String afterString = org.ubhave.intelligenttrigger.utils.Constants.DO_NOT_DISTURB_AFTER_MINUTES;
						
						BasicTriggerConfig newConfig = new BasicTriggerConfig(false);
												
						for (String key : triggerConfigKeys) {
							
							if (key.equals(beforeString) || key.equals(afterString)){
								
								int beforeDef = 0;
								int afterDef = 23*60+59;
								
								int beforeUser = settings.getInt(beforeString, 0);
								int afterUser = settings.getInt(afterString, 23*60+59);
								
								if (triggerDef.getDefinition().getTriggerType() == Constants.TIME_TRIGGER) {							
									TimeTriggerDefinition definition = (TimeTriggerDefinition)triggerDef.getDefinition();
									TriggerConfig config = definition.getSubconfig();			
									if (config.containsKey(afterString)){
										afterDef = (Integer) config.getParameter(afterString);
									}
									if (config.containsKey(beforeString)){							
										beforeDef = (Integer) config.getParameter(beforeString);
										Log.d(TAG, " "+beforeDef);
									}						
								} else if (triggerDef.getDefinition().getTriggerType() == Constants.SENSOR_TRIGGER) {
									SensorTriggerDefinition definition = (SensorTriggerDefinition)triggerDef.getDefinition();
									BasicTriggerConfig config = definition.getSubconfig();
									if (config.containsParam(afterString)){
										afterDef = (Integer) config.getParam(afterString);
									}
									if (config.containsParam(beforeString)){
										beforeDef = (Integer) config.getParam(beforeString);
									}	
								}
								
								int minAfter = Math.min(afterUser, afterDef);
								int maxBefore = Math.max(beforeUser,beforeDef);
								
								if (minAfter <= maxBefore) { 
									// no overlap, use user-defined:
									minAfter = afterUser;
									maxBefore = beforeUser;
								}
								
								Log.d(TAG, "New interval: "+maxBefore+" to "+minAfter);
								newConfig.addParam(beforeString, maxBefore);
								newConfig.addParam(afterString, minAfter);
								
							}
							else {
								if (settings.contains(key)) {
									int value = settings.getInt(key,0);
									newConfig.addParam(key, value);
								}
							}
						}
						
						try {
							d_manager.changeConfig(triggerID, newConfig);
						} catch (ITException e) {
							e.printStackTrace();
						}
						
						// In case trigger manager was killed, trigger on/off state should be reset in the lib.
						if (settings.contains(DynamicApplicationConstants.TRIGGER_SWITCH)) {
							
							boolean triggerOn = settings.getBoolean(DynamicApplicationConstants.TRIGGER_SWITCH, true);
							
							if (triggerOn) {
								d_manager.unpauseAllTriggers();
							} else {
								Log.d(TAG, "But pause triggers");
								d_manager.pauseAllTriggers();
							}
						}
					}
				} else {
					// Condition not satisfied -- remove the trigger if instantiated 
					
					Log.d(TAG, "condition not satisfied");

					if (d_manager.triggerExists(triggerID)) {
						
						Log.d(TAG, "remove trigger");

						d_manager.removeIntelligentTrigger(triggerID);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Calendar c = Calendar.getInstance();
		int currentWeek = c.get(Calendar.WEEK_OF_YEAR);
		int lastTransmittedWeek = settings.getInt(DynamicApplicationConstants.LAST_CLASSIFIER_XMIT_WEEK, -1);
		if (lastTransmittedWeek != currentWeek) {
			ApplicationDataLogger.getInstance().logLearner(d_manager.getJSON(), currentWeek);
			SharedPreferences.Editor editor = settings.edit();
			editor.putInt(DynamicApplicationConstants.LAST_CLASSIFIER_XMIT_WEEK, currentWeek);
			editor.commit();
		}

	}

	public void instantiateTrigger(String a_id, TriggerDefinitionBundle a_triggerDef) {

		try {
			Log.d(TAG, "Adding trigger type "
					+ a_triggerDef.getDefinition().getTriggerType());

			d_manager.addIntelligentTrigger(a_id,
					a_triggerDef.getDefinition(), this);

			d_actionMap.put(a_id, a_triggerDef.getActions());
			
			d_variableMap.put(a_id, a_triggerDef.getVariables());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
/*	public void instantiateTrigger(String a_name) {

		try {

			TriggerDefinitionBundle triggerDef = IntelligentTriggerLoader
					.loadTrigger(a_name);

			Log.d(TAG, "Adding trigger type "
					+ triggerDef.getDefinition().getTriggerType());

			d_manager.addIntelligentTrigger(a_name,
					triggerDef.getDefinition(), this);

			d_actionMap.put(a_name, triggerDef.getActions());
			
			d_variableMap.put(a_name, triggerDef.getVariables());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
*/
	@Override
	public void onTriggerReceived(final String a_triggerID,
			ArrayList<LearnerResultBundle> a_bundles) {

		// TODO: How do we ensure that d_actionMap is not null here?

		Log.d(TAG, "onTriggerReceived");
		
		ArrayList<TriggerAction> actions = d_actionMap.get(a_triggerID);

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(d_context);
		SharedPreferences.Editor editor = settings.edit();
		
		int currentTriggerNo = settings.getInt(
				DynamicApplicationConstants.CURRENT_TRIGGER_NO, 0);
		
		editor.putInt(DynamicApplicationConstants.CURRENT_TRIGGER_NO,
				++currentTriggerNo);
		
		editor.commit();
		
		boolean trained = false;
		boolean random = false;
		

		// Multiple actions can be connected with the same trigger.
		// Each notif action should also carry title/info fields.
		// At runtime we select one of the actions that satisfy their
		// respective conditions. 
		
		ArrayList<TriggerAction> candidateActions = new ArrayList<TriggerAction>();
		
		int actionCnt = 0;
		
		while (actionCnt < actions.size()) {
			
			TriggerAction action = actions.get(actionCnt);
			
			UbhaveConditional condition = action.getCondition();
			
			if (condition == null || condition.evaluate()) {
				candidateActions.add(action);
			}
			
			actionCnt++;
		}
		
		String selectedActionID = "";
		
		if (candidateActions.size() > 0) {
			
			
			int selectedActionIndex = 0;
			
			// NOTE: uncomment if you want random action selection.
			// Random randomGen = new Random();
			// selectedActionIndex = randomGen.nextInt(candidateActions.size())
			
			TriggerAction selectedAction = candidateActions.get(selectedActionIndex);
			
			selectedActionID = selectedAction.getID();

			if (selectedAction.getType() == DynamicApplicationConstants.ACTION_NOTIFICATION) {
				
				// Train last trigger if it's an interruptibility trigger
				int lastTriggerType = settings.getInt(
						DynamicApplicationConstants.LAST_TRIGGER_TYPE, -1);
				
				if (lastTriggerType == 
						org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY) {
					int currentInterTriggerNo = settings.getInt(
							DynamicApplicationConstants.CURRENT_INTER_TRIGGER_NO, 0);
					boolean trainedInterTriggerInstance = settings.getBoolean(
							DynamicApplicationConstants.TRAINED_INTER_TRIGGER_INSTANCE, false);
					if ((trainedInterTriggerInstance == false) && (currentInterTriggerNo>0)) {
						Log.d(TAG, "train on unanswered instance");
						trainInterruptibilityLearner(InterruptibilityLearner.INTERRUPTIBLE_NO);	
					}
				}
				
				int currentTriggerType = -1;
				
				if (a_bundles != null) {
					LearnerResultBundle firstInstance = a_bundles.get(0);
					Log.d(TAG, "onTriggerReceived " + a_triggerID+
							" instance ID "+firstInstance.getInstanceID());
					currentTriggerType = firstInstance.getLearnerID(); 
					int currentNotifTriggerNo = firstInstance.getInstanceID();
					trained = firstInstance.getIsTrained();
					random = firstInstance.getIsRandom();

					if (currentTriggerType == 
						org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY) {
						editor.putInt(DynamicApplicationConstants.CURRENT_INTER_TRIGGER_NO,
							currentNotifTriggerNo);
						editor.putBoolean(DynamicApplicationConstants.TRAINED_INTER_TRIGGER_INSTANCE, false);
						editor.commit();
						try {
							Learner interLearner = d_manager
								.getLearner(org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY);
							if (interLearner.instanceQueueSize() < DynamicApplicationConstants.MIN_INTERRUPTIBILITY_POINTS){
								interLearner.randomTriggerSwitch(true, DynamicApplicationConstants.RANDOM_TRIGGER_P);
							} else {
								if (interLearner.isTrained()) {
									interLearner.randomTriggerSwitch(false, DynamicApplicationConstants.RANDOM_TRIGGER_P);
								}
							}
						} catch (ITException e) {
							e.printStackTrace();
						}
					}
					editor.putInt(DynamicApplicationConstants.LAST_TRIGGER_TYPE, currentTriggerType);
					editor.putLong(DynamicApplicationConstants.LAST_TRIGGER_TIME, System.currentTimeMillis());
					editor.commit();
				
				}
				
				NotificationTriggerAction naction = (NotificationTriggerAction) selectedAction;

				UbhaveApplication ua = (UbhaveApplication) UbhaveApplication
						.getContext();

				NotificationManager mNotificationManager = (NotificationManager) ua
						.getSystemService(Context.NOTIFICATION_SERVICE);
				Intent toAppController = new Intent(ua, MenuActivity.class);
				toAppController.putExtra(
						DynamicApplicationConstants.CONTROL_INSTRUCTION,
						DynamicApplicationConstants.PROCESS_NOTIFICATION);
				toAppController.putExtra(
						DynamicApplicationConstants.NOTIFICATION_EXTRA, naction);
				toAppController.putExtra(
						DynamicApplicationConstants.TRIGGER_TYPE, currentTriggerType);
				toAppController.putExtra(
						DynamicApplicationConstants.TRIGGER_NAME, a_triggerID);				
				toAppController.putExtra(
						DynamicApplicationConstants.CURRENT_TRIGGER_NO, currentTriggerNo);
				toAppController.putExtra(
						DynamicApplicationConstants.TRIGGER_TIME, System.currentTimeMillis());					
				toAppController.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				
				// TODO: check if we should FLAG_UPDATE_CURRENT or replace it or
				// something else
				PendingIntent contentIntent = PendingIntent.getActivity(ua, 0,
						toAppController, PendingIntent.FLAG_CANCEL_CURRENT);

				DynamicApplication config = UbhaveApplication.getConfig();
				Bitmap iconBitmap = null;
				
				if (config != null) {
					Uri iconURI = InterventionResourceManager.getImageResourceURI(config.getNotifIcon());
					try {
						iconBitmap = MediaStore.Images.Media.getBitmap(
								UbhaveApplication.getContext().getContentResolver(), 
								iconURI);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				Notification notification = new NotificationCompat.Builder(ua)
						.setContentTitle(naction.getNotificationTitle())
						.setContentText(naction.getNotificationText())
						.setLargeIcon(iconBitmap)
						.setSmallIcon(android.R.drawable.ic_dialog_alert)
						.setWhen(System.currentTimeMillis())
						.setContentIntent(contentIntent).build();
				notification.defaults |= Notification.DEFAULT_SOUND;
				notification.defaults |= Notification.DEFAULT_LIGHTS;
				mNotificationManager
						.notify(DynamicApplicationConstants.NOTIFICATION_CODE,
								notification);				
			}
			else if (selectedAction.getType() == DynamicApplicationConstants.ACTION_SENSING) {

				SensingTriggerAction saction = (SensingTriggerAction) selectedAction;				
				
				ArrayList<Integer> sensorList = saction.getSensorList();
				if (sensorList.size() > 0) {

					for (int sID : sensorList) {

						HandlerThread uiThread = new HandlerThread("UIHandler");
						uiThread.start();
						Handler mHandler = new Handler(uiThread.getLooper()) {
							@SuppressLint("NewApi")
							@Override
							public void handleMessage(Message msg) {
								try {
									if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
										new SenseAndSave().executeOnExecutor(
												AsyncTask.THREAD_POOL_EXECUTOR,
												a_triggerID, msg.arg1);
									} else {
										new SenseAndSave().execute(a_triggerID,
												msg.arg1);
									}
								} catch (ESException e) {
									e.printStackTrace();
								}
							}
						};
						Message msg = new Message();
						msg.arg1 = sID;
						mHandler.sendMessageDelayed(msg, 0);
					}

				}			
			}		
		} // if candidate action > 0
		
		recordNotification(currentTriggerNo, a_triggerID, trained, random, selectedActionID);
	
	}

	public void trainInterruptibilityLearner(Object a_value){
		
		try {
			SharedPreferences settings = PreferenceManager
					.getDefaultSharedPreferences(d_context);
			SharedPreferences.Editor editor = settings.edit();
			int currentTriggerNo = settings.getInt(
					DynamicApplicationConstants.CURRENT_INTER_TRIGGER_NO, 0);
			
			Log.d(TAG, "Train interruptibility learner with instance "+currentTriggerNo+" and value "+a_value);
			
			d_manager.trainLearnerFromFeedback(
					org.ubhave.intelligenttrigger.utils.Constants.MOD_INTERRUPTIBILITY,
					currentTriggerNo,
					a_value);
			editor.putBoolean(DynamicApplicationConstants.TRAINED_INTER_TRIGGER_INSTANCE, true);
			editor.commit();
		} catch (ITException e) {
			e.printStackTrace();
		} catch (MLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void recordNotification(int a_triggerNumber, 
									String a_triggerName, 
									boolean a_trained,
									boolean a_random,
									String a_triggerActionName) {

		long milliTime = System.currentTimeMillis();
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliTime);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);

		UbhaveNotification notif = new UbhaveNotification(
				a_triggerNumber, a_triggerName, a_triggerActionName, 
				a_trained, a_random,
				dayOfWeek, hourOfDay, milliTime);

		ApplicationDataLogger dl = ApplicationDataLogger.getInstance();
		dl.logNotification(notif);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		
		// For user-dependent triggers:
		for (Map.Entry<String, SparseArray<String>> entry : d_variableMap.entrySet()) {
			String triggerID = entry.getKey();
			SparseArray<String> variables = entry.getValue();
			
			if (variables != null) {
				int mod = variables.indexOfValue(key);
				if (mod >= 0) {
					// TODO: we don't know the value type!
					String newValue = sharedPreferences.getString(key, "");
					if (!newValue.equals("")) {
						try {
							d_manager.changeConditionValue(triggerID, mod, newValue);
						} catch (ITException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		// For switch params:
		ArrayList<String> triggerSwitchKeys = DynamicApplicationConstants.allTriggerParams();
		if (triggerSwitchKeys.contains(key)) {
			
			SharedPreferences settings = PreferenceManager
					.getDefaultSharedPreferences(d_context);
			
			if (key.equals(DynamicApplicationConstants.TRIGGER_SWITCH)) {
				
				boolean triggerOn = settings.getBoolean(DynamicApplicationConstants.TRIGGER_SWITCH, true);
				
				if (triggerOn) {
					d_manager.unpauseAllTriggers();
				} else {
					d_manager.pauseAllTriggers();
				}
				
				Log.d(TAG, "Pref changed "+key+" to "+triggerOn);
				InterventionDataAccess.storeCustomVariable(key, Boolean.toString(triggerOn));
			}
			
		}
		
		// For config params:
		ArrayList<String> triggerConfigKeys = org.ubhave.intelligenttrigger.utils.Constants
				.allConfigParams();

		if (triggerConfigKeys.contains(key)) {			

			String beforeString = org.ubhave.intelligenttrigger.utils.Constants.DO_NOT_DISTURB_BEFORE_MINUTES;
			String afterString = org.ubhave.intelligenttrigger.utils.Constants.DO_NOT_DISTURB_AFTER_MINUTES;
			
			SharedPreferences settings = PreferenceManager
					.getDefaultSharedPreferences(d_context);

			Log.d(TAG, "Pref changed "+key);
			InterventionDataAccess.storeCustomVariable(key, Integer.toString(settings.getInt(key,-1)));

			Set<String> IDs = d_actionMap.keySet();
			
			for (String ID : IDs) {
				
				BasicTriggerConfig newConfig = new BasicTriggerConfig(false);
	
				if (key.equals(beforeString) || key.equals(afterString)){

					// If Uinterval overlaps with Dinterval: use the overlap 
					// If Uinterval outside Dinteval: use Uinterval

					int beforeUser = settings.getInt(beforeString, 0);
					int afterUser = settings.getInt(afterString, 23*60+59);
					
					int beforeDef = 0;
					int afterDef = 23*60+59;
				
					Trigger trigger = d_manager.getTrigger(ID);
					
					if (trigger.triggerType() == Constants.TIME_TRIGGER) {
						TimeTriggerDefinition definition = ((TimeTrigger)trigger).getDefinition();
						TriggerConfig config = definition.getSubconfig();			
						Log.d(TAG, "Subconfig value for after... ");
						if (config.containsKey(afterString)){
							afterDef = (Integer) config.getParameter(afterString);
							Log.d(TAG, " "+afterDef);
						}
						Log.d(TAG, "Subconfig value for before... ");
						if (config.containsKey(beforeString)){							
							beforeDef = (Integer) config.getParameter(beforeString);
							Log.d(TAG, " "+beforeDef);
						}						
					} else if (trigger.triggerType() == Constants.SENSOR_TRIGGER) {
						SensorTriggerDefinition definition = ((SensorTrigger)trigger).getDefinition();
						BasicTriggerConfig config = definition.getSubconfig();
						if (config.containsParam(afterString)){
							afterDef = (Integer) config.getParam(afterString);
						}
						if (config.containsParam(beforeString)){
							beforeDef = (Integer) config.getParam(beforeString);
						}	
					}
					
					int minAfter = Math.min(afterUser, afterDef);
					int maxBefore = Math.max(beforeUser,beforeDef);
					
					if (minAfter <= maxBefore) { 
						// no overlap, use user-defined:
						minAfter = afterUser;
						maxBefore = beforeUser;
					}
					
					Log.d(TAG, "New interval: "+maxBefore+" to "+minAfter);
					newConfig.addParam(beforeString, maxBefore);
					newConfig.addParam(afterString, minAfter);
					
				} else {
					if (settings.contains(key)) {
						int value = settings.getInt(key,0);
						newConfig.addParam(key, value);
					}
				}
				
				try {
					d_manager.changeConfig(ID, newConfig);
				} catch (ITException e) {
					e.printStackTrace();
				}
			}		
		}
	}

	public void saveState() {
		d_manager.saveToPersistent();
	}

}
