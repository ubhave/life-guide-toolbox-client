/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner.ui;

import java.util.ArrayList;
import java.util.HashMap;

import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.UbhaveActivity;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.planner.PlannerController;
import org.ubhave.ubhavedynamicapp.planner.data.CheckListPlanner;
import org.ubhave.ubhavedynamicapp.planner.data.Plan;
import org.ubhave.ubhavedynamicapp.planner.data.PlanSet;
import org.ubhave.ubhavedynamicapp.planner.data.SingleFieldPlan;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CheckListPlannerActivity extends UbhaveActivity{

	private CheckListPlanner planner;
	HashMap<String, CheckBox> entries;
	
	PlanSet planSet;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.planner);
		planner = (CheckListPlanner) getIntent().getParcelableExtra(PlannerController.PLANNER);
		planSet = (PlanSet) getIntent().getParcelableExtra(PlannerController.PLAN_SET);
		
		ArrayList<String> plans = new ArrayList<String>();
		
		for(String key: planSet.getPlans().keySet()){
			plans.add(((SingleFieldPlan)planSet.getPlans().get(key)).getValue());
		}
		
		entries = new HashMap<String, CheckBox>();
		
		setActivityTitle(planner.getTitle());
		TextView textView = (TextView) findViewById(R.id.plannerText);
		textView.setText(planner.getHeaderText());
		
		addOptions(planner.getOptions(),plans);
		
		Button addButton = (Button)findViewById(R.id.addPlanButton);
		addButton.setVisibility(View.GONE);
		
		
	}
	
	private void addOptions(ArrayList<String> options, ArrayList<String> plans) {
		LinearLayout plansListView = (LinearLayout) findViewById(R.id.plannerLayout);
				
		for(String option:options){
			CheckBox checkBox;
			RelativeLayout fieldView = (RelativeLayout) View.inflate(this, R.layout.checklist_plan, null);
						
			checkBox = (CheckBox)fieldView.findViewById(R.id.planBox);
			checkBox.setText(option);
			if(plans.contains(option))
				checkBox.setChecked(true);
			
			plansListView.addView(fieldView);
			
			entries.put(""+entries.keySet().size(), checkBox);
		}		
	}

	@Override
	protected String getActivityType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getActivityID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getActivityExtra() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void submit(View v){
		
		Intent result = getIntent();		
		HashMap<String, Plan> plans = new HashMap<String, Plan>();
		
		for (String key:entries.keySet()){
			if(entries.get(key).isChecked()){
				plans.put(key, new SingleFieldPlan(key, entries.get(key).getText().toString()));
			}
		}
				
		PlanSet set = new PlanSet(planner.getKey());
		set.setPlans(plans);
		
		result.putExtra(PlannerController.PLAN_SET, set);		
		this.setResult(DynamicApplicationController.RESULT_PLANNER_OK, result);
		
		this.finish();
		
	}
	
}