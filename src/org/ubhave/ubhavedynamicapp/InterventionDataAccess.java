/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp;

import org.ubhave.ubhavedynamicapp.log.ApplicationDataLogger;
import org.ubhave.ubhavedynamicapp.planner.data.PlanSet;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class InterventionDataAccess{
	
	
	private static final String CURRENT_INTERVENTION_NAME = "current_intervention_name";
	private static final String CURRENT_INTERVENTION_THEME = "current_intervention_theme";

	public static void storeCustomVariable(String key, String value){
		ApplicationDataLogger.getInstance().logCustomVariable(getCurrentInterventionName()+"_"+key, value);
		Log.i("InterventionDataAccess","STORE "+getCurrentInterventionName()+"_"+key+" "+value);
	}
	
	public static String getVariable(String key){
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());		
		String value = sharedPrefs.getString(getCurrentInterventionName()+"_"+key, "");
		Log.i("InterventionDataAccess","GET "+getCurrentInterventionName()+"_"+key+" "+value);
		return value;
	}
	
	public static String getCurrentInterventionName(){
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		return sharedPrefs.getString(CURRENT_INTERVENTION_NAME,"INTERVENTION-TITLE");
	}
	
	public static void setCurrentInterventionName(String name){
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.putString(CURRENT_INTERVENTION_NAME, name);
		editor.commit();
	}
	
	public static int getCurrentInterventionTheme(){
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		return sharedPrefs.getInt(CURRENT_INTERVENTION_THEME,R.style.Theme);
	}
	
	public static void setCurrentInterventionTheme(int theme){
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.putInt(CURRENT_INTERVENTION_THEME, theme);
		editor.commit();
	}
	
	//PlaceHolder - return current PlanSet object for a given key
	public static PlanSet getPlanSet(String id){
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());
		String JSON = sharedPrefs.getString(id, "");
		return PlanSet.fromStorageJSON(id, JSON);
		//return null;
	}
		
	//usage log queries
	public static boolean checkIfItemAccessed(int id){
		//TODO: Is there a log of the item being accessed?
		return false;
	}
	
	public static boolean checkIfItemAccessedToday(int id){
		//TODO: Is there a log of the item being accessed since midnight?
		return false;
	}
	
	public static boolean itemAccessedInPeriodCount(int id, int hours){
		//TODO: How many logs of this item being accessed has there been in the last X hours
		return false;
	}

	// NOTE: This assumes that the variable is stored as a long converted to a string
	public static boolean variableLessThanHoursOld(String key, int hours){
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(UbhaveApplication.getContext());		
		Log.i("InterventionDataAccess","GET "+getCurrentInterventionName()+"_"+key+" "+sharedPrefs.getString(getCurrentInterventionName()+"_"+key, ""));
		long varTime = Long.parseLong(sharedPrefs.getString(getCurrentInterventionName()+"_"+key, ""));
		long curTime = System.currentTimeMillis();		
		if (varTime + hours*60*60*1000 >= curTime) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean checkIfInterventionFirstRun() {
		//TODO: place holder code -  should be replaced with usage query
		if (getVariable("firstRun").equals("false")){
			return false;
		}
		else
			return true;
	}
	
}