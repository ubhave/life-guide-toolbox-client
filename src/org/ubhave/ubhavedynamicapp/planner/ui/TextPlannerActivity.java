/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.UbhaveActivity;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.planner.PlannerController;
import org.ubhave.ubhavedynamicapp.planner.data.MultiFieldPlan;
import org.ubhave.ubhavedynamicapp.planner.data.Plan;
import org.ubhave.ubhavedynamicapp.planner.data.PlanSet;
import org.ubhave.ubhavedynamicapp.planner.data.PlannerField;
import org.ubhave.ubhavedynamicapp.planner.data.TextPlanner;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TextPlannerActivity extends UbhaveActivity{

	private TextPlanner planner;
	HashMap<String, HashMap <String, EditText>> entries;
	
	PlanSet planSet;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.planner);
		planner = (TextPlanner) getIntent().getParcelableExtra(PlannerController.PLANNER);
		planSet = (PlanSet) getIntent().getParcelableExtra(PlannerController.PLAN_SET);
		entries = new HashMap<String, HashMap <String, EditText>>();
		
		setActivityTitle(planner.getTitle());
		TextView textView = (TextView) findViewById(R.id.plannerText);
		textView.setText(planner.getHeaderText());
		
		for(String key:planSet.getPlans().keySet()){
			MultiFieldPlan plan = (MultiFieldPlan)planSet.getPlans().get(key);
			addPlan(planner.getFields(),plan);
		}
		
		if(entries.keySet().size()<1)
			addPlan(planner.getFields());
		
		planAddingCheck();
		
		
	}
	
	public void addPlan(View v){
		addPlan(planner.getFields());
		planAddingCheck();
	}
	
	private void addPlan(ArrayList<PlannerField> fields) {
		LinearLayout plansListView = (LinearLayout) findViewById(R.id.plannerLayout);
		
		LinearLayout planLayout = (LinearLayout) View.inflate(this, R.layout.text_plan, null);
				
		HashMap<String, EditText> fieldEntries= new HashMap<String, EditText>();
		for(PlannerField field:fields){
			RelativeLayout fieldView = (RelativeLayout) View.inflate(this, R.layout.text_plan_field, null);
			
			TextView labelTV = (TextView)fieldView.findViewById(R.id.fieldLabel);
			labelTV.setText(field.getLabel());
			
			TextView desTV = (TextView)fieldView.findViewById(R.id.fieldDescription);
			if(field.getDecription().equals("")){
				desTV.setVisibility(View.GONE);
			}
			else{
				desTV.setText(field.getDecription());
			}
			
			fieldEntries.put(field.getKey(), (EditText)fieldView.findViewById(R.id.fieldEntry));
			
			planLayout.addView(fieldView);
		}
		
		TextView labelTV = (TextView)planLayout.findViewById(R.id.planHead);
		labelTV.setText("Plan "+(entries.keySet().size()+1));
		plansListView.addView(planLayout);
		
		//TODO: This is ugly and part of a wider problem of "plans have a string id" where really thys just need an int index - we should fix this by changing planSet from taking a hash to taking an array
		entries.put(""+(entries.keySet().size()+1), fieldEntries);
	}
	
	private void addPlan(ArrayList<PlannerField> fields,MultiFieldPlan plan) {
		LinearLayout plansListView = (LinearLayout) findViewById(R.id.plannerLayout);
		
		LinearLayout planLayout = (LinearLayout) View.inflate(this, R.layout.text_plan, null);
				
		HashMap<String, EditText> fieldEntries= new HashMap<String, EditText>();
		for(PlannerField field:fields){
			RelativeLayout fieldView = (RelativeLayout) View.inflate(this, R.layout.text_plan_field, null);
			
			TextView labelTV = (TextView)fieldView.findViewById(R.id.fieldLabel);
			labelTV.setText(field.getLabel());
			
			TextView desTV = (TextView)fieldView.findViewById(R.id.fieldDescription);
			if(field.getDecription().equals("")){
				desTV.setVisibility(View.GONE);
			}
			else{
				desTV.setText(field.getDecription());
			}
			
			EditText fieldEditText = (EditText)fieldView.findViewById(R.id.fieldEntry);
			
			if(plan.getFieldValues().get(field.getKey())!=null){
				fieldEditText.setText(plan.getFieldValues().get(field.getKey()));
			}
				
			
			fieldEntries.put(field.getKey(), fieldEditText);
			
			planLayout.addView(fieldView);
		}
		
		TextView labelTV = (TextView)planLayout.findViewById(R.id.planHead);
		labelTV.setText("Plan "+(entries.keySet().size()+1));
		plansListView.addView(planLayout);
		
		//TODO: This is ugly and part of a wider problem of "plans have a string id" where really thys just need an int index - we should fix this by changing planSet from taking a hash to taking an array
		entries.put(""+(entries.keySet().size()+1), fieldEntries);
	}
	

	@Override
	protected String getActivityType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getActivityID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getActivityExtra() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void submit(View v){
		
		Intent result = getIntent();		
		HashMap<String, Plan> plans = new HashMap<String, Plan>();
		
		Iterator<Entry<String, HashMap<String, EditText>>> it = entries.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, HashMap<String,EditText>> entry = (Map.Entry<String, HashMap<String,EditText>>) it.next();
			String id = entry.getKey();
			HashMap<String,EditText> planFields = entry.getValue();
			
			HashMap<String,String> planValue = new HashMap<String,String>();
			for (String key:planFields.keySet()){
				planValue.put(key, planFields.get(key).getText().toString());
			}
			plans.put(id, new MultiFieldPlan(id, planValue));
		}
		
		PlanSet set = new PlanSet(planner.getKey());
		set.setPlans(plans);
		
		result.putExtra(PlannerController.PLAN_SET, set);		
		this.setResult(DynamicApplicationController.RESULT_PLANNER_OK, result);
		
		this.finish();
		
	}
	
	private void planAddingCheck(){
		if(entries.keySet().size()>=planner.getMaxPlans()){
			Button addButton = (Button)findViewById(R.id.addPlanButton);
			addButton.setVisibility(View.GONE);
		}
		else{
			Button addButton = (Button)findViewById(R.id.addPlanButton);
			addButton.setVisibility(View.VISIBLE);
		}
	}
		
}