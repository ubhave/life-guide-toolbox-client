/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.planner.data;

import java.util.ArrayList;

import org.ubhave.ubhavedynamicapp.planner.PlannerController;

import android.os.Parcel;
import android.os.Parcelable;

public class TextPlanner extends Planner{
	
	ArrayList<PlannerField> fields;
	int maxPlans;
		
	public TextPlanner(ArrayList<PlannerField> f, String h, String t, String k, int max){
		fields=f;
		headerText=h;
		title=t;
		key=k;
		type=PlannerController.PLANNER_TYPE_TEXT;
		maxPlans=max;
	}
	
	public ArrayList<PlannerField> getFields(){
		return fields;
	}
	
	public int getMaxPlans(){
		return maxPlans;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(type);
		dest.writeString(headerText);
		dest.writeString(title);
		dest.writeString(key);
		dest.writeTypedList(fields);
		dest.writeInt(maxPlans);
	}
	
	public TextPlanner(Parcel in){
		type=in.readInt();
		headerText=in.readString();
		title=in.readString();
		key=in.readString();
		fields = new ArrayList<PlannerField>();
		in.readTypedList(fields, PlannerField.CREATOR);
		maxPlans=in.readInt();
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public static final Parcelable.Creator<TextPlanner> CREATOR = new Parcelable.Creator<TextPlanner>()
	{
		public TextPlanner createFromParcel(Parcel in)
		{
			return new TextPlanner(in);
		}

		public TextPlanner[] newArray(int size)
		{
			return new TextPlanner[size];
		}
	};	
}