/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */
package org.ubhave.ubhavedynamicapp.app.web;

import java.io.FileOutputStream;
import java.util.HashMap;

import org.ubhave.ubhavedynamicapp.DynamicApplicationConstants;
import org.ubhave.ubhavedynamicapp.UbhaveApplication;
import org.ubhave.ubhavedynamicapp.WebConnection;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class DownloadInterventionTask extends AsyncTask<Void, Void, String>
{
	private final Context context;
	private final String interventionId;
	private final String interventionURL;
	private final String resourcesURL;
	private final String token;
	private final ProgressDialog dialog;

	public DownloadInterventionTask(Context context, String interventionId, String interventionURL, String resourcesURL, String token)
	{
		this.context = context;
		this.interventionId = interventionId;
		this.interventionURL = interventionURL;
		this.resourcesURL = resourcesURL;
		this.token = token;

		dialog = new ProgressDialog(context);
		dialog.setMessage("Downloading App Set-Up...");
		dialog.setIndeterminate(false);
		dialog.setCancelable(false);
	}

	@Override
	protected void onPreExecute()
	{
		dialog.show();
	}

	@Override
	protected String doInBackground(Void... arg0)
	{
		/*
		 * DEMO VERSION:		 
		try
		{
			Thread.sleep(1500); // remove for non-demo
			copyFile();
			return null;
		}
		catch (Exception e)
		{
			return e.getMessage();
		}
		*/
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", DynamicApplicationConstants.REST_API_CONTENT_TYPE_JSON);
		headers.put("Authentication", "IDToken " + this.token);

		try {
			WebConnection.getData(UbhaveApplication.getContext(), 
					this.interventionURL, 
					DynamicApplicationConstants.CURRENT_INTERVENTION_CONFIG, 
					null,
					headers);
			
			if(resourcesURL!=null||!resourcesURL.equals(""))
				getResources();
		} catch (Exception e) {
			return e.getMessage();
		}

		return null;
		
	}
	
	private void getResources() throws Exception{
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", DynamicApplicationConstants.REST_API_CONTENT_TYPE_ZIP);
		headers.put("Authentication", "IDToken " + this.token);

		System.out.println("Getting resources from "+resourcesURL);
		
		WebConnection.getAndWriteData(this.resourcesURL, 
				DynamicApplicationConstants.CURRENT_INTERVENTION_RES, 
				null,
				headers);		
	}
	
	private void storeConfig(final String fileName, final String fileContents) throws Exception
	{
		FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
		fos.write(fileContents.getBytes());
		fos.close();
	}

	@Override
	protected void onPostExecute(String errorMessage)
	{
		try
		{
			if (errorMessage != null)
			{
				Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
			}
			dialog.dismiss();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
