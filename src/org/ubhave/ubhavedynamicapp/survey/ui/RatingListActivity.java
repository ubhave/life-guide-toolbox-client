/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.survey.ui;

import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.survey.QuestionnaireController;
import org.ubhave.ubhavedynamicapp.survey.data.as.AbstractAnswer;
import org.ubhave.ubhavedynamicapp.survey.data.as.RatingAnswer;
import org.ubhave.ubhavedynamicapp.survey.data.as.RatingListAnswers;
import org.ubhave.ubhavedynamicapp.survey.data.qs.RatingList;
import org.ubhave.ubhavedynamicapp.survey.data.qs.RatingQuestion;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class RatingListActivity extends AbstractQuestionActivity
{
	
	private static final String TAG = "RatingListActivity";

	private RatingQuestion[] questions;
	private RatingAnswer[] answers;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		RatingList ratingList = intent.getParcelableExtra(QuestionnaireController.CURRENT_QUESTION);

		questions = ratingList.getQuestions();
		answers = new RatingAnswer[questions.length];

		LinearLayout background = (LinearLayout) findViewById(R.id.background);
		for (int i = 0; i < questions.length; i++)
		{
			answers[i] = new RatingAnswer(questions[i].getIdentifier(), questions[i].getInitialValue());
			View questionView = getQuestionView(questions[i], i);
			background.addView(questionView);
		}
		initSubmitButton();
	}

	private LinearLayout getQuestionView(final RatingQuestion question, final int questionIndex)
	{
		final String[] descriptions = question.getDescriptions();

		LinearLayout ratingView = (LinearLayout) View.inflate(this, R.layout.question_rating, null);
		setTextView(R.id.ratingTitle, ratingView, question.getTitle());
		setTextView(R.id.minLabel, ratingView, descriptions[0]);
		setTextView(R.id.maxLabel, ratingView, descriptions[descriptions.length - 1]);

		SeekBar ratingBar = (SeekBar) ratingView.findViewById(R.id.ratingSeekBar);
		final TextView label = (TextView) ratingView.findViewById(R.id.ratingLabel);
		label.setText(descriptions[question.getInitialValue()]);

		ratingBar.setMax(100 * question.getMaxValue());
		ratingBar.setProgress((100 * question.getInitialValue()) + 50);
		ratingBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener()
		{
			public void onStopTrackingTouch(SeekBar seekBar)
			{
			}

			public void onStartTrackingTouch(SeekBar seekBar)
			{
			}

			public void onProgressChanged(SeekBar seekBar, int rating, boolean fromUser)
			{
				int position = (rating / 100);
				if (position == descriptions.length)
					position = descriptions.length - 1;
				answers[questionIndex].setValue(position);
				label.setText(descriptions[position]);
			}
		});

		return ratingView;
	}

	private void setTextView(int fieldId, View parent, String text)
	{
		TextView view = (TextView) parent.findViewById(fieldId);
		view.setText(text);
	}

	@Override
	protected int getLayout()
	{
		return R.layout.question_rating_list;
	}

	private void initSubmitButton()
	{
		Button submit = (Button) findViewById(R.id.submitButton);
		submit.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				int[] ids = new int[questions.length];
				int[] values = new int[questions.length];
				for (int i = 0; i < answers.length; i++)
				{
					ids[i] = answers[i].getId();
					values[i] = answers[i].getValue();
				}
				AbstractAnswer answers = new RatingListAnswers(question_id, ids, values);
				submitAnswers(answers);
			}
		});
	}


	@Override
	protected String getActivityType() {
		return TAG;
	}
}
