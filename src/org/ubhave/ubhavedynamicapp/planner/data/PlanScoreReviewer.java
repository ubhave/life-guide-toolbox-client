package org.ubhave.ubhavedynamicapp.planner.data;

import org.ubhave.ubhavedynamicapp.info.data.TextInformation;
import org.ubhave.ubhavedynamicapp.planner.PlannerReviewController;

import android.os.Parcel;
import android.os.Parcelable;

public class PlanScoreReviewer extends PlanReviewer{

	int[] bounds;
	
	public PlanScoreReviewer(String key, String t, String header, TextInformation f, String edit, int[] b){
		super(key,t,header,f,edit);
		
		bounds =b;
		this.type=PlannerReviewController.REVIEWER_TYPE_SCORE;		
	}
	
	public int[] getBounds(){
		return bounds;
	}
		
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeIntArray(bounds);
	}
	
	public PlanScoreReviewer(Parcel in){
		super(in);
		bounds= new int[2];
		in.readIntArray(bounds);
	}
	
	public static final Parcelable.Creator<PlanScoreReviewer> CREATOR = new Parcelable.Creator<PlanScoreReviewer>()
	{
		public PlanScoreReviewer createFromParcel(Parcel in)
		{
			return new PlanScoreReviewer(in);
		}

		public PlanScoreReviewer[] newArray(int size)
		{
			return new PlanScoreReviewer[size];
		}
	};
	
}