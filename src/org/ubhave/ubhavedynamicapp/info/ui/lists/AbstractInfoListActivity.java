/* *****************************************************************************
 *
 * UBhave Dynamic App Framework
 *

This application was developed as part of the EPSRC UBhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.ubhave.org.

Copyright (c) 2014
  University of Southampton
    Charlie Hargood, cah07r.soton.ac.uk
    Danius Michaelides, dtm.soton.ac.uk
  University of Birmingham
    Veljko Pejovic, v.pejovic.bham.ac.uk
  University of Cambridge
    Neal Lathia, neal.lathia.cam.ac.uk
    Kiran Rachuri, kiran.rachuri.cam.ac.uk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of the Universities of Southampton, Birmingham and
      Cambridge nor the names of its contributors may be used to endorse or
      promote products derived from this software without specific prior
      written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***************************************************************************** */

package org.ubhave.ubhavedynamicapp.info.ui.lists;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ubhave.ubhavedynamicapp.R;
import org.ubhave.ubhavedynamicapp.app.DynamicApplicationController;
import org.ubhave.ubhavedynamicapp.info.InformationController;
import org.ubhave.ubhavedynamicapp.info.data.InformationList;
import org.ubhave.ubhavedynamicapp.info.ui.AbstractInformationActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public abstract class AbstractInfoListActivity extends AbstractInformationActivity
{
	protected final static String itemText = "titleText";
//	protected final static String detailText = "detailText";
	protected final static String[] from = new String[] { itemText};//, detailText };
	protected final static int[] to = new int[] { R.id.answerText};//, R.id.detailText };

	protected InformationList items;
	protected boolean parentStatus;
	protected List<Map<String, String>> data;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		data = new ArrayList<Map<String, String>>();

		Intent intent = getIntent();
		items = intent.getParcelableExtra(InformationController.CURRENT_INFORMATION);
		parentStatus = intent.getBooleanExtra(InformationController.CURRENT_INFORMATION_PARENT_STATUS, false);

		ListView lv = initListView();
		setListOnClick(lv);
		enableBackButton();
	}
	
	protected void enableBackButton()
	{
		Button button = (Button) findViewById(R.id.backButton);
		button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				submit();
			}
		});
		if(!parentStatus){
			button.setText("Done");
		}
	}
	
	private void submit()
	{
		this.setResult(DynamicApplicationController.RESULT_INFO_CANCELED);
		this.finish();
	}

	protected ListView initListView()
	{
		loadData();
		ListView lv = (ListView) findViewById(R.id.listView);
		lv.setAdapter(getAdapter());
		return lv;
	}

	protected abstract AdaptiveSimpleAdapter getAdapter();

	protected abstract int getLayout();

	protected abstract void setListOnClick(ListView lv);

	protected void loadData()
	{
		for (int i=0; i<items.size(); i++)
		{
			Map<String, String> map = new HashMap<String, String>();
			map.put(itemText, items.get(i).getTitle());
			data.add(map);
		}
	}
}